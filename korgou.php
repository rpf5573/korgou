<?php
/**
 * Plugin Name: Korgou 
 * Plugin URI:  https://shoplic.kr
 * Description: Korgou Plugin
 * Version:     1.0.1
 * Author:      Shoplic Inc.
 * Author URI:  https://shoplic.kr
 * License:     Shoplic License
 * Text Domain: korgou
 * Domain Path: /languages/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( class_exists( 'Shoplic_Plugin' ) ) {
    (new class([
		'id'            => 'korgou',
		'version'       => '1.0.1.12',
		'file'          => __FILE__,
		'domain_path'   => '/languages/',
    ]) extends Shoplic_Plugin {

        function load()
        {
            parent::load();
        }
    })->load();
}

