jQuery(function($) {
    function converter() {
        var val = parseInt($('#won').val()) || 0;
        $('#cny').val(convert(val, 163));
        $('#usd').val(convert(val, 1020));
    }
    function convert(num, currency) {
        return Math.ceil(num / currency * 100) / 100;
    }
    if ($('.quick-converter').length > 0) {
        $('#won').val('10000');
        $('#won').change(converter);
        $('#won').keyup(converter);
        converter();
    }
});
