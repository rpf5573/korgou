<?php
defined('ABSPATH') or exit;

return new class(__FILE__) extends Shoplic_Controller
{
  protected $id = 'korgou';

  function load()
  {
    $this->style('google-fonts', ['src' => 'https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@300;400;500;700&display=swap']);
    $this->style('home');
    $this->style('loader');

    $this->add_action('message', 10, 2);
    $this->add_shortcode('shipment_tracking');
    $this->add_shortcode('register');
    $this->add_shortcode('currency_converter');
    $this->add_shortcode('cost_calculator');
    $this->add_shortcode('cost_calculator_banner');
    $this->add_ajax_nopriv('process_register');

    $this->add_ajax_nopriv('calculate_cost');
    $this->add_ajax('calculate_cost');
    // $this->script('currency-converter');

    // email 인증 관련
    add_action('init', [$this, 'verify_email_confirm']);
    add_filter('authenticate', [$this, 'authenticate_verify_email'], 999, 3);
  }

  function cost_calculator()
  {
    $this->view('cost-calculator', [
      'couriers' => apply_filters('korgou_forward_get_courier_list', [], ['status' => Korgou_Forward_Courier::STATUS_OPEN]),
    ]);
  }

  function cost_calculator_banner()
  {
    $this->view('cost-calculator-banner');
  }

  function currency_converter()
  {
    $this->view('currency-converter');
  }

  function message($type, $message)
  {
    $this->view('message', ['type' => $type, 'message' => $message]);
  }

  function shipment_tracking()
  {
    $context = [
      'track_no' => $_POST['track_no'] ?? ''
    ];

    $this->view('shipment-tracking', $context);
  }

  function register()
  {
    $context = [];
    $this->view('register', $context);
  }

  function process_register()
  {
    $data = Shoplic_Util::get_post_data('user_email', 'user_pass', 'confirm_pass');

    if (email_exists($data['user_email'])) {
      $user = get_user_by('email', $data['user_email']);
      if ($user) {
        $wait_verify_email = get_user_meta($user->ID, 'wait_verify_email', true);
        if ($wait_verify_email) {
          wp_send_json_error('Please check the verify link in your mailbox.');
        }
      }
      wp_send_json_error('You cannot use this email.');
    }

    if (!is_email($data['user_email'])) {
      wp_send_json_error('Email address is not valid.');
    }

    if (isset($data['confirm_pass']) && $data['confirm_pass'] !== $data['user_pass']) {
      wp_send_json_error('Password does not match.');
    }

    if (false) {
      // Google reCaptcha
      $option = $this->get_option('shoplic_member_register_option');

      // google recaptcha는 잠시 끈다
      if ($option->recaptcha == 'v3' && $option->recaptcha_site_key != '' && $option->recaptcha_secret_key != '') {
        if (!isset($_POST['recaptcha_token']) || empty($_POST['recaptcha_token'])) {
          $errors['register'] = 'Registration is not allowed. Please contact to us.';
        } else {
          $response = wp_remote_post(
            'https://www.google.com/recaptcha/api/siteverify',
            array(
              'method' => 'POST',
              'body'   => array(
                'secret'   => $option->recaptcha_secret_key,
                'response' => $_POST['recaptcha_token'],
              ),
            )
          );

          if (is_wp_error($response)) {
            $errors['register'] = 'Registration is not allowed. Please contact to us.(' . $response->get_error_message() . ').';
          } else {
            $json = json_decode($response['body'], true);
            if (!$json['success']) {
              $errors['register'] = 'Registration is not allowed. Please contact to us.(' . implode(
                ', ',
                $json['error-codes']
              ) . ')';
            } elseif ($json['score'] < 0.5) {
              $errors['register'] = 'Registration is not allowed. Please contact to us.(score: ' . $json->score . ')';
            }
          }
        }
      }
    }

    if (!empty($errors)) {
      wp_send_json_error($errors);
    }

    $user = Shoplic_Util::get_post_data('englishname', 'nativename', 'gender', 'country', 'phonenum', 'mobilenum');

    $userid = apply_filters('korgou_user_generate_userid', '', $user['englishname']);
    if (empty($userid)) {
      wp_send_json_error('Cannot generate user id');
    }
    $data['user_login'] = $userid;

    $user_data = array_intersect_key(
      $data,
      array_flip(
        [
          'user_login',
          'user_pass',
          'user_email',
          'user_url',
          'user_nicename',
          'display_name',
          'user_registered',
          'first_name',
        ]
      )
    );

    remove_action('sanitize_user', 'strtolower');

    $result    = wp_insert_user($user_data);

    if (is_wp_error($result)) {
      $message = $result->get_error_message();
      switch ($result->get_error_code()) {
        case 'user_login_too_long':
        case 'existing_user_login':
        case 'invalid_username':
          $errors['user_login'] = $message;
          break;
        case 'existing_user_email':
          $errors['user_email'] = $message;
          break;
      }
      if (!empty($errors)) {
        wp_send_json_error($errors);
      }
    }

    $time = current_time('mysql');

    $user['userid'] = $userid;
    $user['email'] = $data['user_email'];
    $user['status'] = '1';
    $user['registertime'] = $time;
    $user['regip'] = Shoplic_Util::get_client_ip();
    do_action('korgou_user_insert_user', $user);

    do_action('korgou_user_balance_insert_balance', [
      'userid' => $userid,
      'balance' => 0,
      'state' => '1',
      'updatetime' => $time,
    ]);

    $address = Shoplic_Util::get_post_data(
      'englishname',
      'nativename',
      'zipcode',
      'country',
      'englishname',
      'nativename',
      'province',
      'city',
      'addressdetail',
      'company',
      'phonenum',
      'mobilenum'
    );
    $address['userid'] = $userid;
    do_action('korgou_user_insert_address', $address);

    $option   = $this->get_option('shoplic_member_register_option');
    $redirect = $option->redirect != null ? get_permalink($option->redirect) : home_url();
    // if ($option->logged_in != null) {
    //   wp_set_auth_cookie($result, false, false);
    // }

    $data['ID'] = $result;
    $this->do_action('after_register', $data);

    update_user_meta($result, 'billing_first_name', $address['englishname']);
    update_user_meta($result, 'billing_phone', $address['phonenum']);
    update_user_meta($result, 'billing_email', $user['email']);

    $verify_code = md5(uniqid());
    update_user_meta($result, 'wait_verify_email', $verify_code);
    update_user_meta($user->ID, 'send_verify_email_time', current_time('timestamp'));
    $this->send_verify_email($user, $verify_code);

    wp_send_json_success($redirect);
  }

  function calculate_cost()
  {
    $user_country = $_POST['country'];
    $courier_id = $_POST['courier'];
    $weight = intval($_POST['weight']);

    $courier = apply_filters('korgou_forward_get_courier', null, ['id' => $courier_id]);
    if ($courier == null) {
      $courier_name = 'N/A';
      $courier_img = null;
      $price = 'N/A';
    } else {
      $courier_name = $courier->enname;
      $courier_img = $this->get_courier_img($courier_id);

      $courier_country = apply_filters('korgou_forward_get_courier_country', null, [
        'forwardcourierid' => $courier_id,
        'user_country' => $user_country
      ]);
      $country = ($courier_country != null) ? $courier_country->country : 'other';

      $forward_fare = apply_filters('korgou_forward_get_minimum_fare', null, $country, $courier_id, $weight);
      $price = ($forward_fare != null) ? $forward_fare->price : 'N/A';
    }

    wp_send_json_success($this->contents('calculate-cost', [
      'price' => $price,
      'courier_name' => $courier_name,
      'courier_img' => $courier_img,
    ]));
  }

  function get_courier_img($courier_id)
  {
    $posts = get_posts([
      'post_type' => 'courier',
      'post_status' => 'publish',
      'posts_per_page' => 1,
      'meta_key' => 'courier_id',
      'meta_value' => $courier_id,
    ]);

    return empty($posts) ? '' : wp_get_attachment_url(get_post_thumbnail_id($posts[0]->ID), 'full');
  }

  function send_verify_email($user, $verify_code = '')
  {
    $blogname = get_option('blogname');
    $home_url = home_url();
    $verify_email_url = home_url('?action=korgou_verify_email_confirm&verify_code=' . $verify_code);

    $to = $user['email'];
    $subject = "{$blogname} - Verify your email for register";
    $message = "Almost done, please verify your email. Verify using this link: {$verify_email_url}";

    $headers = 'From: ' . $to . "\r\n" .
      'Reply-To: ' . $to . "\r\n";

    $result = wp_mail($to, $subject, $message, $headers);
  }

  function verify_email_confirm()
  {
    $action = isset($_GET['action']) ? $_GET['action'] : '';
    $verify_code = isset($_GET['verify_code']) ? $_GET['verify_code'] : '';

    if ($action !== 'korgou_verify_email_confirm') {
      return;
    }
    if (empty($verify_code)) {
      return;
    }

    $users = get_users(array('meta_key' => 'wait_verify_email', 'meta_value' => $verify_code));

    foreach ($users as $user) {
      delete_user_meta($user->ID, 'wait_verify_email');
      update_user_meta($user->ID, 'verify_email', '1');

      $login_url = wp_login_url();

      wp_redirect(add_query_arg(array('verify_email_confirm' => '1'), wp_login_url()));
      exit;
    }
    if (is_user_logged_in()) {
      wp_redirect(home_url());
    } else {
      wp_redirect(wp_login_url());
    }
    exit;
  }

  function authenticate_verify_email($user, $username, $password)
  {
    $pass = ((!is_wp_error($user)) && $password) ? wp_check_password($password, $user->user_pass, $user->ID) : false;
    if (!$pass) {
      return $user;
    }

    $wait_verify_email = get_user_meta($user->ID, 'wait_verify_email', true);
    if ($wait_verify_email) { // 아직 인증을 안했다면
      return new WP_Error('verify_email_failed', 'Please check the email sent to your email address.');
    }
    return $user;
  }
};
