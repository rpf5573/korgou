<div class="col courier">
    <?php if ($courier_img): ?>
        <img src="<?php echo $courier_img; ?>">
    <?php else: ?>
        <?php echo $courier_name; ?>
    <?php endif; ?>
</div>
<div class="col text-right">
    <span class="cost">&#8361;<?php echo number_format($price); ?></span>
</div>
