<div class="ast-row">
    <div class="ast-col-12">
        <?php $this->ajax_form('process_register', ['class' => 'needs-validation korgou-register']); ?> 
            <div class="card-box">
                <h4 class="section-title"><?php _e('Basic Information', 'korgou'); ?></h4>
                <div class="card-body">
                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Email', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="email" class="form-control" id="email-input" name="user_email" value="" required>
                            <small class="form-text"><?php _e('For password recovry and important notifications, please enter your frequently used email address.', 'korgou'); ?></small>
                        </div>
                    </div>
                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Password', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="password" class="form-control" id="user_pass" name="user_pass" value="" required>
                        </div>
                    </div>
                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Confirm Password', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="password" class="form-control" id="email-input" name="confirm_pass" value="" required data-parsley-equalto="#user_pass">
                        </div>
                    </div>
                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('English Full Name', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="text" class="form-control" id="-input" name="englishname" value="" required>
                            <small class="form-text"><?php _e('Capitalized letters in English', 'korgou'); ?></small>
                        </div>
                    </div>
                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Native Full Name', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="text" class="form-control" id="-input" name="nativename" value="" required>
                            <small class="form-text"><?php _e('In the language of the recipient country', 'korgou'); ?></small>
                        </div>
                    </div>
                    <div class="form-group ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Gender', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <?php BS_Form::select([
                                'name' => 'gender',
                                'id' => 'gender-input',
                                'options' => Korgou_User::$GENDERS,
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Country', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <?php do_action('korgou_select_country'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-box">
                <h4 class="section-title" style="margin-top: 2em;"><?php _e('Address', 'korgou'); ?></h4>
                <div class="card-body">
                    <p>
                        <?php _e('Enter the address information in English. However, address in China, Hong Kong and Taiwan can be input in Chinese.', 'korgou'); ?>
                    </p>

                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Zip code', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="text" class="form-control" id="" name="zipcode" value="" required>
                        </div>
                    </div>
                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('State / Province / Region', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="text" class="form-control" id="" name="province" value="" required>
                        </div>
                    </div>
                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('City', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="text" class="form-control" id="" name="city" value="" required>
                        </div>
                    </div>
                    <div class="form-group ast-row required">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Detaild Address', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="text" class="form-control" id="" name="addressdetail" value="" required>
                        </div>
                    </div>
                    <div class="form-group ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Company / Organization Name', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="text" class="form-control" id="" name="company" value="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-box">
                <h4 class="section-title" style="margin-top: 2em;"><?php _e('Contact', 'korgou'); ?></h4>
                <div class="card-body">
                    <div class="form-group ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Phone Number', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="text" class="form-control" id="" name="phonenum" value="">
                            <small class="form-text">Eg. +1-312-400-5566</small>
                        </div>
                    </div>
                    <div class="form-group required ast-row">
                        <label for="" class="ast-col-sm-4 ast-col-form-label"><?php _e('Mobile Phone', 'korgou'); ?></label>
                        <div class="ast-col-sm-8">
                            <input type="text" class="form-control" id="" name="mobilenum" value="" required>
                        </div>
                    </div>
                    <div class="form-group required">
                        <div class="form-check">
                            <input class="form-check-input" style="margin-top: 0.7em;" type="checkbox" value="" id="defaultCheck1" required>
                            <label class="form-check-label" for="defaultCheck1">
                                <?php _e('Please indicate that you agree to the Terms of Service', 'korgou'); ?> 
                            </label>
                        </div>
                    </div>

                    <div>
                        <span class="required-mark">*</span>
                        <?php _e('Required field', 'korgou'); ?>
                    </div>
                </div>

                <div style="text-align:center;">
                    <input type="reset" class="btn btn-secondary" value="<?php _e('Reset', 'korgou'); ?>">
                    <input type="submit" id="submit-btn" class="btn btn-primary" value="<?php _e('Register', 'korgou'); ?>">
                </div>

            </div> <!-- end card-box -->
        </form>
    </div> <!-- end col -->
</div>

<script src="https://cdn.jsdelivr.net/gh/guillaumepotier/Parsley.js@2.9.2/dist/parsley.js"></script>
<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.parsley({
        requiredMessage: '',
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid',
        errorsWrapper: '<div style="color: red;"></div>',
        errorTemplate: '<div></div>',
        trigger: 'submit'
    }).on('field:validated', function() {
    }).on('form:submit', function() {
        $form.ajaxSubmit(function(response) {
            if (response.success)
                location.href = response.data;
            else
                alert(response.data);
        });
        return false; // dont\'t submit form for this demo
    });
    return;

    $form.ajaxForm(function(response) {
        if (response.success)
            location.reload();
        else
            alert(response.data);
    }).parsley({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid',
        errorsWrapper: '<div style="color: red;"></div>',
        errorTemplate: '<div></div>',
        trigger: 'submit'
    });
});
</script>

