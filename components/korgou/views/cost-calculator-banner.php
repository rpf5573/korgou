<?php if (is_user_logged_in()): ?>
    <a href="/shipping-fee-calculator/">
<?php else: ?>
    <a href="<?php echo add_query_arg('redirect_to', urlencode('/shipping-fee-calculator/'), '/login/'); ?>" onclick="return confirm('Please log in to calculate the shipping amount.');">
<?php endif; ?>
    <img src="https://www.korgou.com/wp-content/uploads/2021/11/calculator-btn.jpg" class="attachment-full size-full" alt="" srcset="https://www.korgou.com/wp-content/uploads/2021/11/calculator-btn.jpg 728w, https://www.korgou.com/wp-content/uploads/2021/11/calculator-btn-300x37.jpg 300w, https://www.korgou.com/wp-content/uploads/2021/11/calculator-btn-600x74.jpg 600w" sizes="(max-width: 728px) 100vw, 728px" width="728" height="90">
</a>