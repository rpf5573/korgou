<div class="kg-cost-cal">
    <?php $this->ajax_form('calculate_cost'); ?>

        <input type="hidden" name="weight">

        <div class="form-group">
            <label for="input-country"><b><?php _e('Ship To', 'korgou'); ?></b></label>
            <?php do_action('korgou_select_country'); ?>
        </div>
        <div class="form-group mt-3">
            <label for="input-courier"><b><?php _e('Courier', 'korgou'); ?></b></label>
            <select class="form-control" id="input-courier" name="courier">
                <?php if (is_user_logged_in()): ?>
                    <option value=""><?php _e('Choose', 'korgou'); ?></option>
                    <?php foreach ($couriers as $courier): ?>
                        <option value="<?php echo $courier->id; ?>"><?php echo $courier->enname; ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
            <span>​Please check the available shipping method to your address via the notice on the website.</span>
        </div>
        <div class="form-group mt-3">
            <label for="input-weight"><b><?php _e('Package Weight(g)', 'korgou'); ?></b></label>
            <input type="number" class="form-control" id="input-weight">
        </div>
        <div class="form-group mt-3">
            <label for="input-length"><b><?php _e('Package Dimensions(cm)', 'korgou'); ?></b></label>
            <div class="dimensions">
                <input type="number" class="form-control" id="input-length" placeholder="L">
                <span>&#10006;</span>
                <input type="number" class="form-control" id="input-width" placeholder="W">
                <span>&#10006;</span>
                <input type="number" class="form-control" id="input-height" placeholder="H">
            </div>
        </div>

        <p class="mt-4">
            <?php if (is_user_logged_in()): ?>
                There may be a difference from the actual quote in progress. 
                <br>
                <button type="button" id="cost-btn" class="btn btn-primary btn-block mt-4">View Shipping Rates</button>
            <?php else: ?>
                Please <a href="<?php echo add_query_arg('redirect_to', '/shipping-fee-calculator/', '/login/'); ?>" style="text-decoration: underline;">log in</a> to see the estimate.
                <br>
                There may be a difference from the actual quote in progress. 
            <?php endif; ?>
        </p>
    </form>

    <div class="row mt-5" id="cost-result"></div>
</div>

<script type="text/javascript">
jQuery(function($) {

    <?php if (!is_user_logged_in()): ?>
        $('.kg-cost-cal select').prop('disabled', true);
        $('.kg-cost-cal input').prop('disabled', true);
    <?php endif; ?>

/*
    $('.kg-cost-cal input').keydown(function(e) {
        // Allow only backspace and delete
        if ( e.keyCode == 46 || e.keyCode == 8 ) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (e.keyCode < 48 || e.keyCode > 57 ) {
                e.preventDefault(); 
            }   
        }
    });
    */

    function getValue(field) {
        var val = parseInt($('#input-'+field).val());
        return !isNaN(val) && isFinite(val) ? val : 0;
    }

    function calculateWeight(courier) {
    }

    $('#cost-btn').click(function() {
        if ($('#input-country').val() == '') {
            alert('<?php _e('Please choose a country.', 'korgou'); ?>');
        } else if ($('#input-courier').val() == '') {
            alert('<?php _e('Please choose a courier.', 'korgou'); ?>');
        } else {
            var weight = getValue('weight'),
                length = getValue('length'),
                width = getValue('width'),
                height = getValue('height');

            if (weight == 0 || length == 0 || width == 0 || height == 0) {
                alert('<?php _e('Please insert package weight and dimensions.', 'korgou'); ?>');
            } else {
                var dimWeight = ['2', '3', '9', '11'].includes($('#input-courier').val()) ?
                    Math.ceil(length * width * height / 5000) * 1000 :
                    Math.ceil(length * width * height / 6000) * 1000 ;
                $('input[name="weight"]').val(Math.max(getValue('weight'), dimWeight));

                $(this).closest('form').ajaxSubmit(function(response) {
                    if (response.success) {
                        $('#cost-result').html(response.data);
                    } else {
                        alert(response.data);
                    }
                });
            }
        }
    });
});
</script>

