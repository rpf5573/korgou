<div class="dropdown currency-converter">
    <button type="button" class="dropbtn currency-converter-btn"><?php _e('Currency Converter', 'korgou'); ?></button>
    <div class="dropdown-content">
        <div class="pb0 pbreak">
            <div class="fields  fieldname1_1 cff-currency-field" id="field_1-0">
                <label for="fieldname1_1"><?php _e('Korean Won', 'korgou'); ?> (<?php _e('KRW', 'korgou'); ?>)</label>
                <div class="dfield"><input id="fieldname1_1" name="fieldname1_1" class="field cffcurrency medium valid" type="text" value="10000" min="0" aria-invalid="false">
                    <span class="uh"></span>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="fields  fieldname4_1 cff-calculated-field" id="field_1-1" style="">
                <label for="fieldname4_1"><?php _e('Chinese Yuan', 'korgou'); ?> (<?php _e('CNY', 'korgou'); ?>)</label>
                <div class="dfield">
                    <input id="fieldname4_1" name="fieldname4_1" readonly="" class="codepeoplecalculatedfield field medium" type="text" value="" dep="" notdep="">
                    <span class="uh"></span>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="fields  fieldname5_1 cff-calculated-field" id="field_1-2" style="">
                <label for="fieldname5_1"><?php _e('US Dollars', 'korgou'); ?> (<?php _e('USD', 'korgou'); ?>)</label>
                <div class="dfield">
                    <input id="fieldname5_1" name="fieldname5_1" readonly="" class="codepeoplecalculatedfield field medium" type="text" value="" dep="" notdep="">
                    <div><?php _e('The USD exchange rate has included the fees charged by PayPal. For more details, please refer to <a href="/pricing/">Pricing</a>.', 'korgou'); ?></div>
                </div>
                <div class="clearer"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    function converter() {
        var val = parseInt($('#fieldname1_1').val());
        $('#fieldname4_1').val(convert(val, 163));
        $('#fieldname5_1').val(convert(val, 1020));
    }
    function convert(num, currency) {
        return Math.ceil(num / currency * 100) / 100;
    }
    $('#fieldname1_1').change(converter);
    $('#fieldname1_1').keyup(converter);
    converter();

    $('.dropdown').hover(function() {
        $('#fieldname1_1').focus();
    });
});
</script>