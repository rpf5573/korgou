<div class="row mb-4">
    <div class="col-12">
        <div class="card-box text-center">
            <p class="display-4"><?php _e('Error', 'korgou'); ?></p>
            <p class="lead"><?php _e($message, 'korgou'); ?></p>
        </div>
    </div>
</div>
