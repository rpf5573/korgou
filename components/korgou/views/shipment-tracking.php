<!--추적 번호 입력 박스。-->
<input type="text" id="YQNum" maxlength="50" value="<?php echo $track_no; ?>"/>
<!--버튼은 스크립트를 불러오기 위한 방법으로 사용됨。-->
<input type="button" value="TRACK" onclick="doTrack()"/>
<!--추적 결과를 표시하는 상자。-->
<div id="YQContainer"></div>

<!--스크립트코드는 페이지의 하단에 입력되어, 페이지가 로딩된후 실행될수있습니다.-->
<script type="text/javascript" src="//www.17track.net/externalcall.js"></script>
<script type="text/javascript">
function doTrack() {
    var num = document.getElementById("YQNum").value;
    if(num===""){
        alert("Enter your number."); 
        return;
    }
    YQV5.trackSingle({
        //요구됨, 컨텐츠 호스트의 켄테이너 아이디를 지정할것.
        YQ_ContainerId:"YQContainer",
        //옵션, 추적 결과 높이 지정할것, 최대 높이는 800px, 기본값은 560 픽셀입니다.
        YQ_Height:560,
        //옵션, 배송업체 선택, 디폴트는 자동 인식.
        YQ_Fc:"0",
        //옵션, UI 언어 지정할것, 디폴트 언어는 브라우져 세팅에 기반해서 자동으로 감지함.
        YQ_Lang:"en",
        //요구됨, 추적할 필요가 있는 번호 지정함.
        YQ_Num:num
    });
}
<?php if (!empty($track_no)): ?>
    jQuery(function($) {
        doTrack();
    });
<?php endif; ?>
</script>