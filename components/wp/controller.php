<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'wp';

    function load()
    {
        $this->add_action('footer');
    }

    function footer()
    {
        $currencies = [];
        foreach (apply_filters('korgou_currency_get_exchange_list', []) as $c) {
            $currencies[$c->currency] = $c->exchangekrw;
        }
        $context = [
            'currencies' => $currencies,
        ];
        $this->view('footer', $context);
    }

};
