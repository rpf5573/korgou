        <h4 class="section-title"><?php _e('Packages selected for forward', 'korgou'); ?></h4>
        <div class="card-box">

            <div class="table-responsive">
            <table class="table-bordered table table-hover">
                <thead class="thead-light">
                <tr>
                    <th><?php _e('Package No.', 'korgou'); ?></th>
                    <th><?php _e('Arrival time', 'korgou'); ?></th>
                    <th><?php _e('Tracking number', 'korgou'); ?></th>
                    <th><?php _e('Weight', 'korgou'); ?></th>
                    <th><?php _e('Remarks', 'korgou'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($packages as $package): ?>
                <tr>
                    <td><?php echo $package->packageid; ?></td>
                    <td><?php echo $package->arrivaltime; ?></td>
                    <td><?php echo $package->domestictrackno; ?></td>
                    <td><?php echo $package->weight; ?></td>
                    <td><?php echo $package->remark; ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            </div>
        </div> <!-- end card-box -->

