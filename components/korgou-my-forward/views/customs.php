        <h4 class="section-title"><?php _e('Customs declaration', 'korgou'); ?></h4>
        <div class="card-box">

            <div class="table-responsive">
            <table class="table-bordered table table-hover">
                <thead class="thead-light">
                <tr>
                    <th><?php _e('Contents', 'korgou'); ?></th>
                    <th><?php _e('Quantity', 'korgou'); ?></th>
                    <th><?php _e('Value (in KRW or USD)', 'korgou'); ?></th>
                    <!--
                    <th>Net Weight(g)</th>
                    <th>HS Tariff Number</th>
                    -->
                </tr>
                </thead>
                <tbody>
                <?php foreach ($customs as $custom): ?>
                <tr>
                    <td><?php echo $custom->contents; ?></td>
                    <td><?php echo $custom->quantity; ?></td>
                    <td><?php echo $custom->value; ?></td>
                    <!--
                    <td><?php echo $custom->netweight; ?></td>
                    <td><?php echo $custom->hstariffnumber; ?></td>
                    -->
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            </div>
        </div> <!-- end card-box -->

