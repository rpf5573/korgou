<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-light">
                <tr>
                    <th><?php _e('Package forward order number', 'korgou'); ?></th>
                    <th><?php _e('Status', 'korgou'); ?></th>
                    <th><?php _e('Weight', 'korgou'); ?>(g)</th>
                    <th><?php _e('Total', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
                    <th><?php _e('Application Time', 'korgou'); ?></th>
                    <th><?php _e('Recipient', 'korgou'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($forwards->items as $forward): ?>
                <tr>
                    <td><a href="<?php echo add_query_arg('id', $forward->forwardid); ?>"><?php echo $forward->forwardid; ?></a></td>
                    <td>
                        <?php _e($forward->get_status_name(), 'korgou'); ?>
                        <?php if ($forward->status == '2'): ?>
                            <button type="button" class="btn btn-warning btn-xs pay-btn" data-id="<?php echo $forward->forwardid; ?>"><?php _e('Pay Now', 'korgou'); ?></button>
                        <?php endif; ?>
                    </td>
                    <td><?php echo number_format($forward->packageweight); ?></td>
                    <td><?php echo number_format($forward->totalfee); ?></td>
                    <td><?php echo $forward->packagetime; ?></td>
                    <td><?php echo $forward->englishname . ' | ' . $forward->nativename . ' | ' . $forward->country; ?></td>
                    <td>
                        <?php if ($forward->status == '1'): ?>
                            <button type="button" class="btn btn-danger btn-xs cancel-modal-btn" data-toggle="modal" data-target="#cancel-modal"
                                data-forwardid="<?php echo $forward->forwardid; ?>"><?php _e('Cancel', 'korgou'); ?></button>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            </div>

            <?php do_action('korgou_my_pagination', $forwards); ?>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="modal" tabindex="-1" role="dialog" id="cancel-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $this->ajax_form('request_cancel'); ?>
        <div class="modal-header">
            <h4 class="modal-title"><?php _e('Cancel', 'korgou'); ?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="forwardid" id="forwardid">
            <div class="form-group">
                <label for="exampleFormControlTextarea1"><?php _e('Cancel Reason', 'korgou'); ?></label>
                <textarea class="form-control" id="cancel-reason" name="cancel_reason" rows="5"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php _e('Close', 'korgou'); ?></button>
            <button type="button" class="btn btn-primary" id="cancel-btn"><?php _e('Cancel', 'korgou'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php $this->ajax_form('default_address'); ?>
    <input type="hidden" name="id" value="" id="default-id">
</form>

<?php $this->ajax_form('delete_address'); ?>
    <input type="hidden" name="id" value="" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.cancel-modal-btn').click(function() {
        $('#cancel-reason').val('');
        $('#forwardid').val($(this).data('forwardid'));
    });

    $('#cancel-btn').click(function() {
        var reason = $.trim($('#cancel-reason').val());
        if (reason == '') {
            alert('<?php _e('Please write a reason to cancel.', 'korgou'); ?>');
            return false;
        }

        $(this).closest('form').ajaxSubmit(function(response) {
            if (response.success) {
                location.reload();
            } else {
                alert(response.data);
                $('#cancel-modal').modal('hide');
            }
        });

        return false;
    });

    $('.pay-btn').click(function() {
        var id = $(this).data('id');
        KG.payNow('<?php _e('Are you sure to pay for this order?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('pay'); ?>',
            _wpnonce: '<?php $this->the_nonce('pay'); ?>',
            forwardid: id
        },
        location.href,
        {
            action: '<?php $this->the_tag('add_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_to_cart'); ?>',
            forwardid: id
        });
    });
});
</script>
