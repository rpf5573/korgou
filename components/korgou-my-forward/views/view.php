<div class="row mb-4">
    <div class="col-12">
        <?php include 'packages.php'; ?>
        <?php include 'detail.php'; ?>
        <?php include 'customs.php'; ?>
        <?php include 'valueadded.php'; ?>

        <a href="<?php echo remove_query_arg('id'); ?>" class="btn btn-secondary"><?php _e('Back', 'korgou'); ?></a>
    </div>
</div>
