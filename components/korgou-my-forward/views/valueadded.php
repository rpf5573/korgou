<h4 class="section-title"><?php _e('Your value-added service and other requirements', 'korgou'); ?></h4>
<div class="card-box">

    <div class="table-responsive">
    <table class="table table-bordered">
        <thead class="thead-light">
            <th><?php _e('Service Name', 'korgou'); ?></th>
            <th><?php _e('Service Fee', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
        </thead>
        <tbody>
            <?php foreach ($vas as $va): ?>
                <tr>
                    <td><?php echo "{$va->zhname} {$va->enname} {$va->koname}"; ?></td>
                    <td><?php echo number_format($va->price); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>

    <div class="table-responsive">
    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th><?php _e('Notes and other requirements', 'korgou'); ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo empty($forward->forwardcomment) ? 'N/A' : $forward->forwardcomment; ?></td>
        </tr>
    </tbody>
    </table>
    </div>

    <?php if (!empty($forward->remark)): ?>
        <div class="table-responsive">
        <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th><?php _e('Remark', 'korgou'); ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $forward->remark; ?></td>
            </tr>
        </tbody>
        </table>
        </div>
    <?php endif; ?>
</div>
