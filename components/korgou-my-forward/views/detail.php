<h4 class="section-title"><?php _e('Package forward order details', 'korgou'); ?></h4>

<div class="card-box">
    <div class="table-responsive">
        <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th class=""><?php _e('Forward No.', 'korgou'); ?></th>
                <th><?php _e('Status', 'korgou'); ?></th>
                <th><?php _e('Weight', 'korgou'); ?>(g)</th>
                <th><?php _e('Application Time', 'korgou'); ?></th>
                <th><?php _e('Forwarded Time', 'korgou'); ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $forward->forwardid; ?></td>
                <td><?php _e($forward->get_status_name()); ?></td>
                <td><?php echo number_format($forward->packageweight); ?>
                    <button type="button" class="btn btn-info btn-xs ml-2" data-toggle="modal" data-target="#items-modal">Detail</button>
                </td>
                <td><?php echo $forward->packagetime; ?></td>
                <td><?php echo $forward->forwardtime; ?></td>
            </tr>
        </tbody>
        </table>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th><?php _e('Shipping fee', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
                <th><?php _e('Processing fee', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
                <th><?php _e('Insurance fee', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
                <th><?php _e('Splitting fee', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
                <th><?php _e('Storage fee', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
                <th><?php _e('Re-packing fee', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
                <th><?php _e('Value-added service fee', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
                <th><?php _e('Total', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo number_format($forward->forwardfee); ?></td>
                <td><?php echo number_format($forward->servicefee); ?></td>
                <td><?php echo number_format($forward->insurancefee); ?></td>
                <td><?php echo number_format($forward->splittingfee); ?></td>
                <td><?php echo number_format($forward->get_storagefee_total()); ?>
                    <?php if (!empty($storages)): ?>
                        <button type="button" class="btn btn-info btn-xs ml-2" data-toggle="modal" data-target="#storage-modal">Detail</button>
                    <?php endif; ?>
                </td>
                <td><?php echo number_format($forward->repackingfee); ?></td>
                <td><?php echo number_format($forward->valueaddedfee); ?></td>
                <td><?php echo number_format($forward->totalfee); ?></td>
            </tr>
        </tbody>
        </table>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th><?php _e('Country/Area', 'korgou'); ?></th>
                <th><?php _e('Recipient', 'korgou'); ?></th>
                <th><?php _e('Destination', 'korgou'); ?></th>
                <th><?php _e('Company', 'korgou'); ?></th>
                <th><?php _e('Contact', 'korgou'); ?></th>
                <th><?php _e('Logistics', 'korgou'); ?></th>
                <th><?php _e('Track No.', 'korgou'); ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $forward->country; ?></td>
                <td><?php echo "{$forward->englishname}<br>{$forward->nativename}"; ?></td>
                <td><?php echo "{$forward->province} - {$forward->city} - {$forward->addressdetail}"; ?></td>
                <td><?php echo $forward->company; ?></td>
                <td><?php echo "{$forward->phonenum}<br>{$forward->mobilenum}"; ?></td>
                <td><?php echo $forward->forwardcouriername; ?></td>
                <td><?php echo $forward->forwardcouriertrackno; ?></td>
            </tr>
        </tbody>
        </table>
    </div>

</div>

<div class="modal fade" id="items-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?php _e('Weight and Shipping Fee', 'korgou'); ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
            ** <?php _e('DHL/FEDEX may change the amount due to changes in oil prices.', 'korgou'); ?>
        </p>

        <table class="table table-bordered">
            <thead class="thead-light">
                <th style="width: 20%;">#</th>
                <th style="width: 40%;"><?php _e('Weight', 'korgou'); ?>(g)</th>
                <th style="width: 40%;"><?php _e('Shipping Fee', 'korgou'); ?>(KRW)</th>
            </thead>
            <tbody>
                <?php
                $total_weight = 0;
                $total_fee = 0;
                
                for ($i = 0; $i < sizeof($items); $i++):
                    $total_weight += $items[$i]->weight;
                    $total_fee += $items[$i]->forwardfee;
                
                ?>
                    <tr>
                        <td><?php echo $i + 1; ?></td>
                        <td><?php echo number_format($items[$i]->weight); ?></td>
                        <td><?php echo number_format($items[$i]->forwardfee); ?></td>
                    </tr>
                <?php endfor; ?>
            </tbody>
            <tfooter>
                <th><?php _e('Total', 'korgou'); ?></th>
                <th><?php echo number_format($total_weight); ?></th>
                <th><?php echo number_format($total_fee); ?></th>
            </tfooter>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="storage-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?php _e('Storage fee', 'korgou'); ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
            <thead class="thead-light">
                <th style="width: 20%;">#</th>
                <th style="width: 40%;"><?php _e('Fee', 'korgou'); ?></th>
                <th style="width: 40%;"><?php _e('Time', 'korgou'); ?></th>
            </thead>
            <tbody>
                <?php
                for ($i = 0; $i < sizeof($storages); $i++):
                ?>
                    <tr>
                        <td><?php echo $i + 1; ?></td>
                        <td><?php echo $storages[$i]->fee; ?></td>
                        <td><?php echo $storages[$i]->createdtime; ?></td>
                    </tr>
                <?php endfor; ?>
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>