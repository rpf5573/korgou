<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_my_forward';

    function load()
    {
        $this->add_ajax('request_cancel');
        $this->add_ajax('pay');
        $this->add_ajax('add_to_cart');
        $this->add_shortcode();
        $this->add_action('process_payment', 10, 2);
    }

    function pay()
    {
        // Shoplic_Util::display_errors();
        if (!isset($_REQUEST['forwardid']) || empty($_REQUEST['forwardid'])) {
            wp_send_json_error('Forward is not available');
        }

        $userid = KG::get_current_userid();
        $forwardid = $_REQUEST['forwardid'];

        $this->process_payment($userid, $forwardid);

        kg_send_json_success();
    }

    function process_payment($userid, $forwardid)
    {
        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
        if ($forward == null || $forward->userid != $userid) {
            throw new RuntimeException('Forward is not available');
        }
        if ($forward->status == Korgou_Forward::STATUS_PAID) {
            throw new RuntimeException('Forward has been paid already.');
        }

        do_action('korgou_user_balance_check_balance', - $forward->totalfee);

        do_action('korgou_user_balance_change_balance',
            $forwardid, $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $forward->totalfee, 'KRW', 'Package forward', '', '', 'sys', 'sys', ''
        ); 

        do_action('korgou_forward_update_forward_status', $forwardid, Korgou_Forward::STATUS_PAID);
    }


    function add_to_cart()
    {
        Shoplic_Util::display_errors();
        if (!isset($_REQUEST['forwardid']) || empty($_REQUEST['forwardid'])) {
            wp_send_json_error('Forward is not available');
        }

        $userid = KG::get_current_userid();

        $forwardid = $_REQUEST['forwardid'];
        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
        if ($forward == null || $forward->userid != $userid) {
            wp_send_json_error('Forward is not available');
        }

        do_action('korgou_my_payment_add_to_cart', $userid, $forward->totalfee, 'forward', ['forwardid' => $forwardid]);

        wp_send_json_success();
    }

    function request_cancel()
    {
        $data = Shoplic_Util::get_post_data('forwardid', 'cancel_reason');
        $data['status'] = Korgou_Forward::STATUS_CANCEL_REQUESTED;

        do_action('korgou_forward_update_forward', $data);

        wp_send_json_success();
    }

    function shortcode()
    {
        $id = $_GET['id'] ?? 0;

        if ($id) {
            $view = 'view';
            $goods_list = [];
            $forward = apply_filters('korgou_forward_get_forward', null, [
                'userid' => KG::get_current_userid(),
                'forwardid' => $id,
            ]);
            $context = [
                'forward' => $forward,
            ];
            if ($forward != null) {
                $packages = [];
                foreach (explode(',', $forward->packagelist) as $packageid) {
                    $package = apply_filters('korgou_package_get_package', null, ['packageid' => $packageid]);
                    if ($package != null)
                        $packages[] = $package;
                }
                $context['packages'] = $packages;
                $context['customs'] = apply_filters('korgou_forward_get_customs_declaration_list', [], ['forwardid' => $forward->forwardid]);
                $context['vas'] = apply_filters('korgou_forward_get_value_added_forward_list', [], ['forwardid' => $forward->forwardid]);
                $context['storages'] = apply_filters('korgou_forward_get_storage_list', [], ['forwardid' => $forward->forwardid]);
                $context['items'] = apply_filters('korgou_forward_get_item_list', [], ['forwardid' => $forward->forwardid]);
            }
        } else {
            $view = 'list';
            $paged = max( 1, get_query_var('paged') );
            $rowcount = $_GET['rowcount'] ?? 10;
            $context = [
                'forwards' => apply_filters('korgou_forward_get_forward_page', [], [
                    'userid' => KG::get_current_userid(),
                ], null, null, $rowcount, $paged),
            ];
        }

        $this->view($view, $context);
    }
};
