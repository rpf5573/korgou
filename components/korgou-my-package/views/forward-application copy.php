<!-- CSS -->
<link href="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/css/smart_wizard_all.min.css" rel="stylesheet" type="text/css" />
<!-- JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/js/jquery.smartWizard.min.js" type="text/javascript"></script>

<?php $this->ajax_form('package_apply_forward'); ?>

<div class="row">
    <div class="col-12">
        <div class="card-box">

            <div id="smartwizard">
                <ul class="nav">
                <li>
                    <a class="nav-link" href="#step-1">
                        <div class="font-weight-bold">Step 1</div>
                        Options & Services
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#step-2">
                        <div class="font-weight-bold">Step 2</div>
                        Address
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#step-3">
                        <div class="font-weight-bold">Step 3</div>
                        Customs declaration
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="#step-4">
                        <div class="font-weight-bold">Step 4</div>
                        Confirm
                    </a>
                </li>
                </ul>
            
                <div class="tab-content">
                <div id="step-1" class="tab-pane" role="tabpanel">
                    <?php include 'forward-application/options.php'; ?>
                </div>
                <div id="step-2" class="tab-pane" role="tabpanel">
                    <?php include 'forward-application/address.php'; ?>
                </div>
                <div id="step-3" class="tab-pane" role="tabpanel">
                    <?php include 'forward-application/customs.php'; ?>
                    Step content
                </div>
                <div id="step-4" class="tab-pane" role="tabpanel">
                    <?php include 'forward-application/confirm.php'; ?>
                </div>
            </div>
        </div>

<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="card-title"><?php _e('Packages selected for forward', 'korgou'); ?></h4>
            
            <div class="table-responsive">
            <table class="table-bordered table table-hover">
                <thead class="thead-light">
                <tr>
                    <th><?php _e('Package No.', 'korgou'); ?></th>
                    <th><?php _e('Arrival time', 'korgou'); ?></th>
                    <th><?php _e('Package source', 'korgou'); ?></th>
                    <th><?php _e('Domestic courier', 'korgou'); ?></th>
                    <th><?php _e('Tracking number', 'korgou'); ?></th>
                    <th><?php _e('Items in package', 'korgou'); ?></th>
                    <th><?php _e('Weight', 'korgou'); ?>(g)</th>
                    <th><?php _e('Remarks', 'korgou'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($packages as $package): ?>
                <input type="hidden" name="packageid[]" value="<?php echo $package->packageid; ?>">
                <tr>
                    <td><?php echo $package->packageid; ?></td>
                    <td><?php echo $package->arrivaltime; ?></td>
                    <td><?php echo $package->packagesource; ?></td>
                    <td><?php echo $package->domesticcourier; ?></td>
                    <td><?php echo $package->domestictrackno; ?></td>
                    <td><?php echo $package->packagecontent; ?></td>
                    <td><?php echo $package->weight; ?></td>
                    <td><?php echo $package->remark; ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            </div>
        </div> <!-- end card-box -->

        <div class="card bg-warning text-center h3 p-3 mb-3">
            <a href="" class="text-white" target="_blank"><?php _e('Please make sure that you have read and understood the <u>KorGou Package Forwarding Service Instructions</u> before filling out the application forms.', 'korgou'); ?></a>
        </div> <!-- end card-box -->

        <div class="card-box border border-danger">
            <h4 class="card-title"><?php _e('Choose processing option', 'korgou'); ?></h4>
            <div class="form-group row">
                <div class="col-lg-10">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="processing" id="processing-normal" value="N" checked>
                        <label class="form-check-label" for="processing-normal"><?php _e('Normal', 'korgou'); ?></label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="processing" id="processing-priority" value="P">
                        <label class="form-check-label" for="processing-priority"><?php _e('Priority', 'korgou'); ?></label>
                    </div>
                </div>

            </div>
            <ul class="mt-2 pl-3">
                <li>
                    <?php _e('Packaging normally takes about three to four days as of the application date, but if you apply for PRIORITY, the packing will be completed within one day of the application date except on weekends and holidays.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('The processing fee will be 4,000 KRW/kg.', 'korgou'); ?>
                </li>
            </ul>
        </div>

        <div class="card-box border">
            <h4 class="card-title"><?php _e('Choose packing method option', 'korgou'); ?></h4>
            <div class="form-group row">
                <div class="col-lg-10">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="packing" id="packing-normal" value="N" checked>
                        <label class="form-check-label" for="packing-normal"><?php _e('Normal', 'korgou'); ?></label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="packing" id="packing-premium" value="P">
                        <label class="form-check-label" for="packing-premium"><?php _e('Premium re-packing(one by one)', 'korgou'); ?></label>
                    </div>
                </div>

            </div>
            <ul class="mt-2 pl-3">
                <li>
                    <?php _e('If you want to pack your packages with bubblewrap each for safe delivery, please choose PREMIUM RE-PACKING.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('The cost will be added 1,000 won/kg starting from 4,000 won.', 'korgou'); ?>
                </li>
            </ul>
        </div>
        <div class="card-box">
            <h4 class="card-title"><?php _e('Choose forward courier', 'korgou'); ?></h4>
            <div class="form-group row">
                <label class="col-lg-2 col-form-label"><?php _e('Forward through', 'korgou'); ?></label>
                <div class="col-lg-4">
                    <select name="forwardcourierid" class="form-control">
                        <option value="">--- <?php _e('Choose', 'korgou'); ?> ---</option>
                        <?php foreach ($couriers as $courier): ?>
                            <option value="<?php echo $courier->id; ?>"><?php echo $courier->enname; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="card-box" id="forward-address">
            <?php require 'forward-application/address.php'; ?>
        </div> <!-- end card-box -->
        
        <div class="card-box">
            <h4 class="card-title"><?php _e('Fill in the Customs declaration', 'korgou'); ?></h4>
            
            <table class="table-bordered table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th style="width: 40%;"><?php _e('Contents', 'korgou'); ?></th>
                    <th><?php _e('Quantity', 'korgou'); ?></th>
                    <th><?php _e('Total Value(in KRW or USD)', 'korgou'); ?></th>
                    <!--
                    <th>Net Weight(g)</th>
                    <th>HS Tariff Number</th>
                    -->
                    <th style="min-width: 63px;"></th>
                </tr>
                </thead>
                <tbody id="customs">
                <tr>
                    <td class="align-middle">1</td>
                    <td>
                        <input type="text" class="form-control" name="contents[]">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="quantity[]">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="value[]">
                    </td>
                <!--
                    <td>
                        <input type="text" class="form-control" name="netweight[]">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="hstariffnumber[]">
                    </td>
                -->
                    <td>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="text-right">
                <button type="button" id="add-customs-btn" class="btn btn-info"><?php _e('Add Input Box', 'korgou'); ?></button>
            </div>
        </div> <!-- end card-box -->

        <div class="card-box">
            <h4 class="card-title"><?php _e('Please choose your desired value-added service', 'korgou'); ?></h4>

            <?php foreach ($value_addeds as $va): ?>
                <?php if (in_array($va->type, ['ITEM_PHOTOGRAPHY', 'ITEM_SORTING', 'RETURN_EXCHANGE'])) continue; ?>
                <div class="form-group row">
                    <div class="col">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="va-<?php echo $va->type; ?>" name="valueaddedservice[]" value="<?php echo $va->type; ?>">
                            <label class="form-check-label" for="va-<?php echo $va->type; ?>">
                                <?php echo $va->zhname . ' ' . $va->enname . ' ' . $va->koname . ' - ' . $va->price . ' ' . $va->currency; ?>
                            </label>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <div class="form-group row">
                <div class="col-lg-6">
                    <label for=""><?php _e('Other special requirements concerning forwarding and packaging', 'korgou'); ?></label>
                    <textarea class="form-control" id="" rows="5" name="forwardcomment"></textarea>    
                </div>
            </div>
        </div>

    </div> <!-- end col -->
</div>

<p class="text-center">
    <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-secondary mr-2"><?php _e('Cancel', 'korgou'); ?></a>
    <button type="button" id="submit-btn" class="btn btn-primary mr-2"><?php _e('Complete application form, Submit forward application', 'korgou'); ?></button>
</p>

</form>

<script type="text/template" id="customs-template">
    <tr>
        <td class="align-middle">1</td>
        <td>
            <input type="text" class="form-control" name="contents[]">
        </td>
        <td>
            <input type="text" class="form-control" name="quantity[]">
        </td>
        <td>
            <input type="text" class="form-control" name="value[]">
        </td>
        <!--
        <td>
            <input type="text" class="form-control" name="netweight[]">
        </td>
        <td>
            <input type="text" class="form-control" name="hstariffnumber[]">
        </td>
        -->
        <td class="align-middle">
            <button type="button" class="btn btn-danger btn-xs delete-customs-btn"><i class="fe-trash"></i></button>
        </td>
    </tr>
</script>

<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.submit(() => {
        return false;
    });
    $btn.click(() => {
        $form.ajaxSubmit((response) => {
            if (response.success) {
                alert('<?php _e('Successful operation'); ?>');
                location.href = '<?php echo home_url('/my/packages/'); ?>';
            } else {
                alert(response.data);
            }
        });
    });
    function renumberCustoms() {
        $('#customs tr').each(function(index) {
            $(this).children().first().text(index+1);
        });
    }
    function setAddress() {
        /*
        $('#address-tb input[type="radio"]').each(function() {
            if (this.checked)
                $(this).closest('tr').addClass('font-weight-bold');
            else 
                $(this).closest('tr').removeClass('font-weight-bold');
        });
        */
    }
    setAddress();
    $('#address-tb tr').click(function() {
        $(this).find('input[type="radio"]').click();
    });
    $('#address-tb input[type="radio"]').click(function() {
        var $tr = $(this).closest('tr');
        $('[data-target]', $tr).each(function() {
            $('[name="' + $(this).data('target') + '"]').val($(this).text());
        });
        setAddress();
        return false;
    });
    $('#new-address-btn').click(function() {
        $('#address-fields input').val('');
        $('#address-fields select').val('');
        $('#address-tb input[type="radio"]:checked').prop('checked', false);
        setAddress();
        return false;
    });
    $('#add-customs-btn').click(function() {
        $('#customs').append($('#customs-template').html());
        renumberCustoms();
        return false;
    });
    $('#customs').on('click', '.delete-customs-btn', function() {
        $(this).closest('tr').remove();
        renumberCustoms();
        return false;
    });

    $('#smartwizard').smartWizard({
        theme: 'dots',
        autoAdjustHeight: false
    });
});
</script>
