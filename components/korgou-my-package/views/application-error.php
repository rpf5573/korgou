<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="table-responsive">
                <table class="table-bordered table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th><?php _e('Package No.', 'korgou'); ?></th>
                        <th><?php _e('Tracking number', 'korgou'); ?></th>
                        <th><?php _e('Weight', 'korgou'); ?>(g)</th>
                        <th><?php _e('Arrival time', 'korgou'); ?></th>
                        <th><?php _e('Status', 'korgou'); ?></th>
                        <th><?php _e('Remarks', 'korgou'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($packages as $package): ?>
                    <tr>
                        <td><?php echo $package->packageid; ?></td>
                        <td><?php echo $package->domestictrackno; ?></td>
                        <td><?php echo $package->weight; ?></td>
                        <td><?php echo $package->arrivaltime; ?></td>
                        <td class="<?php if ($package->status != Korgou_Package::STATUS_IN_REPO) echo 'text-danger'; ?>">
                            <?php _e($package->get_status_name(), 'korgou'); ?>
                        </td>
                        <td><?php echo $package->remark; ?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <p class="text-center">
                <b class="text-danger h4"><?php _e('This is a package that has already been applied or cannot be applied due to being expired.', 'korgou'); ?></b>
                <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-secondary"><?php _e('Back'); ?></a>
            </p>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
