<div class="row">
    <div class="col-12">
        <div class="card-box">
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="input-trackno" class="col-form-label"><?php _e('Package ID', 'korgou'); ?></label>
                        <p class="p-2 border"><?php echo $package->packageid; ?></p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="input-cargodetail" class="col-form-label"><?php _e('Disposal status', 'korgou'); ?></label>
                        <p class="p-2 border"><?php echo Korgou_Package::$DISPOSALS[$package->disposal] ?? '&nbsp;'; ?></p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="input-cargodetail" class="col-form-label"><?php _e('Disposal notes', 'korgou'); ?></label>
                        <p class="p-2 border"><?php echo $disposal->cargodetail ?? '&nbsp;'; ?></p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="input-cargodetail" class="col-form-label"><?php _e('Item disposal remarks', 'korgou'); ?></label>
                        <p class="p-2 border" style="min-height: 47px;"><?php echo $disposal->checkremark ?? '&nbsp;'; ?></p>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-6">
                        <label for="input-cargodetail" class="col-form-label"><?php _e('Disposal fee', 'korgou'); ?></label>
                        <p class="p-2 border"><?php echo empty($disposal->disposalfee) ? '&nbsp;' : number_format($disposal->disposalfee) . ' KRW'; ?></p>
                    </div>
                </div>

                <?php if (!empty($user_images)): ?>
                    <div class="mb-2">
                        <label class="col-form-label">Attachments</label>
                        <br>
                        <?php do_action('korgou_package_show_image', $user_images); ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($images)): ?>
                    <div class="mb-4">
                        <label class="col-form-label">Korgou Uploads</label>
                        <br>
                        <?php do_action('korgou_package_show_image', $images); ?>
                    </div>
                <?php endif; ?>

                <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-secondary"><?php _e('Back', 'korgou'); ?></a>
                <?php if ($package->disposal == Korgou_Package::DISPOSAL_CONFIRMED): ?>
                    <button type="button" class="btn btn-warning pay-btn" data-packageid="<?php echo $package->packageid; ?>"><?php _e('Pay Now', 'korgou'); ?></button>
                <?php endif; ?>
                <?php if (in_array($package->disposal, [Korgou_Package::DISPOSAL_PENDING, Korgou_Package::DISPOSAL_CONFIRMED])): ?>
                    <button type="button" class="btn btn-light cancel-btn" data-packageid="<?php echo $package->packageid; ?>"><?php _e('Cancel', 'korgou'); ?></button>
                <?php endif; ?>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
    $(document).on('click', '.cancel-btn', function() {
        if (confirm('<?php _e('Are you sure?', 'korgou'); ?>')) {
            $.post('/wp-admin/admin-ajax.php', {
                action: '<?php $this->the_tag('cancel_disposal'); ?>',
                _wpnonce: '<?php $this->the_nonce('cancel_disposal'); ?>',
                packageid: $(this).data('packageid')
            }, function(response) {
                if (response.success) {
                    location.href = '<?php echo home_url('/my/packages/'); ?>';
                } else {
                    alert(response.data);
                }
            });
        }
        return false;
    });
    $(document).on('click', '.pay-btn', function() {
        KG.payNow('<?php _e('This is paid service and the corresponding amount will be deducted from your account. Are you sure to apply for this value-added service?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('pay_disposal'); ?>',
            _wpnonce: '<?php $this->the_nonce('pay_disposal'); ?>',
            packageid: $(this).data('packageid')
        },
        location.href,
        {
            action: '<?php $this->the_tag('add_disposal_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_disposal_to_cart'); ?>',
            packageid: $(this).data('packageid')
        });
        return false;
    });
    
});
</script>
