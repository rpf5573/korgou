<style>
    .text-red-1 {
        color: #B1170D;
    }
    .text-red-2 {
        color: #EF2260;
    }
</style>

<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="form-row mb-2">
                <div class="col-md-6">
                    <label for="input-trackno" class="col-form-label"><?php _e('Package ID', 'korgou'); ?></label>
                    <input type="text" class="form-control-plaintext border px-2" id="input-packageid" name="packageid" value="<?php echo $package->packageid; ?>" readonly>
                </div>
            </div>
            <p class="text-red-2">
                <b class="text-red-1"><?php _e('The Basic Cost of the Disposal Service is KRW 10,000.', 'korgou'); ?></b>
                <br>
                <?php _e('If you want to separate and discard some items in the package,', 'korgou'); ?>
                <b class="text-red-1"><?php _e('an Additional Cost will be Incurred.', 'korgou'); ?></b>
                <br>
                <?php _e('<u>The additional service fee of KRW 1,000 per piece</u> will be charged depending on the quantity of items included in the package.', 'korgou'); ?>
            </p>
            <p><i>
                <?php _e('Example)', 'korgou'); ?>
                <br>
                <?php _e('If you discard the album out of 10 items and want only the photo card, the total service cost is 20000 won.', 'korgou'); ?>
                <br>
                <?php _e('Basic cost: 10,000 won', 'korgou'); ?>
                <br>
                <?php _e('Additional cost: 10,000 won (1,000 * 10ea)', 'korgou'); ?>
                <br>
                <?php _e('Total cost: 20,000 won', 'korgou'); ?>
            </i></p>
            <div class="form-row mb-4">
                <div class="col-md-6">
                    <label for="input-cargodetail" class="col-form-label"><?php _e('Disposal notes', 'korgou'); ?></label>
                    <textarea rows="5" class="form-control" id="input-cargodetail" name="cargodetail" required></textarea>
                </div>
            </div>
            <div class="form-row mb-4">
                <div class="col-md-6">
                    <label for="input-images" class="col-form-label"><?php _e('Attachments', 'korgou'); ?> (png, jpg or gif)</label>
                    <?php $this->upload_image_form(Korgou_Package_Image::IMAGETYPE_USER_DISPOSAL); ?>
                </div>
            </div>

            <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-secondary"><?php _e('Cancel', 'korgou'); ?></a>
            <button type="button" id="submit-btn" class="btn btn-primary waves-effect waves-light"><?php _e('Apply item disposal service', 'korgou'); ?></button>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
    $('#submit-btn').click(function() {
        if ($('#input-cargodetail').val() == '') {
            alert('<?php _e('Notes required', 'korgou'); ?>')
            return false;
        }

        var imageids = [];
        $('input[name="imageid"]').each(function() {
            imageids.push($(this).val());
        });

        KG.payNow('<?php _e('This is paid service and the corresponding amount will be deducted from your account. Are you sure to apply for this value-added service?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('apply_disposal'); ?>',
            _wpnonce: '<?php $this->the_nonce('apply_disposal'); ?>',
            packageid: $('#input-packageid').val(),
            cargodetail: $('#input-cargodetail').val(),
            imageids: imageids.join(',')
        },
        '<?php echo home_url('/my/packages/'); ?>', {});
        return false;
    })
});
</script>
