<div class="row">
    <div class="col-md-2">
        <h4 class="card-title"><?php _e('Cart total', 'korgou'); ?></h4>
    </div>
    <div class="col-md-10">
        <label><?php printf(_n('%d package', '%d packages', sizeof($packages), 'korgou') . ' / %sg', sizeof($packages), number_format($total_weight)); ?></label>
        <p class="text-warning"><?php _e('In case of actual packing, the total weight may change due to the provision of consolidate service.', 'korgou'); ?></p>
    </div>
</div>

<h4 class="card-title mt-3"><?php _e('Choose options', 'korgou'); ?></h4>
<fieldset id="options-fs">
    <div class="form-group row">
        <label class="col-md-2 col-form-label"><?php _e('Forward courier', 'korgou'); ?></label>
        <div class="col-md-4">
            <select name="forwardcourierid" id="select-courier" class="form-control">
                <option value="">--- <?php _e('Choose', 'korgou'); ?> ---</option>
                <?php foreach ($couriers as $courier): ?>
                    <option value="<?php echo $courier->id; ?>"><?php echo $courier->enname; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div id="select-courier-notice" class="form-group row" style="display: none;">
        <div class="offset-md-2 col-md-10 text-danger">
            <b>
                <!-- <?php _e('Regarding domestic shipping, Receiver should be individual not companies like other shipping companies. If not, the forward order can be denied by Korgou.', 'korgou'); ?> -->
                <?php _e('Consolidation and Premium Re-packaging services are not available for Domestic delivery.', 'korgou'); ?>
            </b>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label pt-0"><?php _e('Processing', 'korgou'); ?></label>
        <div class="col-md-10">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="processing" id="processing-normal" value="N" checked>
                <label class="form-check-label" for="processing-normal"><?php _e('Normal', 'korgou'); ?></label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="processing" id="processing-priority" value="P">
                <label class="form-check-label" for="processing-priority"><?php _e('Priority', 'korgou'); ?></label>
            </div>

            <ul class="mt-2 pl-3">
                <li>
                    <?php _e('Packaging normally takes about three to four days as of the application date, but if you apply for PRIORITY, the packing will be completed within one day of the application date except on weekends and holidays.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('The processing fee will be 4,000 KRW/kg.', 'korgou'); ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label pt-0"><?php _e('Packing method', 'korgou'); ?></label>
        <div class="col-md-10">
            <div class="form-check">
                <input class="form-check-input option-check" data-option="repacking" type="checkbox" name="repacking" id="packing-premium" value="Y">
                <label class="form-check-label" for="packing-premium"><?php _e('Premium re-packing', 'korgou'); ?></label>
            </div>
            <ul class="mt-2 pl-3">
                <li class="text-danger">
                    <b><?php _e('If you want to remove the original box and pack your packages with bubble wrap for cost effective and safe delivery, please choose PREMIUM REPACKING.', 'korgou'); ?></b>
                </li>
                <li>
                    <?php _e('The cost will be added 1,000 won/kg starting from 4,000 won.', 'korgou'); ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label pt-0"><?php _e('Insurance', 'korgou'); ?></label>
        <div class="col-md-10">
            <div class="form-check">
                <input class="form-check-input option-check" data-option="insurance" type="checkbox" name="insurance" id="input-insurance" value="Y">
                <label class="form-check-label" for="input-insurance">
                <?php _e('Shipping insurance(10,000won for EMS,FEDEX,TNT and AIR&SEA PARCEL / 15,000won for DHL)', 'korgou'); ?>
                </label>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label pt-0"><?php _e('Value added service', 'korgou'); ?></label>
        <div class="col-md-10">
            <?php foreach ($value_addeds as $va): ?>
                <?php if (in_array($va->type, ['ITEM_PHOTOGRAPHY', 'ITEM_SORTING', 'RETURN_EXCHANGE'])) continue; ?>
                <div class="form-check mb-1">
                    <input class="form-check-input option-check" data-option="<?php echo $va->type; ?>" type="checkbox" id="va-<?php echo $va->type; ?>" name="valueaddedservice[]" value="<?php echo $va->type; ?>">
                    <label class="form-check-label" for="va-<?php echo $va->type; ?>">
                        <?php _e($va->enname, 'korgou');  echo '(' . number_format($va->price) . ' ' . $va->currency . ')'; ?>
                    </label>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label"><?php _e('Other special requirements', 'korgou'); ?></label>
        <div class="col-md-10">
            <textarea class="form-control" id="" rows="5" name="forwardcomment" maxlength="200"></textarea>    
            <div class="row">
                <div class="col">
                    <?php _e('Special requirements concerning forwarding and packaging', 'korgou'); ?>
                </div>
                <div class="col text-right">
                    <span id="forwardcomment-chars">0</span>/200
                </div>
            </div>
        </div>
    </div>
</fieldset>

<div class="sw-toolbar text-right" role="toolbar">
    <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-link text-dark cancel-btn"><?php _e('Cancel', 'korgou'); ?></a>
    <button class="btn btn-info sw-btn-prev" type="button"><?php _e('Previous', 'korgou'); ?></button>
    <button class="btn btn-info" id="next-step2-btn" type="button"><?php _e('Next', 'korgou'); ?></button>
</div>

<script type="text/javascript">
jQuery(function($) {
    $(window).on('checkOrder', function() {
        $('#option-courier').text($('select[name="forwardcourierid"] option:selected').text());
        $('#option-processing').text($('input[name="processing"]:checked').next().text());
        $('.option-check').each(function() {
            var $option = $('#option-'+$(this).data('option'));
            if ($(this).is(':checked'))
                $option.show();
            else
                $option.hide();
        });
        $('#option-forwardcomment').html($.trim($('textarea[name="forwardcomment"]').val()).replace(/\n/g, '<br>'));
    });

    $('#next-step2-btn').click(function() {
        if ($('select[name="forwardcourierid"]').val() == '') {
            alert('<?php _e('Please choose a forward courier.'); ?>');
            return false;
        } else {
            $('#smartwizard').smartWizard("goToStep", 1);
        }
    });
    $('textarea[name="forwardcomment"]').keyup(function() {
        $('#forwardcomment-chars').text($(this).val().length);
    });

    $('#select-courier').change(function() {
        if ($(this).val() == '8') {
            $('#select-courier-notice').show();
        } else {
            $('#select-courier-notice').hide();
        }
    });
});
</script>
