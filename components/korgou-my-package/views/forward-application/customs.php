<h4 class="card-title"><?php _e('Fill in the Customs declaration', 'korgou'); ?></h4>
<div class="table-responsive">
<table class="table-bordered table">
    <thead class="thead-light">
    <tr>
        <th>#</th>
        <th style="width: 40%;"><?php _e('Contents', 'korgou'); ?></th>
        <th><?php _e('Quantity', 'korgou'); ?></th>
        <th><?php _e('Total Value(in USD)', 'korgou'); ?></th>
        <!--
        <th>Net Weight(g)</th>
        <th>HS Tariff Number</th>
        -->
        <th style="min-width: 63px;"></th>
    </tr>
    </thead>
    <tbody id="customs">
    <tr>
        <td class="align-middle">1</td>
        <td>
            <input type="text" class="form-control" name="contents[]">
        </td>
        <td>
            <input type="number" class="form-control" name="quantity[]">
        </td>
        <td>
            <input type="number" class="form-control" name="value[]">
        </td>
    <!--
        <td>
            <input type="text" class="form-control" name="netweight[]">
        </td>
        <td>
            <input type="text" class="form-control" name="hstariffnumber[]">
        </td>
    -->
        <td>
        </td>
    </tr>
    </tbody>
</table>
<p class="text-danger"><b>
    <?php _e('If you do not enter the relevant information, delivery will not be processed. It cannot be entered as an unknown item such as "Product" or "Gift".', 'korgou'); ?>
</b></p>
</div>

<div>
    <button type="button" id="add-customs-btn" class="btn btn-success"><?php _e('Add Input Box', 'korgou'); ?></button>
</div>

<div class="sw-toolbar text-right" role="toolbar">
    <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-link text-dark cancel-btn"><?php _e('Cancel', 'korgou'); ?></a>
    <button class="btn btn-info sw-btn-prev" type="button"><?php _e('Previous', 'korgou'); ?></button>
    <button class="btn btn-info sw-btn-next- check-order-btn" id="next-step4-btn" type="button"><?php _e('Next', 'korgou'); ?></button>
</div>

<script type="text/template" id="customs-template">
    <tr>
        <td class="align-middle">1</td>
        <td>
            <input type="text" class="form-control" name="contents[]">
        </td>
        <td>
            <input type="text" class="form-control" name="quantity[]">
        </td>
        <td>
            <input type="text" class="form-control" name="value[]">
        </td>
        <!--
        <td>
            <input type="text" class="form-control" name="netweight[]">
        </td>
        <td>
            <input type="text" class="form-control" name="hstariffnumber[]">
        </td>
        -->
        <td class="align-middle">
            <button type="button" class="btn btn-danger btn-xs delete-customs-btn"><i class="fe-trash"></i></button>
        </td>
    </tr>
</script>

<script type="text/javascript">
jQuery(function($) {
    function alertCustoms() {
        alert('<?php _e('Please fill in the details correctly to proceed with the shipment.', 'korgou'); ?>\n<?php _e('Otherwise, the delivery process will not take place.', 'korgou'); ?>');
    }
    function renumberCustoms() {
        $('#customs tr').each(function(index) {
            $(this).children().first().text(index+1);
        });
    }
    $('#add-customs-btn').click(function() {
        $('#customs').append($('#customs-template').html());
        renumberCustoms();
        return false;
    });
    $('#customs').on('click', '.delete-customs-btn', function() {
        $(this).closest('tr').remove();
        renumberCustoms();
        return false;
    });
    $('#next-step4-btn').click(function(e) {
        var valid = true;
        $('input[name="contents[]"]').each(function() {
            var val = $.trim($(this).val());
            var match = val.match(/[a-zA-Z ]/g);
            if (!match || match.length < 2) {
                valid = false;
                return false;
            }
        });
        if (!valid) {
            alertCustoms();
            return false;
        }
        $('input[name="quantity[]"], input[name="value[]"]').each(function() {
            var val = $.trim($(this).val());
            if (val.length == 0) {
                valid = false;
                return false;
            }
        });
        if (!valid) {
            alertCustoms();
            return false;
        }

        $(window).trigger('checkOrder');
        $('#smartwizard').smartWizard("goToStep", 3);
        return false;
    });
    $(window).on('checkOrder', function() {
        $('#option-customs').html('');
        $('#customs tr').each(function() {
            if ($(this).find('input').eq(0).val() != '') {
                var $tr = $('<tr>');
                $tr.append('<td>' + $(this).children().eq(0).text() + '</td>');
                $(this).find('input').each(function() {
                    $tr.append('<td>' + $(this).val() + '</td>');
                });
                console.log($tr.html());
                $('#option-customs').append($tr);
            }
        });
    });

});
</script>

