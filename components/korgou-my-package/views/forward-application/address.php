<h4 class="card-title"><?php _e('Enter the recipient information', 'korgou'); ?></h4>
<p class="text-danger">
<?php _e('Please enter the address information in English. However, address in China, Hong Kong and Taiwan can be input in Chinese.', 'korgou'); ?>
</p>

<?php if (!empty($addresses)): ?>
    <div class="table-responsive">

    <table class="table table-sm table-bordered table-hover" id="address-tb">
        <thead>
        <tr>
            <th style="min-width: 24px;"></th>
            <th><?php _e('English Name', 'korgou'); ?></th>
            <th><?php _e('Native Name', 'korgou'); ?></th>
            <th><?php _e('Country', 'korgou'); ?></th>
            <th><?php _e('Zip code', 'korgou'); ?></th>
            <th><?php _e('City', 'korgou'); ?></th>
            <th><?php _e('Province', 'korgou'); ?></th>
            <th><?php _e('Address Detail', 'korgou'); ?></th>
            <th><?php _e('Company', 'korgou'); ?></th>
            <th class="d-none1"><?php _e('Phone Number', 'korgou'); ?></th>
            <th class="d-none1"><?php _e('Mobile Number', 'korgou'); ?></th>
            <!--
            <th style="width: 60px;"></th>
            -->
        </tr>
        </thead>
        <tbody>
        <?php
        $default = null;
        foreach ($addresses as $addr):
        ?>
        <tr>
            <td class="align-center">
                <input type="radio" class="form-check-input ml-0" style="margin-top: -0.3em;" name="use_address" value="<?php echo $addr->id; ?>"
                <?php
                    if ($addr->asdefault == '1') {
                        $default = $addr;
                        echo 'checked';
                    }
                    ?>
                >
            </td>
            <td data-target="englishname"><?php echo $addr->englishname; ?></td>
            <td data-target="nativename"><?php echo $addr->nativename; ?></td>
            <td data-target="country"><?php echo $addr->country; ?></td>
            <td data-target="zipcode"><?php echo $addr->zipcode; ?></td>
            <td data-target="city"><?php echo $addr->city; ?></td>
            <td data-target="province"><?php echo $addr->province; ?></td>
            <td data-target="addressdetail"><?php echo $addr->addressdetail; ?></td>
            <td data-target="company"><?php echo $addr->company; ?></td>
            <td data-target="phonenum" class="d-none1"><?php echo $addr->phonenum; ?></td>
            <td data-target="mobilenum" class="d-none1"><?php echo $addr->mobilenum; ?></td>
            <!--
            <td>
                <a href="#" class="btn btn-link delete-address-btn" data-id="<?php echo $addr->id; ?>"><i class="fe-trash-2"></i>
            </td>
            -->
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    <hr>
<?php endif; ?>

<?php if ($default == null) $default = new Korgou_User_Address; ?>

<fieldset id="address-fields">
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('Full Name(in English)', 'korgou'); ?></label>
        <div class="col-lg-4">
            <input type="text" class="form-control" name="englishname" value="<?php echo $default->englishname; ?>">
            <p class="form-text"><?php _e('Capitalized letters in English', 'korgou'); ?></p>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('Full Name (in the native language)', 'korgou'); ?></label>
        <div class="col-lg-4">
            <input type="text" class="form-control" name="nativename" value="<?php echo $default->nativename; ?>">
            <p class="form-text"><?php _e('In the language of the recipient country', 'korgou'); ?></p>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('Country', 'korgou'); ?></label>
        <div class="col-lg-4">
            <?php do_action('korgou_select_country', $default->country); ?>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('Zip / Postal Code', 'korgou'); ?></label>
        <div class="col-lg-4">
            <input type="text" class="form-control" name="zipcode" value="<?php echo $default->zipcode; ?>">
            <p class="form-text">
                <?php _e('Please enter 000 if you don\'t have a Zip Code.', 'korgou'); ?><br>
                <b class="text-danger">
                    <?php _e('Chinese address is not allowed.', 'korgou'); ?>
                </b>
            </p>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('State / Province / Region', 'korgou'); ?></label>
        <div class="col-lg-4">
            <input type="text" class="form-control" name="province" value="<?php echo $default->province; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('City', 'korgou'); ?></label>
        <div class="col-lg-4">
            <input type="text" class="form-control" name="city" value="<?php echo $default->city; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('Address detail', 'korgou'); ?></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="addressdetail" value="<?php echo $default->addressdetail; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('Company / Organization name', 'korgou'); ?></label>
        <div class="col-lg-4">
            <input type="text" class="form-control" name="company" value="<?php echo $default->company; ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('Phone Number', 'korgou'); ?></label>
        <div class="col-lg-4">
            <input type="text" class="form-control" name="phonenum" value="<?php echo $default->phonenum; ?>">
            <p class="form-text">Eg. +1-312-400-5566</p>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label "><?php _e('Mobile phone', 'korgou'); ?></label>
        <div class="col-lg-4">
            <input type="text" class="form-control" name="mobilenum" value="<?php echo $default->mobilenum; ?>">
        </div>
    </div>
</fieldset>

<!--
<p class="text-right">
    <button type="button" class="btn btn-info btn-sm"><?php _e('Save', 'korgou'); ?></button>
</p>
-->


<div class="sw-toolbar text-right" role="toolbar">
    <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-link text-dark cancel-btn"><?php _e('Cancel', 'korgou'); ?></a>
    <button class="btn btn-info sw-btn-prev" type="button"><?php _e('Previous', 'korgou'); ?></button>
    <button class="btn btn-info sw-btn-next disabled" type="button"><?php _e('Next', 'korgou'); ?></button>
</div>

<script type="text/javascript">
jQuery(function($) {
    function setAddress() {
        /*
        $('#address-tb input[type="radio"]').each(function() {
            if (this.checked)
                $(this).closest('tr').addClass('font-weight-bold');
            else 
                $(this).closest('tr').removeClass('font-weight-bold');
        });
        */
    }
    setAddress();
    /*
    $('#address-tb tr').click(function() {
        $(this).find('input[type="radio"]').click();
    });
    */
    $('#address-tb input[type="radio"]').click(function() {
        var $tr = $(this).closest('tr');
        console.log($tr);
        $('[data-target]', $tr).each(function() {
            $('[name="' + $(this).data('target') + '"]').val($(this).text());
        });
        setAddress();
    });
    $('#new-address-btn').click(function() {
        $('#address-fields input').val('');
        $('#address-fields select').val('');
        $('#address-tb input[type="radio"]:checked').prop('checked', false);
        setAddress();
        return false;
    });

    $(window).on('checkOrder', function() {
        const fields = ['englishname', 'nativename', 'country', 'zipcode', 'city', 'province', 'addressdetail', 'company', 'phonenum', 'mobilenum'];
        fields.forEach(function(f) {
            $('#option-' + f).text($('#address-fields [name="' + f + '"]').val());
        });
    });

    $('.delete-address-btn').click(function() {
        if (confirm('<?php _e('Are you sure?', 'korgou'); ?>')) {
            var self = this;
            $.post('/wp-admin/admin-ajax.php', {
                action: '<?php $this->the_tag('delete_address'); ?>',
                _wpnonce: '<?php $this->the_nonce('delete_address'); ?>',
                id: $(self).data('id')
            }, function(response) {
                if (response.success) {
                    $(self).closest('tr').remove();
                } else {
                    alert(response.data);
                }
            }, 'json');
        }
    })
});
</script>
