<h4 class="card-title"><?php _e('Check your order', 'korgou'); ?></h4>

<div class="row">
    <div class="col-md-2 h5">
        <?php _e('Packages', 'korgou'); ?>
    </div>
    <div class="col-md-10">
        <div class="table-responsive">
        <table class="table-bordered table table-hover">
            <thead class="thead-light">
            <tr>
                <th><?php _e('Package No.', 'korgou'); ?></th>
                <th><?php _e('Arrival time', 'korgou'); ?></th>
                <th><?php _e('Package source', 'korgou'); ?></th>
                <th><?php _e('Domestic courier', 'korgou'); ?></th>
                <th><?php _e('Tracking number', 'korgou'); ?></th>
                <th><?php _e('Items in package', 'korgou'); ?></th>
                <th><?php _e('Weight', 'korgou'); ?>(g)</th>
                <th><?php _e('Remarks', 'korgou'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($packages as $package): ?>
            <input type="hidden" name="packageid[]" value="<?php echo $package->packageid; ?>">
            <tr>
                <td><?php echo $package->packageid; ?></td>
                <td><?php echo $package->arrivaltime; ?></td>
                <td><?php echo $package->packagesource; ?></td>
                <td><?php echo $package->domesticcourier; ?></td>
                <td><?php echo $package->domestictrackno; ?></td>
                <td><?php echo $package->packagecontent; ?></td>
                <td><?php echo $package->weight; ?></td>
                <td><?php echo $package->remark; ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2 h5">
        <?php _e('Options', 'korgou'); ?>
    </div>
    <div class="col-md-10">
        <div class="table-responsive">
        <table class="table table-sm table-borderless">
            <tr>
                <th style="width: 200px;">
                    <?php _e('Forward courier', 'korgou'); ?>
                </th>
                <td id="option-courier">
                </td>
            </tr>
            <tr>
                <th>
                    <?php _e('Processing', 'korgou'); ?>
                </th>
                <td id="option-processing">
                    Normal
                </td>
            </tr>
            <tr id="option-repacking">
                <th>
                    <?php _e('Premium re-packing', 'korgou'); ?>
                </th>
                <td>
                    <i class="fe-check-square"></i>
                </td>
            </tr>
            <tr id="option-insurance">
                <th>
                    <?php _e('Insurance', 'korgou'); ?>
                </th>
                <td>
                    <i class="fe-check-square"></i>
                </td>
            </tr>
            <?php foreach ($value_addeds as $va): ?>
                <?php if (in_array($va->type, ['ITEM_PHOTOGRAPHY', 'ITEM_SORTING', 'RETURN_EXCHANGE'])) continue; ?>
                <tr id="option-<?php echo $va->type; ?>">
                    <th>
                        <?php _e($va->enname, 'korgou'); ?>
                    </th>
                    <td>
                        <i class="fe-check-square"></i>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <th style="vertical-align: top;">
                    <?php _e('Other special requirements', 'korgou'); ?>
                </th>
                <td id="option-forwardcomment">
                </td>
            </tr>
        </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2 h5">
        <?php _e('Address', 'korgou'); ?>
    </div>
    <div class="col-md-10">
        <div class="table-responsive">
        <table class="table table-sm table-borderless">
            <tr>
                <th style="width: 200px;">
                    <?php _e('English Name', 'korgou'); ?>
                </th>
                <td id="option-englishname">
                </td>
            </tr>
            <tr>
                <th><?php _e('Native Name', 'korgou'); ?></th>
                <td id="option-nativename">
                </td>
            </tr>
            <tr>
                <th><?php _e('Country', 'korgou'); ?></th>
                <td id="option-country">
                </td>
            </tr>
            <tr>
                <th><?php _e('Zip code', 'korgou'); ?></th>
                <td id="option-zipcode">
                </td>
            </tr>
            <tr>
                <th><?php _e('City', 'korgou'); ?></th>
                <td id="option-city">
                </td>
            </tr>
            <tr>
                <th><?php _e('Province', 'korgou'); ?></th>
                <td id="option-province">
                </td>
            </tr>
            <tr>
                <th><?php _e('Address Detail', 'korgou'); ?></th>
                <td id="option-addressdetail">
                </td>
            </tr>
            <tr>
                <th><?php _e('Company', 'korgou'); ?></th>
                <td id="option-company">
                </td>
            </tr>
            <tr>
                <th><?php _e('Phone Number', 'korgou'); ?></th>
                <td id="option-phonenum">
                </td>
            </tr>
            <tr>
                <th><?php _e('Mobile Number', 'korgou'); ?></th>
                <td id="option-mobilenum">
                </td>
            </tr>
        </table>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-2 h5">
        <?php _e('Custom declarations', 'korgou'); ?>
    </div>
    <div class="col-md-10">
        <div class="table-responsive">
        <table class="table-bordered table">
            <thead class="thead-light">
            <tr>
                <th>#</th>
                <th style="width: 40%;"><?php _e('Contents', 'korgou'); ?></th>
                <th><?php _e('Quantity', 'korgou'); ?></th>
                <th><?php _e('Total Value(in USD)', 'korgou'); ?></th>
            </tr>
            </thead>
            <tbody id="option-customs">
            </tbody>
        </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col text-right">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="check-options">
            <label class="form-check-label" for="check-options">
                <?php printf(__('I have read and understood the <a href="%s" target="_blank">KorGou Package Forwarding Service Instructions.', 'korgou'),
                    home_url('/guide/are-there-any-tips-during-my-submission-of-package-forward-application/')); ?>
            </label>
        </div>
    </div>
</div>

<div class="sw-toolbar text-right mt-3" role="toolbar">
    <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-link text-dark cancel-btn"><?php _e('Cancel', 'korgou'); ?></a>
    <button class="btn btn-info sw-btn-prev" type="button"><?php _e('Previous', 'korgou'); ?></button>
    <button type="button" id="submit-btn" class="ladda-button btn btn-primary mr-2" data-style="expand-right"><?php _e('Submit forward application', 'korgou'); ?></button>
</div>

<script type="text/javascript">
jQuery(function($) {
    var l = Ladda.create(document.querySelector('.ladda-button'));

    $('#smartwizard').smartWizard({
        theme: 'dots',
        autoAdjustHeight: false,
        toolbarSettings: {
            showNextButton: false, // show/hide a Next button
            showPreviousButton: false, // show/hide a Previous button
        },
        anchorSettings: {
            anchorClickable: false
        },
        keyboardSettings: {
            keyNavigation: false
        }
    });
    $('.sw-toolbar .cancel-btn').click(function() {
        return confirm('<?php _e('Are you sure to cancel?'); ?>');
    });
    $('.sw-toolbar .btn-info:not(.disabled)').click(function() {
        $(window).scrollTop(0);
    });
    $('#smartwizard ul.nav li:last a').click(function() {
        $(window).trigger('checkOrder');
    });

    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.submit(function() {
        return false;
    });
    $btn.click(function() {
        if ($('#check-options:checked').length == 0) {
            alert('<?php _e('Please check "I have read and understood the KorGou Package Forwarding Service Instructions', 'korgou'); ?>');
            return false;
        }
        l.start();
        $form.ajaxSubmit(function(response) {
            if (response.success) {
                alert('<?php _e('Successful operation'); ?>');
                location.href = '<?php echo home_url('/my/packages/'); ?>';
            } else {
                l.stop();
                alert(response.data);
            }
        });
    });
})
</script>
