<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="form-row mb-2">
                <div class="col-md-6">
                    <label for="input-trackno" class="col-form-label"><?php _e('Package ID', 'korgou'); ?></label>
                    <input type="text" class="form-control-plaintext" id="input-packageid" name="packageid" value="<?php echo $package->packageid; ?>" readonly>
                </div>
            </div>
            <div class="form-row mb-4">
                <div class="col-md-6">
                    <label for="input-cargodetail" class="col-form-label"><?php _e('Detailed list of package contents', 'korgou'); ?></label>
                    <textarea rows="5" class="form-control" id="input-cargodetail" name="cargodetail" required></textarea>
                </div>
            </div>

            <div class="form-row mb-4">
                <div class="col-md-6">
                    <label for="input-images" class="col-form-label"><?php _e('Attachments', 'korgou'); ?> (png, jpg or gif)</label>
                    <?php $this->upload_image_form(Korgou_Package_Image::IMAGETYPE_USER_PHOTO); ?>
                </div>
            </div>

            <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-secondary"><?php _e('Cancel', 'korgou'); ?></a>
            <button type="button" id="submit-btn" class="btn btn-primary waves-effect waves-light"><?php _e('Apply premium photo service', 'korgou'); ?></button>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
    $('#submit-btn').click(function() {
        if ($('#input-cargodetail').val() == '') {
            alert('<?php _e('Detailed list of package contents required', 'korgou'); ?>')
            return false;
        }

        var imageids = [];
        $('input[name="imageid"]').each(function() {
            imageids.push($(this).val());
        });
        KG.payNow('<?php _e('This is paid service and the corresponding amount will be deducted from your account. Are you sure to apply for this value-added service?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('apply_sorting'); ?>',
            _wpnonce: '<?php $this->the_nonce('apply_sorting'); ?>',
            packageid: $('#input-packageid').val(),
            cargodetail: $('#input-cargodetail').val(),
            imageids: imageids.join(',')
        },
        '<?php echo home_url('/my/packages/'); ?>',
        {
            action: '<?php $this->the_tag('add_sorting_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_sorting_to_cart'); ?>',
            packageid: $('#input-packageid').val(),
            cargodetail: $('#input-cargodetail').val()
        });
        return false;
    })
});
</script>
