<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php if (empty($packages)): ?>
            <?php _e('Cart is empty.', 'korgou'); ?>
            <?php else: ?>

            <p class="text-right">
                <label><?php _e('Type', 'korgou'); ?>:</label>
                <i class="fas fa-envelope-open-text ml-2"></i> <?php _e('Envelope', 'Korgou'); ?>
                <i class="fas fa-archive ml-1"></i> <?php _e('Box', 'Korgou'); ?> 
                <i class="far fa-sticky-note ml-1"></i> <?php _e('Other', 'Korgou'); ?>
            </p>

            <div class="table-responsive">
                <table class="table-bordered table table-hover">
                    <thead class="thead-light">
                        <tr>
                            <th>
                                <input data-index="1" id="check-all" type="checkbox">
                            </th>
                            <th><?php _e('Package No.', 'korgou'); ?></th>
                            <th><?php _e('Type', 'korgou'); ?></th>
                            <th><?php _e('Tracking number', 'korgou'); ?></th>
                            <th><?php _e('Weight', 'korgou'); ?>(g)</th>
                            <th><?php _e('Arrival time', 'korgou'); ?></th>
                            <th><?php _e('Status', 'korgou'); ?></th>
                            <th><?php _e('Free storage period', 'korgou'); ?></th>
                            <th><?php _e('Storage charge period', 'korgou'); ?></th>
                            <th><?php _e('Remarks', 'korgou'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; foreach ($packages as $package): $total += $package->weight; ?>
                        <tr>
                            <td class="bs-checkbox " style="width: 36px; ">
                                <?php if ($package->status == '1'): ?>
                                <input data-index="1" data-weight="<?php echo $package->weight; ?>" name="packageid"
                                    class="cb-packageid" type="checkbox" value="<?php echo $package->packageid; ?>">
                                <?php endif; ?>
                            </td>
                            <td><?php echo $package->packageid; ?></td>
                            <td class="text-center"><i class="
                                <?php
                                if ($package->type == 'E')
                                    echo 'fas fa-envelope-open-text';
                                else if ($package->type == 'B')
                                    echo 'fas fa-archive';
                                else if ($package->type == 'O')
                                    echo 'far fa-sticky-note';
                                ?>"></i>
                            </td>
                            <td><?php echo $package->domestictrackno; ?></td>
                            <td><?php echo number_format($package->weight); ?></td>
                            <td><?php echo $package->arrivaltime; ?></td>
                            <td class="<?php $this->the_status_style($package); ?>">
                                <?php _e($package->get_status_name(), 'korgou'); ?>
                            </td>
                            <td><?php $package->the_free_period(); ?></td>
                            <td>
                                <?php $package->the_charge_period(); ?><br>
                                <?php $package->the_storagefee(); ?> KRW<br>
                            </td>
                            <td><?php echo $package->remark; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>

                    <!--
                        <tfoot>
                        <tr>
                            <th></th>
                            <th colspan="2"><?php _e('Total', 'korgou'); ?></th>
                            <th><?php echo $total; ?></th>
                            <th colspan="5"></th>
                        </tr>
                        -->
                </table>
            </div>

            <div class="row mt-2">
                <div class="col" style="display: flex; align-items: center;">
                    <a href="<?php echo home_url('/my/packages/'); ?>" class="text-dark border-bottom border-dark"><b>
                        <i class="fe-arrow-left" style="font-weight: 600;"></i>
                        <?php _e('Go to arrived packages', 'korgou'); ?>
                                </b></a>
                </div>
                <div class="col text-center">
                    <button type="button" id="delete-btn"
                        class="btn btn-secondary mr-md-2 mb-2 mb-sm-0"><?php _e('Remove from cart', 'korgou'); ?></button>
                    <button type="button"
                        class="btn btn-primary forward-btn"><?php _e('Submit my order', 'korgou'); ?></button>
                </div>
                <div class="col"></div>
            </div>

            <?php endif; ?>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
    function getIds() {
        var ids = [];
        $('.cb-packageid:checked').each(function() {
            ids.push($(this).val());
        });
        return ids;
    }
    $('#confirm-return-btn').click(function() {
        var ids = getIds();
        if (ids.length < 1) {
            alert(
                '<?php _e('Please select the packages to be returned or exchanged.', 'korgou'); ?>'
            );
            return false;
        }

        $('#return-modal').modal('show');
        return false;
    });

    $('#delete-btn').click(function() {
        var ids = getIds();

        if (ids.length < 1) {
            alert('<?php _e('Please select the packages.', 'korgou'); ?>');
            return false;
        }

        $.post('/wp-admin/admin-ajax.php', {
            action: '<?php $this->the_tag('remove_from_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('remove_from_cart'); ?>',
            packageid: ids.join(',')
        }, function(response) {
            if (response.success) {
                $('.cb-packageid:checked').each(function() {
                    $(this).closest('tr').remove();
                    var count = response.data;
                    if (count > 0) {
                        $('.cart-count').text(count);
                    } else {
                        $('.cart-count').remove();
                    }
                });

                alert('<?php _e('Removed', 'korgou'); ?>');
            } else {
                alert(response.data());
            }
        });

        return false;
    });

    $('.forward-btn').click(function() {
        var ids = getIds();
        if (ids.length < 1) {
            alert('<?php _e('Please select the packages to be forwarded.', 'korgou'); ?>');
            return false;
        }

        location.href = '<?php echo home_url('/my/packages/forward-application/?packageids='); ?>' +
            ids.join(',');

        return false;
    });

    $('.default-btn').click(function() {
        $('#default-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('default_address'); ?>"]').ajaxSubmit(function(
            response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
    $('.delete-btn').click(function() {
        $('#delete-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('delete_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
    $('.apply-photo-btn').click(function() {
        KG.payNow(
            '<?php _e('This is paid service and the corresponding amount will be deducted from your account. Are you sure to apply for this value-added service?', 'korgou'); ?>', {
                action: '<?php $this->the_tag('apply_photo'); ?>',
                _wpnonce: '<?php $this->the_nonce('apply_photo'); ?>',
                packageid: $(this).data('packageid')
            },
            location.href, {
                action: '<?php $this->the_tag('add_photo_to_cart'); ?>',
                _wpnonce: '<?php $this->the_nonce('add_photo_to_cart'); ?>',
                packageid: $(this).data('packageid')
            });
        return false;
    });

    $('#check-all').click(function() {
        $('.cb-packageid').prop('checked', $(this).is(':checked'));
        updateSelectTotalWeight();
    });
    $('.cb-packageid').click(function() {
        var checkAll = true;
        var total = 0;
        $('.cb-packageid').each(function() {
            if (!$(this).is(':checked')) {
                checkAll = false;
                return false;
            } else {
                total += parseInt($(this).data('weight'));
            }
        });
        $('.forward-btn .total-weight').text((total > 0) ? ' (' + total + 'g)' : '');
        $('#check-all').prop('checked', checkAll);
        updateSelectTotalWeight();
    });

    function updateSelectTotalWeight() {
        var total = 0;
        $('.cb-packageid').each(function() {
            if ($(this).is(':checked')) {
                total += parseInt($(this).data('weight'));
            }
        });
        $('.forward-btn .total-weight').text((total > 0) ? ' (' + total + 'g)' : '');
    }
});
</script>