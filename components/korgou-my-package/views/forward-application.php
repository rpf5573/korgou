<!-- CSS -->
<link href="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/css/smart_wizard_all.min.css" rel="stylesheet" type="text/css" />
<!-- JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/js/jquery.smartWizard.min.js" type="text/javascript"></script>

<?php $this->ajax_form('apply_forward'); ?>

<div class="row">
    <div class="col-12">
        <div class="card bg-warning text-center h4 p-3 mb-3">
            <a href="<?php echo home_url('/guide/how-do-i-forward-packages-by-korgou/'); ?>" class="text-white" target="_blank"><?php _e('Please make sure that you have read and understood the <u>KorGou Package Forwarding Service Instructions</u> before filling out the application forms.', 'korgou'); ?></a>
        </div> <!-- end card-box -->

        <div id="smartwizard" class="card-box">

            <ul class="nav">
                <?php foreach (['Options', 'Address', 'Customs declaration', 'Confirmation'] as $i => $step): ?>
                    <li>
                        <a class="nav-link" href="#step-<?php echo $i+1; ?>">
                            <div class="font-weight-bold">Step <?php echo $i+1; ?></div>
                            <?php _e($step, 'korgou'); ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            
            <div class="tab-content">
                <div id="step-1" class="tab-pane" role="tabpanel">
                    <?php include 'forward-application/options.php'; ?>
                </div>
                <div id="step-2" class="tab-pane" role="tabpanel">
                    <?php include 'forward-application/address.php'; ?>
                </div>
                <div id="step-3" class="tab-pane" role="tabpanel">
                    <?php include 'forward-application/customs.php'; ?>
                </div>
                <div id="step-4" class="tab-pane" role="tabpanel">
                    <?php include 'forward-application/confirm.php'; ?>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div>

</form>

<script type="text/javascript">
jQuery(function($) {
});
</script>
