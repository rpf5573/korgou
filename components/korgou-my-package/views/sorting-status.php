<div class="row">
    <div class="col-12">
        <div class="card-box">
                <div class="form-row mb-2">
                    <div class="col-md-6">
                        <label for="input-trackno" class="col-form-label"><?php _e('Package ID', 'korgou'); ?></label>
                        <p class="border p-2"><?php echo $package->packageid; ?></p>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-6">
                        <label for="input-cargodetail" class="col-form-label"><?php _e('Detailed list of package contents', 'korgou'); ?></label>
                        <p class="border p-2"><?php echo $check->cargodetail; ?></p>
                    </div>
                </div>

                <div class="form-row mb-2">
                    <div class="col-md-6">
                        <label for="input-cargodetail" class="col-form-label"><?php _e('Remarks', 'korgou'); ?></label>
                        <p class="border p-2"><?php echo $check->checkremark; ?></p>
                    </div>
                </div>

                <?php if (!empty($user_images)): ?>
                    <div class="mb-4 check-check-test">
                        <label for="input-cargodetail" class="col-form-label"><?php _e('Attachments', 'korgou'); ?></label>
                        <br>
                        <?php do_action('korgou_package_show_image', $user_images); ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($images)): ?>
                    <div class="mb-4 check-check-test">
                        <label for="input-cargodetail" class="col-form-label"><?php _e('Admin Attachements', 'korgou'); ?></label>
                        <br>
                        <?php do_action('korgou_package_show_image', $images); ?>
                    </div>
                <?php endif; ?>

                <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-secondary"><?php _e('Back', 'korgou'); ?></a>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'),
        $form = $btn.closest('form');
    $form.submit(function() {
        return false;
    })
    $btn.click(function() {
        if ($('#input-cargodetail').val() == '') {
            alert('<?php _e('Detailed list of package contents required', 'korgou'); ?>')
            return false;
        }

        if (!confirm('<?php _e('This is paid service and the corresponding amount will be deducted from your account. Are you sure to apply for this value-added service?', 'korgou'); ?>'))
            return false;

        $form.ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                location.href = '<?php echo home_url('/my/packages/'); ?>';
            }
        });
        return false;
    })
});
</script>
