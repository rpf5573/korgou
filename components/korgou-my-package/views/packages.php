<?php
$DISPOSALS = [
    '2' => __('Disposal confirmation pending', 'korgou'),
    '4' => __('Disposal paid', 'korgou'),
    '5' => __('Disposal completed', 'korgou'),
    '6' => __('Disposal failed', 'korgou'),
    '7' => __('Disposal cancelled', 'korgou'),
    '8' => __('Disposal refunded', 'korgou'),
];
?>
<style>
    table thead th {
        text-align: center;
    }
    table tbody tr > td:not(:last-child) {
        text-align: center;
    }
</style>

<div class="row">
    <div class="col-12">
        <div class="card-box">
            <p class="h4 text-danger"><b><?php _e('Up to 15 items registered as a can be added to the cart. Envelope is unlimited.', 'korgou'); ?></b></p>
            <?php $this->cart_count_message(); ?>

            <p><b><span class="selected-packages text-primary"></span></b>&nbsp;</p>

            <p class="text-right">
                <label><?php _e('Type', 'korgou'); ?>:</label>
                <i class="fas fa-envelope-open-text ml-2"></i> <?php _e('Envelope', 'Korgou'); ?>
                <i class="fas fa-archive ml-1"></i> <?php _e('Box', 'Korgou'); ?> 
                <i class="far fa-sticky-note ml-1"></i> <?php _e('Other', 'Korgou'); ?>
            </p>

            <div class="table-responsive">
                <table class="table-bordered table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th>
                            <input data-index="1" id="check-all" type="checkbox">
                        </th>
                        <th><?php _e('Package No.', 'korgou'); ?></th>
                        <th><?php _e('Type', 'korgou'); ?></th>
                        <th><?php _e('Tracking number', 'korgou'); ?></th>
                        <th><?php _e('Weight', 'korgou'); ?>(g)</th>
                        <th><?php _e('Arrival time', 'korgou'); ?></th>
                        <th><?php _e('Status', 'korgou'); ?></th>
                        <th><?php _e('Premium photo service', 'korgou'); ?></th>
                        <th><?php _e('Disposal service', 'korgou'); ?></th>
                        <th><?php _e('Free storage period', 'korgou'); ?></th>
                        <th><?php _e('Storage charge period', 'korgou'); ?></th>
                        <th><?php _e('Remarks', 'korgou'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($packages->items as $package): $in_cart = in_array($package->packageid, $cart); ?>
                    <tr>
                        <td class="bs-checkbox " style="width: 36px; ">
                            <?php if ($package->status == '1'): ?>
                                <?php if ($in_cart): ?>
                                    <i class="fas fa-shopping-cart"></i>
                                <?php else: ?>
                                    <input data-index="1" data-weight="<?php echo $package->weight; ?>" name="packageid" class="cb-packageid" type="checkbox" value="<?php echo $package->packageid; ?>">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php echo $package->packageid; ?>
                        </td>
                        <td class="text-center"><i class="
                            <?php
                            if ($package->type == 'E')
                                echo 'fas fa-envelope-open-text';
                            else if ($package->type == 'B')
                                echo 'fas fa-archive';
                            else if ($package->type == 'O')
                                echo 'far fa-sticky-note';
                            ?>"></i></td>
                        <td><?php echo $package->domestictrackno; ?></td>
                        <td><?php echo number_format($package->weight); ?></td>
                        <td><?php echo $package->arrivaltime; ?></td>
                        <td class="<?php $this->the_status_style($package); ?>">
                            <?php _e($package->get_status_name(), 'korgou'); ?>
                        </td>
                        <td>
                            <?php if ($package->checkliststatus == '1'): ?>
                                <?php if ($package->status == '6'): ?>
                                    <a href="#" class="btn btn-pink btn-xs btn-block alert-expired"><?php _e('Premium photo service application', 'korgou'); ?></a>
                                <?php elseif ($package->status == '3'): ?>
                                    <a href="#" class="btn btn-pink btn-xs btn-block alert-in-cart"><?php _e('Premium photo service application', 'korgou'); ?></a>
                                <?php elseif ($package->status == '1'): ?>
                                    <?php if ($in_cart): ?>
                                        <a href="#" class="btn btn-pink btn-xs btn-block alert-in-cart"><?php _e('Premium photo service application', 'korgou'); ?></a>
                                    <?php else: ?>
                                        <a href="<?php echo home_url('/my/packages/sorting-service/?packageid=' . $package->packageid); ?>" class="btn btn-pink btn-xs btn-block"><?php _e('Premium photo service application', 'korgou'); ?></a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php elseif ($package->checkliststatus == '3'): ?>
                                <a href="<?php echo home_url('/my/packages/sorting-status/?packageid=' . $package->packageid); ?>" class="btn btn-link"><?php _e('Premium photo service completed', 'korgou'); ?></a>
                            <?php elseif ($package->checkliststatus == '4'): ?>
                                <a href="<?php echo home_url('/my/packages/sorting-status/?packageid=' . $package->packageid); ?>" class="btn btn-link"><?php _e('Premium photo service failed', 'korgou'); ?></a>
                            <?php elseif ($package->checkliststatus == '2'): ?>
                                <?php _e('Premium photo service applied', 'korgou'); ?>
                            <?php else: ?>
                                <?php _e('Unknown', 'korgou'); ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php
                            $link = home_url('/my/packages/disposal-status/?packageid=' . $package->packageid);
                            if ($package->disposal == '1'): ?>
                                <?php if ($package->status == '6'): ?>
                                    <a href="#" class="btn btn-blue btn-xs btn-block alert-expired"><?php _e('Disposal service application', 'korgou'); ?></a>
                                <?php elseif ($package->status == '3'): ?>
                                    <a href="#" class="btn btn-blue btn-xs btn-block alert-in-cart"><?php _e('Disposal service application', 'korgou'); ?></a>
                                <?php elseif ($package->status == '1'): ?>
                                    <?php if ($in_cart): ?>
                                        <a href="#" class="btn btn-blue btn-xs btn-block alert-in-cart"><?php _e('Disposal service application', 'korgou'); ?></a>
                                    <?php elseif ($package->checkliststatus == '1'): ?>
                                        <a href="#" class="btn btn-blue btn-xs btn-block alert-photo-required"><?php _e('Disposal service application', 'korgou'); ?></a>
                                    <?php else: ?>
                                        <a href="<?php echo home_url('/my/packages/disposal-service/?packageid=' . $package->packageid); ?>" class="btn btn-blue btn-xs btn-block"><?php _e('Disposal service application', 'korgou'); ?></a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php elseif ($package->disposal == '3'): ?>
                                <a href="<?php echo $link; ?>" class="btn btn-blue btn-xs btn-block"><?php _e('Pay for disposal fee', 'korgou'); ?></a>
                            <?php else: ?>
                                <a href="<?php echo $link; ?>" class="p-0 btn btn-link"><?php echo $DISPOSALS[$package->disposal] ?? __('Unknown', 'korgou'); ?></a>
                            <?php endif; ?>
                        </td>
                        <td><?php $package->the_free_period(); ?></td>
                        <td>
                            <?php $package->the_charge_period(); ?><br>
                            <?php $package->the_storagefee(); ?> KRW<br>
                        </td>
                        <td><?php echo $package->remark; ?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <?php do_action('korgou_my_pagination', $packages); ?>

            <p class="text-center mt-2">
                <button type="button" id="confirm-return-btn" class="btn btn-secondary mr-md-2 mb-2 mb-sm-0"><?php _e('Return/Exchange application', 'korgou'); ?></button>
                <!--
                <button type="button" class="btn btn-primary forward-btn"><?php _e('Package forward application', 'korgou'); ?><span class="total-weight"></span></button>
                -->
                <button type="button" class="btn btn-primary forward-btn"><?php _e('Add to cart for shipping', 'korgou'); ?><span class="total-weight"></span></button>
            </p>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="modal" tabindex="-1" role="dialog" id="return-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php _e('Return/Exchange Application', 'korgou'); ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-check pl-0">
            <label class="form-check-label mr-3" for="return-check-1">
                <?php _e('All packages customers submitted return application will be sent back to the original sender using CJ logistics.', 'korgou'); ?>
            </label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="return-btn" class="btn btn-secondary" data-dismiss="modal"><?php _e('Return/Exchange application', 'korgou'); ?></button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    var formatter = new Intl.NumberFormat();

    function getIds() {
        var ids = [];
        $('.cb-packageid:checked').each(function() {
            ids.push($(this).val());
        });
        return ids;
    }
    $('#confirm-return-btn').click(function() {
        var ids = getIds();
        if (ids.length < 1) {
            alert('<?php _e('Please select the packages to be returned or exchanged.', 'korgou'); ?>');
            return false;
        }

        $('#return-modal').modal('show');
        return false;
    });

    $('#return-btn').click(function() {
        var ids = getIds().join(',');

        KG.payNow('<?php _e('This is paid service and the corresponding amount will be deducted from your account. Are you sure to apply for this value-added service?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('apply_return_exchange'); ?>',
            _wpnonce: '<?php $this->the_nonce('apply_return_exchange'); ?>',
            packageid: ids
        },
        '<?php echo home_url('/my/packages/'); ?>',
        {
            action: '<?php $this->the_tag('add_return_exchange_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_return_exchange_to_cart'); ?>',
            packageid: ids
        });
        return false;
    });

    $('.forward-btn').click(function() {
        var ids = getIds();
        if (ids.length < 1) {
            alert('<?php _e('Please select the packages to be forwarded.', 'korgou'); ?>');
            return false;
        }

        // location.href = '<?php echo home_url('/my/packages/forward-application/?packageids='); ?>' + ids.join(',');
        $.post('/wp-admin/admin-ajax.php', {
            action: '<?php $this->the_tag('add_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_to_cart'); ?>',
            packageid: ids.join(',')
        }, function(response) {
            if (response.success) {
                if (confirm('<?php _e('Go to cart!!', 'korgou'); ?>')) {
                    location.href = '<?php echo home_url('/my/packages/forward-cart/'); ?>';
                } else {
                    location.reload();
                }
            } else {
                alert(response.data);
            }
        });

        return false;
    });

    $('.default-btn').click(function() {
        $('#default-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('default_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
    $('.delete-btn').click(function() {
        $('#delete-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('delete_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
    $('.apply-photo-btn').click(function() {
        KG.payNow('<?php _e('This is paid service and the corresponding amount will be deducted from your account. Are you sure to apply for this value-added service?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('apply_photo'); ?>',
            _wpnonce: '<?php $this->the_nonce('apply_photo'); ?>',
            packageid: $(this).data('packageid')
        },
        location.href,
        {
            action: '<?php $this->the_tag('add_photo_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_photo_to_cart'); ?>',
            packageid: $(this).data('packageid')
        });
        return false;
    });
    
    $('#check-all').click(function() {
        $('.cb-packageid').prop('checked', $(this).is(':checked'));
        updateSelectTotalWeight();
    });
    $('.cb-packageid').click(function() {
        var checkAll = true;
        var total = 0;
        $('.cb-packageid').each(function() {
            if (!$(this).is(':checked')) {
                checkAll = false;
                return false;
            } else {
                total += parseInt($(this).data('weight'));
            }
        });
        $('.forward-btn .total-weight').text((total > 0) ? ' (' + total + 'g)' : '');
        $('#check-all').prop('checked', checkAll);
        updateSelectTotalWeight();
    });

    $('.alert-photo-required').click(function() {
        alert('<?php _e('In order to use the service, the customer MUST First Apply for the Photo service and identify the item.', 'korgou'); ?>');
        return false;
    });

    $('.alert-expired').click(function() {
        alert('<?php _e('Please contact the customer center to request activation of the EXPIRED items.', 'korgou'); ?>');
        return false;
    });

    $('.alert-in-cart').click(function() {
        alert('<?php _e('Package in the Cart or Applied Forwarding for Delivery cannot Apply for Service.', 'korgou'); ?>');
        return false;
    });

    function updateSelectTotalWeight() {
        var total = 0;
        var count = 0;
        $('.cb-packageid').each(function() {
            if ($(this).is(':checked')) {
                total += parseInt($(this).data('weight'));
                count++;
            }
        });
        var totalFormatted = formatter.format(total);
        $('.forward-btn .total-weight').text((count > 0) ? ' (' + (count == 1 ? '1 package' : count + ' packages') + ' / ' + totalFormatted + 'g)' : '');
        $('.selected-packages').text((count > 0) ? 'You selected ' + (count == 1 ? '1 package' : count + ' packages') + ' (' + totalFormatted + 'g).' : '');
    }
});
</script>
