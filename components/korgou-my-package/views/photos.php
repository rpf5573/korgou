<?php
// @TODO 이미지 다운로드
?>
<div class="row mb-4">
    <div class="col-12">
        <div class="card-box">
            <p>
                <label for="input-trackno" class="col-form-label mr-2"><?php _e('Package ID', 'korgou'); ?></label>
                <span><?php echo $package->packageid; ?>
            </p>
            <p>
                <?php if (!empty($images)): ?>
                    <?php do_action('korgou_package_show_image', $images); ?>
                <?php endif; ?>
            </p>

            <a href="<?php echo remove_query_arg('packageid', home_url('/my/packages/')); ?>" class="btn btn-secondary"><?php _e('Back', 'korgou'); ?></a>
        </div>
    </div>
</div>

