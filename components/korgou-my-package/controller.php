<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_my_package';

    protected $_cart_count = null;

    function load()
    {
        $this->add_ajax('apply_forward');

        $this->add_ajax('apply_photo');
        $this->add_ajax('add_photo_to_cart');
        $this->add_action('process_photo', 10, 2);

        $this->add_ajax('apply_sorting');
        $this->add_ajax('add_sorting_to_cart');
        $this->add_action('process_sorting', 10, 3);

        $this->add_ajax('apply_disposal');
        $this->add_ajax('cancel_disposal');
        $this->add_ajax('pay_disposal');
        $this->add_ajax('add_disposal_to_cart');
        $this->add_action('process_disposal', 10, 2);

        $this->add_ajax('apply_return_exchange');
        $this->add_ajax('add_return_exchange_to_cart');
        $this->add_action('process_return_exchange', 10, 2);

        $this->add_ajax('delete_address');

        $this->add_ajax('add_to_cart');
        $this->add_ajax('remove_from_cart');

        $this->add_ajax('upload_image');

        $this->add_shortcode();
        $this->add_shortcode('disposal_service');
        $this->add_shortcode('disposal_status');
        $this->add_shortcode('sorting_service');
        $this->add_shortcode('sorting_status');
        $this->add_shortcode('photos');
        $this->add_shortcode('forward_application');
        $this->add_shortcode('cart');
        $this->add_action('cart_count');
        $this->add_action('cart_count_message');
    }

    function shortcode()
    {
        $userid = KG::get_current_userid();
        $cart_items = apply_filters('korgou_package_get_cart_list', [], [
            'userid' => $userid,
        ]);
        $cart = [];
        if (!empty($cart_items)) {
            foreach ($cart_items as $item) {
                $cart[] = $item->packageid;
            }
        }

        $paged = max( 1, get_query_var('paged') );
        $rowcount = $_GET['rowcount'] ?? 10;
        $context = [
            'cart' => $cart,
            'packages' => apply_filters('korgou_package_get_package_page', [], [
                'userid' => KG::get_current_userid(),
            ], null, null, $rowcount, $paged),
        ];
        $this->view('packages', $context);
    }

    function add_to_cart()
    {
        $userid = KG::get_current_userid();

        $config = apply_filters('korgou_get_config', null, 'PACKAGE_FORWARD_LIMIT');
        $limit = ($config == null) ? 0 : intval($config->v);

        $packageid = $_POST['packageid'];
        $packageids = explode(',', $packageid);

        $cart_box_count = 0;
        $selected_box_count = 0;

        $cart_items = apply_filters('korgou_package_get_cart_list', [], [
            'userid' => $userid,
        ]);
        if (!empty($cart_items)) {
            $cart_packageids = [];
            foreach ($cart_items as $cart) {
                $cart_packageids[] = $cart->packageid;
            }
            $packages = apply_filters('korgou_package_get_package_list', [], [
                'userid' => $userid,
                'packageid' => $cart_packageids,
            ]);
            foreach ($packages as $package) {
                if ($package->type == 'B' || $package->type == 'O')
                    $cart_box_count++;
            }
        }

        $packages = apply_filters('korgou_package_get_package_list', [], [
            'userid' => $userid,
            'type' => ['B', 'O'],
            'packageid' => $packageids,
        ]);
        foreach ($packages as $package) {
            if (!in_array($package->packageid, $cart_packageids))
                $selected_box_count++;
        }

        // wp_send_json_error($all_cart_count . ' + ' . $selected_box_count . ' - ' . $package_count . ' > ' . $limit);
        if ($cart_box_count + $selected_box_count > $limit) {
            if ($cart_box_count >= $limit) {
                wp_send_json_error(__("You have already exceeded the limit.\nThe selected packages cannot be added to the cart.", 'korgou'));
            }

            if ($cart_box_count > 0) {
                wp_send_json_error(sprintf(__("You cannot apply for more than %d items.\nThe selected packages cannot be added to the cart.\nTotal packages in cart : %d ea", 'korgou'), $limit, $cart_box_count));
            } else {
                wp_send_json_error(sprintf(__("You cannot apply for more than %d items.\nTotal packages in cart : %d ea", 'korgou'), $limit, $cart_box_count));
            }
        }

        $args = [
            'userid' => $userid
        ];
        foreach ($packageids as $id) {
            $args['packageid'] = $id;
            do_action('korgou_package_replace_cart', $args);
        }

        wp_send_json_success();
    }

    function remove_from_cart()
    {
        $userid = KG::get_current_userid();
        $packageid = $_POST['packageid'];
        $packageids = explode(',', $packageid);

        $args = [
            'userid' => $userid
        ];
        foreach ($packageids as $id) {
            $args['packageid'] = $id;
            do_action('korgou_package_delete_cart', $args);
        }

        wp_send_json_success(apply_filters('korgou_package_count_cart', 0, ['userid' => $userid]));
    }

    function get_cart_count()
    {
        if ($this->_cart_count == null) {
            $userid = KG::get_current_userid();
            $this->_cart_count = apply_filters('korgou_package_count_cart', 0, ['userid' => $userid]);
        }

        return $this->_cart_count;
    }

    function cart_count()
    {
        $count = $this->get_cart_count();

        if ($count > 0) {
            printf('<span class="badge badge-pill badge-light cart-count" style="padding: 0.25em 0.6em; border-radius: 10rem;">%d</span>', $count);
        }
    }

    function cart_count_message()
    {
        $userid = KG::get_current_userid();
        $cart_items = apply_filters('korgou_package_get_cart_list', [], [
            'userid' => $userid,
        ]);
        $count = sizeof($cart_items);
        if ($count > 0) {
            $cart_box_count = 0;

            $cart_packageids = [];
            foreach ($cart_items as $cart) {
                $cart_packageids[] = $cart->packageid;
            }
            $packages = apply_filters('korgou_package_get_package_list', [], [
                'userid' => $userid,
                'packageid' => $cart_packageids,
            ]);
            $type_count = [];
            foreach (Korgou_Package::$TYPES as $type => $label) {
                $type_count[$type] = 0;
            }
            foreach ($packages as $package) {
                $type_count[$package->type] = $type_count[$package->type] + 1;
            }

            echo '<p>';
            printf( _n( 'You already have %s package in your cart.', 'You already have %d packages in your cart.', $count, 'korgou'), $count);
            echo '&nbsp;(';
            $count_messages = [];
            foreach ($type_count as $type => $count) {
                $count_messages[] = sprintf( __('%s: %d ea', 'korgou'), Korgou_Package::$TYPES[$type], $count);
            }
            echo implode(', ', $count_messages);
            echo ')</p>';
        }
    }

    function cart()
    {
        $userid = KG::get_current_userid();
        $cart_items = apply_filters('korgou_package_get_cart_list', [], [
            'userid' => $userid,
        ]);
        if (empty($cart_items)) {
            $packages = [];
        } else {
            $packageids = [];
            foreach ($cart_items as $cart) {
                $packageids[] = $cart->packageid;
            }
            $packages = apply_filters('korgou_package_get_package_list', [], [
                'userid' => $userid,
                'packageid' => $packageids,
            ]);
        }

        $context = [
            'packages' => $packages,
        ];
        $this->view('cart', $context);
    }

    function the_status_style($package)
    {
        $style = '';
        switch ($package->status) {
            case '2':
            case '3':
                $style = 'text-primary';
                break;
            case '4':
            case '5':
                $style = 'text-success';
                break;
            case '6':
                break;
            default:
                break;
        }
        echo $style;
    }

    function pagination($page)
    {
        $context = [
            'page' => $page,
            'rowcount' => $_GET['rowcount'] ?? 10,
        ];
        $this->view('pagination', $context);
    }

    function apply_photo()
    {
        Shoplic_Util::display_errors();

        $packageid = $_POST['packageid'];
        $userid = KG::get_current_userid();

        $this->process_photo($userid, $packageid);

        wp_send_json_success(__('Operation success', 'korgou'));
    }

    function process_photo($userid, $packageid)
    {
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            throw new RuntimeException('Package not available');

        $value_added = apply_filters('korgou_get_value_added', null, ['type' => 'ITEM_PHOTOGRAPHY']);

        do_action('korgou_user_balance_check_balance', - $value_added->price);

        $desc = $value_added->get_desc();

        do_action('korgou_user_balance_change_balance',
            $packageid, $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $value_added->price, 'KRW', $desc, '', '', 'sys', 'sys', ''
        ); 

        do_action('korgou_package_update_package',
            ['photo' => Korgou_Package::PHOTO_APPLY],
            ['packageid' => $packageid, 'photo' => Korgou_Package::PHOTO_NONE]
        );

    }

    function add_photo_to_cart()
    {
        Shoplic_Util::display_errors();
        $packageid = $_POST['packageid'];
        $userid = KG::get_current_userid();
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            wp_send_json_error('Package not available');

        $value_added = apply_filters('korgou_get_value_added', null, ['type' => 'ITEM_PHOTOGRAPHY']);

        do_action('korgou_my_payment_add_to_cart', $userid, $value_added->price, 'item-photography', ['packageid' => $packageid]);

        wp_send_json_success();
    }

    function apply_disposal()
    {
        $packageid = sanitize_text_field($_POST['packageid']);
        $userid = KG::get_current_userid();

        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            throw new RuntimeException('Package not available');

        do_action('korgou_package_insert_disposal', [
            'packageid' => $packageid,
            'cargodetail' => sanitize_textarea_field($_POST['cargodetail'])
        ]);

        do_action('korgou_package_update_package',
            ['disposal' => Korgou_Package::DISPOSAL_PENDING],
            ['packageid' => $packageid, 'disposal' => Korgou_Package::DISPOSAL_NOT_APPLIED]
        );

        $this->process_images($packageid);

        wp_send_json_success(__('Operation success', 'korgou'));
    }

    function cancel_disposal()
    {
        $packageid = sanitize_text_field($_POST['packageid']);
        $userid = KG::get_current_userid();

        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            throw new RuntimeException('Package not available');

        do_action('korgou_package_update_package',
            ['disposal' => Korgou_Package::DISPOSAL_CANCELLED],
            ['packageid' => $packageid],
        );

        wp_send_json_success(__('Operation success', 'korgou'));
    }

    function disposal_service()
    {
        $package = apply_filters('korgou_package_get_package', null, [
            'userid' => KG::get_current_userid(),
            'packageid' => $_GET['packageid'] ?? '',
        ]);
        switch ($package->disposal) {
            case Korgou_Package::DISPOSAL_NOT_APPLIED:
                $view = 'application';
                break;
            case Korgou_Package::DISPOSAL_PENDING:
            case Korgou_Package::DISPOSAL_CONFIRMED:
                $view = 'status';
                break;
            default:
                $view = '';
                break;
        }

        if (!empty($view)) {
            $context = [
                'package' => $package,
            ];
            $this->view('disposal-service/' . $view, $context);
        }
    }

    function disposal_status()
    {
        $package =  apply_filters('korgou_package_get_package', null, [
            'userid' => KG::get_current_userid(),
            'packageid' => sanitize_text_field($_GET['packageid']) ?? '',
        ]);
        if ($package != null) {
            $disposal = apply_filters('korgou_package_get_disposal', null, [
                'packageid' => $package->packageid,
            ]);
            $user_images = apply_filters('korgou_package_get_image_list', [], [
                'packageid' => $package->packageid,
                'imagetype' => Korgou_Package_Image::IMAGETYPE_USER_DISPOSAL,
            ]);
            $images = apply_filters('korgou_package_get_image_list', [], [
                'packageid' => $package->packageid,
                'imagetype' => Korgou_Package_Image::IMAGETYPE_DISPOSAL,
            ]);
            $context = [
                'package' => $package,
                'disposal' => $disposal,
                'user_images' => $user_images,
                'images' => $images,
            ];
        } else {
            $context = [];
        }
        $this->view('disposal-service/status', $context);
    }

    function pay_disposal()
    {
        Shoplic_Util::display_errors();

        $packageid = $_POST['packageid'];
        $userid = KG::get_current_userid();

        $this->process_disposal($userid, $packageid);

        wp_send_json_success(__('Operation success', 'korgou'));
    }

    function process_disposal($userid, $packageid)
    {
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            throw new RuntimeException('Package not available');

        $disposal = apply_filters('korgou_package_get_disposal', null, [
            'packageid' => $packageid,
        ]);
        if ($disposal == null)
            throw new RuntimeException('Disposal not available');

        $fee = $disposal->disposalfee;

        do_action('korgou_user_balance_check_balance', - $fee);

        do_action('korgou_user_balance_change_balance',
            $packageid, $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $fee, 'KRW', 'Item disposal service', '', '', 'sys', 'sys', ''
        ); 

        do_action('korgou_package_update_package',
            ['disposal' => Korgou_Package::DISPOSAL_PAID],
            ['packageid' => $packageid]
        );
    }

    function add_disposal_to_cart()
    {
        /*
        Shoplic_Util::display_errors();

        아래 오류 메시지 출력되어 주석 처리, 2022-03-25 
        <br />
        <b>Warning</b>:  A non-numeric value encountered in <b>/var/www/html/korgou.com/wp-content/plugins/woocommerce/includes/class-wc-cart-totals.php</b> on line <b>229</b><br />
        <br />
        <b>Warning</b>:  A non-numeric value encountered in <b>/var/www/html/korgou.com/wp-content/plugins/woocommerce/includes/class-wc-discounts.php</b> on line <b>87</b><br />
        */

        $packageid = $_POST['packageid'];
        $userid = KG::get_current_userid();
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            wp_send_json_error('Package not available');

        $disposal = apply_filters('korgou_package_get_disposal', null, [
            'packageid' => $packageid,
        ]);
        if ($disposal == null)
            throw new RuntimeException('Disposal not available');

        $fee = $disposal->disposalfee;

        do_action('korgou_my_payment_add_to_cart', $userid, $fee, 'item-disposal', ['packageid' => $packageid]);

        wp_send_json_success();
    }

    function sorting_service()
    {
        $context = [
            'package' => apply_filters('korgou_package_get_package', null, [
                'userid' => KG::get_current_userid(),
                'packageid' => $_GET['packageid'] ?? '',
            ]),
        ];
        $this->view('sorting-service', $context);

    }

    function sorting_status()
    {
        $package =  apply_filters('korgou_package_get_package', null, [
            'userid' => KG::get_current_userid(),
            'packageid' => $_GET['packageid'] ?? '',
        ]);
        if ($package != null) {
            $check = apply_filters('korgou_package_get_check', null, [
                'packageid' => $package->packageid,
            ]);
            $user_images = apply_filters('korgou_package_get_image_list', [], [
              'packageid' => $package->packageid,
              'imagetype' => Korgou_Package_Image::IMAGETYPE_USER_PHOTO,
            ]);
            $images = apply_filters('korgou_package_get_image_list', [], [
                'packageid' => $package->packageid,
                'imagetype' => [Korgou_Package_Image::IMAGETYPE_CHECKLIST, Korgou_Package_Image::IMAGETYPE_IN_REPO_PACKAGE],
            ]);
            $context = [
                'package' => $package,
                'check' => $check,
                'user_images' => $user_images,
                'images' => $images,
            ];
        } else {
            $context = [];
        }
        $this->view('sorting-status', $context);
    }

    function apply_sorting()
    {
        $packageid = $_POST['packageid'];
        $userid = KG::get_current_userid();

        $this->process_sorting($userid, $packageid, $_POST['cargodetail']);
        $this->process_images($packageid);

        wp_send_json_success(__('Operation success', 'korgou'));
    }

    function process_images($packageid)
    {
        if (isset($_POST['imageids'])) {
            foreach (explode(',', $_POST['imageids']) as $imageid) {
                do_action('korgou_package_update_image', [
                    'id' => $imageid,
                    'packageid' => $packageid,
                ]);
            }
        }
    }

    function process_sorting($userid, $packageid, $cargodetail)
    {
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            throw new RuntimeException('Package not available');

        if ($package->status != Korgou_Package::STATUS_IN_REPO)
            throw new RuntimeException('Package not awaiting user process');

        $value_added = apply_filters('korgou_get_value_added', null, ['type' => 'ITEM_SORTING']);

        do_action('korgou_user_balance_check_balance', - $value_added->price);

        $desc = $value_added->get_desc();

        do_action('korgou_user_balance_change_balance',
            $packageid, $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $value_added->price, 'KRW', $desc, '', '', 'sys', 'sys', ''
        ); 

        do_action('korgou_package_update_package',
            ['checkliststatus' => Korgou_Package::CHECKLISTSTATUS_APPLY],
            ['packageid' => $packageid, 'checkliststatus' => Korgou_Package::CHECKLISTSTATUS_NONE]
        );

        // do_action_ref_array('korgou_package_insert_check', [
        do_action('korgou_package_insert_check', [
            'packageid' => $packageid, 
            'cargodetail' => $cargodetail,
        ]); 
        $this->debug(func_get_args());
    }

    function add_sorting_to_cart()
    {
        Shoplic_Util::display_errors();
        $packageid = $_POST['packageid'];
        $userid = KG::get_current_userid();
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            throw new RuntimeException('Package not available');

        if ($package->status != Korgou_Package::STATUS_IN_REPO)
            throw new RuntimeException('Package not awaiting user process');

        $value_added = apply_filters('korgou_get_value_added', null, ['type' => 'ITEM_SORTING']);

        do_action('korgou_my_payment_add_to_cart', $userid, $value_added->price, 'item-sorting', ['packageid' => $packageid, 'cargodetail' => $_POST['cargodetail']]);

        wp_send_json_success();
    }

    function check_package()
    {
        $packageid = $_GET['packageid'] ?? '';
        if (!empty($packageid)) {
            $package =  apply_filters('korgou_package_get_package', null, [
                'userid' => KG::get_current_userid(),
                'packageid' => $packageid,
            ]);
            if ($package != null)
                return $package;
        }

        do_action('korgou_message', 'error', 'Package is not available');
        return false;
    }

    function photos()
    {
        $package = $this->check_package();
        if (!$package)
            return;

        $images = apply_filters('korgou_package_get_image_list', [], [
            'packageid' => $package->packageid,
        ]);
        $context = [
            'package' => $package,
            'images' => $images,
        ];
        $this->view('photos', $context);
    }

    function apply_return_exchange()
    {
        Shoplic_Util::display_errors();

        $packageid = $_POST['packageid'] ?? '';
        if (empty($packageid))
            wp_send_json_error('Package not available');

        $userid = KG::get_current_userid();

        $this->process_return_exchange($userid, $packageid);

        kg_send_json_success();
    }

    function process_return_exchange($userid, $packageid)
    {
        Shoplic_Util::display_errors();

        $this->debug(func_get_args());

        $ids = explode(',', $packageid);

        $packages = [];
        $args = [
            'userid' => $userid,
            'status' => Korgou_Package::STATUS_IN_REPO
        ];
        foreach ($ids as $id) {
            $args['packageid'] = $id;
            $package =  apply_filters('korgou_package_get_package', null, $args);
            if ($package == null)
                throw new RuntimeException('Package not available');

            $packages[] = $package;
        }

        $this->debug($packages);

        if (empty($packages))
            throw new RuntimeException('Package not available');
        
        $value_added = apply_filters('korgou_get_value_added', null, ['type' => 'RETURN_EXCHANGE']);

        $this->debug($value_added);

        do_action('korgou_user_balance_check_balance', - $value_added->price * sizeof($packages));

        $desc = $value_added->get_desc();

        try {
            SPDB::start_transaction();

            foreach ($packages as $package) {
                $this->debug('updating package');
                do_action('korgou_package_update_package', [
                    'packageid' => $package->packageid, 
                    'status' => Korgou_Package::STATUS_APPLY_EXCHANGE
                ]);

                $this->debug('changing balancing');

                do_action('korgou_user_balance_change_balance',
                    $package->packageid, $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $value_added->price, 'KRW', $desc, '', '', 'sys', 'sys', ''
                ); 
            }

            SPDB::commit();
        } catch (Exception $e) {
            SPDB::rollback();
            throw $e;
        } 
    }

    function add_return_exchange_to_cart()
    {
        Shoplic_Util::display_errors();

        $packageid = $_POST['packageid'] ?? '';
        if (empty($packageid))
            wp_send_json_error('Package not available');

        $ids = explode(',', $packageid);
        $userid = KG::get_current_userid();
        $packages = [];
        $args = [
            'userid' => $userid,
            'status' => Korgou_Package::STATUS_IN_REPO
        ];
        foreach ($ids as $id) {
            $args['packageid'] = $id;
            $package =  apply_filters('korgou_package_get_package', null, $args);
            if ($package == null)
                throw new RuntimeException('Package not available');

            $packages[] = $package;
        }

        if (empty($packages))
            throw new RuntimeException('Package not available');
        
        $value_added = apply_filters('korgou_get_value_added', null, ['type' => 'RETURN_EXCHANGE']);

        do_action('korgou_my_payment_add_to_cart', $userid, $value_added->price * sizeof($packages), 'return-exchange', ['packageid' => $packageid]);

        wp_send_json_success();
    }

    function forward_application()
    {
        $packageids = explode(',', $_GET['packageids']);
        $packages = [];
        $args = ['userid' => KG::get_current_userid()];
        $error = false;
        $total_weight = 0;
        foreach ($packageids as $id) {
            $args['packageid'] = $id;
            $package = apply_filters('korgou_package_get_package', null, $args);
            $packages[] = $package;
            if ($package->status != Korgou_Package::STATUS_IN_REPO) {
                $error = true;
            }
            $total_weight += $package->weight;
        }

        if ($error) {
            $context = [
                'packages' => $packages,
                'total_weight' => $total_weight,
            ];
            $this->view('application-error', $context);
        } else {
            $context = [
                'packages' => $packages,
                'total_weight' => $total_weight,
                'value_addeds' => apply_filters('korgou_get_value_added_list', [], ['status' => Korgou_Value_Added::STATUS_OK]),
                'couriers' => apply_filters('korgou_forward_get_courier_list', [], ['status' => Korgou_Forward_Courier::STATUS_OPEN]),
                'addresses' => apply_filters('korgou_user_get_addresses', []),
            ];
            $this->view('forward-application', $context);
        }
    }

    function apply_forward()
    {
        //Shoplic_Util::display_errors();
        // validation
        $this->debug('_POST:', $_POST);

        $packageid = $_POST['packageid'] ?? '';
        if (empty($packageid))
            wp_send_json_error('No package id');
        
        if (!isset($_POST['forwardcourierid']) || empty($_POST['forwardcourierid']))
            wp_send_json_error('Forward courier is not available.');

        $userid = KG::get_current_userid();
        $packages = [];
        $weight_total = 0;
        $args = [
            'userid' => $userid,
        ];
        foreach ($packageid as $pid) {
            $args['packageid'] = $pid;
            $package = apply_filters('korgou_package_get_package', null, $args);
            if ($package == null || $package->status != Korgou_Package::STATUS_IN_REPO)
                wp_send_json_error('Package is not available.');

            $packages[] = $package;
            $weight_total += $package->weight;
        }

        $courier = apply_filters('korgou_forward_get_courier', null, ['id' => $_POST['forwardcourierid']]);
        if ($courier == null)
            wp_send_json_error('Forward courier is not available.');

        $forwardid = 'F' . apply_filters('korgou_nextval', 0, 'packageseq');

        $fee = 0;
        if (isset($_POST['valueaddedservice'])) {
            foreach ($_POST['valueaddedservice'] as $type) {
                $value_added = apply_filters('korgou_get_value_added', null, ['type' => $type]);
                if ($value_added == null)
                    continue;

                $fee += $value_added->price;

                do_action('korgou_forward_insert_valueadded', [
                    'forwardid' => $forwardid,
                    'valueaddedtype' => $type,
                ]);
            }
        }

        // $customs_decls = Shoplic_Util::get_post_data_array('contents', 'quantity', 'value', 'netweight', 'hstariffnumber');
        $customs_decls = Shoplic_Util::get_post_data_array('contents', 'quantity', 'value');
        foreach ($customs_decls as $customs) {
            $customs['forwardid'] = $forwardid;
            do_action('korgou_forward_insert_customs_declaration', $customs);
        }

        $storagefee = 0;
        foreach ($packages as $package) {
            $storagefee += $package->get_storagefee();
            do_action('korgou_package_update_package', [
                'packageid' => $package->packageid,
                'status' => Korgou_Package::STATUS_APPLY_FORWARD,
                'storagefee' => $package->get_storagefee(),
                'userprocesstime' => current_time('mysql'),
            ]);
        }

        $this->debug('Creating forward');

        $forward = array_merge([
            'forwardid' => $forwardid,
            'userid' => $userid,
            'status' => Korgou_Forward::STATUS_ACCEPTED,
            'valueaddedfee' => $fee,
            'packagetime' => current_time('mysql'),
            'forwardcouriername' => $courier->zhname . '-' . $courier->enname,
            'servicefeediscount' => 0,
            'storagefee' => $storagefee,
            'packagelist' => implode(',', $packageid),
        ], Korgou_Util::get_post_data(
            'forwardcourierid', 'forwardcomment', 'englishname', 'nativename', 'country',
            'zipcode', 'province', 'city', 'addressdetail', 'company',
            'phonenum', 'mobilenum', 'processing', 'insurance',
            'repacking'
        ));

        $this->debug('Forward:', $forward);

        do_action('korgou_forward_insert_forward', $forward);

        $this->debug('Forward inserted');

        $data = [
            'userid' => $userid,
            'englishname' => $forward['englishname'],
            'nativename' => $forward['nativename'],
            'country' => $forward['country'],
            'zipcode' => $forward['zipcode'],
            'province' => $forward['province'],
            'city' => $forward['city'],
            'addressdetail' => $forward['addressdetail'],
            'company' => $forward['company'],
            'phonenum' => $forward['phonenum'],
            'mobilenum' => $forward['mobilenum'],
        ];
        $old_address = apply_filters('korgou_user_get_address', null, $data);
        if ($old_address == null) {
            $data['asdefault'] = '1';
            do_action('korgou_user_insert_address', $data);
        } else {
            do_action('korgou_user_default_address', $old_address->id);
        }

        $args = [
            'userid' => $userid,
        ];
        foreach ($packageid as $pid) {
            $args['packageid'] = $pid;
            do_action('korgou_package_delete_cart', $args);
        }

        wp_send_json_success();
    }

    function delete_address()
    {
        if (apply_filters('korgou_user_get_current_address', null, $_POST['id']??0) == null)
            wp_send_json_error(__('Address does not exist.', 'korgou'));

        do_action('korgou_user_delete_address', $_POST['id']);
        wp_send_json_success();
    }

    function upload_image_form($type)
    {
        $this->view('upload-image-form', [
            'type' => $type
        ]);
    }

    function upload_image()
    {
        $arr_img_ext = [ 'image/png', 'image/jpeg', 'image/jpg', 'image/gif' ];
        if (in_array($_FILES['file']['type'], $arr_img_ext)) {
            $path = $_FILES['file']['name'];
            $ext = strtolower(pathinfo($path,PATHINFO_EXTENSION));

            $dir = KORGOU_IMAGE_PATH . '/' . Shoplic_Util::now('ymd');
            mkdir($dir, 0755, true);

            $prefix = $dir . '/' . Shoplic_Util::uuid_v4() . '_';
            $big = $prefix . 'BIG.' . $ext;
            $small = $prefix . 'SMALL.' . $ext;

            if (move_uploaded_file($_FILES["file"]["tmp_name"], $big)) {
                $editor = wp_get_image_editor($big);
                $editor->resize(300, 300);
                $saved = $editor->save($small);
                if ($saved) {
                    $imageid = apply_filters('korgou_package_insert_image', 0, [
                        'smallimageurl' => $small,
                        'bigimageurl' => $big,
                        'imagetype' => $_REQUEST['type'],
                    ]);
                    wp_send_json_success($imageid);
                }
            }
        }

        wp_send_json_error();
    }

};

