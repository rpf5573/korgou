<?php $this->ajax_form('servicetype_3'); ?>
    <p>
        <?php _e('KorGou pays any bills due or make small amount of money transfer for you in Korea.', 'korgou'); ?>
    </p>
    <div class="form-group">
        <label for="input-bankname"><?php _e('Name of the bank', 'korgou'); ?>*</label>

        <select class="form-control" id="select-bank">
            <option value=""><?php _e('Type below or choose', 'korgou'); ?></option>
            <?php foreach (Korgou::$BANKS as $bank) : ?>
                <option><?php echo $bank; ?></option>
            <?php endforeach; ?>
        </select>

        <input type="text" class="form-control" id="input-bankname" name="siteurl">

        <!--
        <small id="passwordHelpBlock" class="form-text">
        <?php _e('Please correctly input the name of the bank, preferablly in Korean, such as 하나은행, 국민은행.', 'korgou'); ?>
        </small>
        -->
    </div>
    <div class="form-group">
        <label for="input-accountno"><?php _e('Account number', 'korgou'); ?>*</label>
        <input type="text" class="form-control" id="input-accountno" name="loginusername" placeholder="">
    </div>
    <div class="form-group">
        <label for="input-accountusername"><?php _e('Account holder', 'korgou'); ?>*</label>
        <input type="text" class="form-control" id="input-accountusername" name="loginuserpsw" placeholder="">
    </div>
    <div class="form-group">
        <label for="input-transferamount"><?php _e('Amount', 'korgou'); ?>*</label>
        <input type="text" class="form-control" id="input-transferamount" name="goodsmoney" placeholder="">
    </div>
    <div class="form-group">
        <label for="input-otherdemands"><?php _e('Notes and other requirements', 'korgou'); ?></label>
        <textarea class="form-control" id="input-otherdemands" name="otherdemands" rows="5"></textarea>
    </div>

    <p>
        <button type="button" class="btn btn-secondary prev-btn"><?php _e('Prev', 'korgou'); ?></button>
        <button type="button" class="btn btn-primary submit-btn"><?php _e('Submit', 'korgou'); ?></button>
    </p>
</form>

<script type="text/javascript">
jQuery(function($) {
    $('#select-bank').change(function() {
        if ($(this).val() == 0) {
            $('#input-bankname').val('').show();
        } else {
            $('#input-bankname').hide().val($(this).val());
        }
    });
});
</script>
