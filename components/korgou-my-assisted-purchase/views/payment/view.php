<div class="row mb-4">
    <div class="col-12">
        <div class="card-box">
            <h4 class="card-title"><?php _e('Order', 'korgou'); ?></h4>

            <div class="table-responsive">
            <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th><?php _e('ID', 'korgou'); ?></th>
                    <th><?php _e('Shopping site address', 'korgou'); ?></th>
                    <th><?php _e('Assisted payment amount', 'korgou'); ?></th>
                    <th><?php _e('Service charge', 'korgou'); ?></th>
                    <th><?php _e('Total payment', 'korgou'); ?></th>
                    <th><?php _e('Refund', 'korgou'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $purchase->id; ?></td>
                    <td><?php echo $purchase->siteurl; ?></td>
                    <td><?php echo number_format($purchase->goodsmoney); ?></td>
                    <td><?php echo $purchase->expectcommission; ?></td>
                    <td><?php echo number_format($purchase->realmoney); ?></td>
                    <td><?php echo number_format($purchase->refundmoney); ?></td>
                </tr>
            </tbody>
            </table>
            </div>

            <div class="table-responsive">
            <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th><?php _e('If an item becomes out of stock', 'korgou'); ?></th>
                    <th><?php _e('Status', 'korgou'); ?></th>
                    <th><?php _e('Domestic tracking number', 'korgou'); ?></th>
                    <th><?php _e('Application Time', 'korgou'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $purchase->get_goodsoutstock_desc(); ?></td>
                    <td><?php echo $purchase->get_status_name(); ?>
                        <?php if ($purchase->status == Korgou_Purchase::STATUS_WAIT_PAY): ?>
                            <button type="button" class="btn btn-warning btn-xs pay-btn" data-id="<?php echo $purchase->id; ?>"><?php _e('Pay Now', 'korgou'); ?></button>
                        <?php endif; ?>
                    </td>
                    <td><?php echo $purchase->domestictrackno; ?></td>
                    <td><?php echo $purchase->applytime; ?></td>
                </tr>
            </tbody>
            </table>
            </div>

            <div class="table-responsive">
            <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th><?php _e('Notes and other requirements', 'korgou'); ?></th>
                </tr>
                <tr>
                    <td><?php echo $purchase->otherdemands; ?></td>
                </tr>
            </tbody>
            </table>
            </div>

            <div class="table-responsive">
            <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th><?php _e('Remarks', 'korgou'); ?></th>
                </tr>
                <tr>
                    <td><?php echo $purchase->remark; ?></td>
                </tr>
            </table>
            </div>
        </div>

        <?php if (!empty($goods_list)): ?>
            <div class="card-box">
                <h4 class="card-title"><?php _e('Items', 'korgou'); ?></h4>

                <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th><?php _e('Item link', 'korgou'); ?></th>
                            <th><?php _e('Quantity', 'korgou'); ?></th>
                            <th><?php _e('Unit price', 'korgou'); ?></th>
                            <th><?php _e('Color', 'korgou'); ?></th>
                            <th><?php _e('Size', 'korgou'); ?></th>
                            <th><?php _e('Other options or requirements', 'korgou'); ?></th>
                            <th><?php _e('Buy status', 'korgou'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($goods_list as $goods): ?>
                            <tr>
                                <td>
                                    <a href="<?php echo $goods->goodsurl; ?>" target="_blank" class="text-dark-"><?php echo $goods->goods ?? $goods->goodsurl; ?></a>
                                </td>
                                <td><?php echo $goods->piece; ?></td>
                                <td><?php echo number_format($goods->price); ?></td>
                                <td><?php echo $goods->color; ?></td>
                                <td><?php echo $goods->goodssize; ?></td>
                                <td><?php echo $goods->goodsoptions; ?></td>
                                <td><?php _e($goods->get_buystatus_name(), 'korgou'); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>

        <?php if (!empty($images)): ?>
            <div class="card-box">
                <h4 class="card-title"><?php _e('Images', 'korgou'); ?></h4>
                <?php do_action('korgou_package_show_image', $images); ?>
            </div>
        <?php endif; ?>

        <a href="#" onclick="history.back(); return false;" class="btn btn-secondary"><?php _e('Back', 'korgou'); ?></a>

    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('.pay-btn').click(function() {
        var id = $(this).data('id');
        KG.payNow('<?php _e('Are you sure to pay for this order?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('pay'); ?>',
            _wpnonce: '<?php $this->the_nonce('pay'); ?>',
            id: id
        },
        location.href,
        {
            action: '<?php $this->the_tag('add_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_to_cart'); ?>',
            id: id
        });
    });
});
</script>
