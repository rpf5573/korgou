<div class="row mb-3">
    <div class="col-12" id="section-1"
        <?php if ($type == 'direct') echo 'style="display: none;"'; ?>>
        <div class="card-box">
            <p><b><?php _e('With KorGou\'s assisted payment service, you can conveniently shop in Korea regardless of the language and payment barriers.', 'korgou'); ?></b>
            </p>
            <p>
                <?php _e('International shipping charges will be caculated after the purchased items arrive at the KorGou warehouse.', 'korgou'); ?><br>
                <?php _e('For more information about KorGou\'s assisted payment service and pricing schedule, please check <a href="/assisted-purchase/">http://www.korgou.com/en/assisted-purchase/</a>', 'korgou'); ?>
            </p>
            <p class="text-danger font-weight-bold">
                <?php _e(' Regarding Assisted purchase and payment services, we are not responsible for any possible default from sellers. We only do those services on behalf of customers but customers should have all responsibility on the transaction.', 'korgou'); ?>
            </p>
            <ul>
                <li>
                    <?php _e('Regarding claims of loss and missing on packages customers received from us, we only accept box opening video as a proof.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('All claims for damages must be reported to KorGou within 7 days after receiving the package. Otherwise they will be automatically denied.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('Regarding import duty in customer’s country, it is on customer’s reponsibility.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('Please note that battery itself or battery built-in products, spray type product, liquid products, perfume etc. may be difficult to clear from all countries custom’s office. We suggest you using Sea parcel for forwarding those items.', 'korgou'); ?>
                </li>
            </ul>
            <div class="form-group">
                <label for=""><?php _e('Payment type', 'korgou'); ?></label>
                <?php if ($type != 'transfer'): ?>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="servicetype" id="input-servicetype-1" value="1" checked>
                        <label class="form-check-label" for="input-servicetype-1">
                            <?php _e('Assisted purchase (users have already registered at the shopping site or the items to be purchased have been added to the cart)', 'korgou'); ?>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="servicetype" id="input-servicetype-2" value="2">
                        <label class="form-check-label" for="input-servicetype-2">
                            <?php _e('Assisted purchase (users haven\'t registered at the shopping site or the site is not open to foreign registration)', 'korgou'); ?>
                        </label>
                    </div>
                <?php endif; ?>
                <?php if ($type != 'payment'): ?>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="servicetype" id="input-servicetype-3" value="3" <?php if ($type == 'transfer') echo 'checked'; ?>>
                        <label class="form-check-label" for="input-servicetype-3">
                            <?php _e('Korean domestic money transfer or payment', 'korgou'); ?>
                        </label>
                    </div>
                <?php endif; ?>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="servicetype" id="input-servicetype-4" value="4" <?php if ($type == 'direct') echo 'checked'; ?>>
                    <label class="form-check-label" for="input-servicetype-4">
                        <?php _e('Direct Selling Goods  by Korgou(COSMETIC or DEEP TALKS)', 'korgou'); ?>
                    </label>
                </div>
            </div>

            <button type="button" class="btn btn-primary next-btn"><?php _e('Next', 'korgou'); ?></button>
        </div> <!-- end card-bmx -->

    </div>

    <div class="col-12" id="section-2" style="display: none;">
        <div class="card-box servicetype-box" id="servicetype-1">
            <?php include 'servicetype-1.php'; ?>
        </div>

        <div class="card-box servicetype-box" id="servicetype-2" style="display: none;">
            <?php include 'servicetype-2.php'; ?>
        </div>

        <div class="card-box servicetype-box" id="servicetype-3" style="display:none;">
            <?php include 'servicetype-3.php'; ?>
        </div>

        <div class="card-box servicetype-box" id="servicetype-4" style="display:none;">
            <?php include 'servicetype-4.php'; ?>
        </div>

    </div> <!-- end col -->

    <div class="col-12" id="section-3" style="display: none;">
        <div class="card-box" id="assist-purchase-result">
        </div>
    </div> <!-- end col -->

</div>

<?php $this->ajax_form('default_address'); ?>
    <input type="hidden" name="id" value="" id="default-id">
</form>

<?php $this->ajax_form('delete_address'); ?>
    <input type="hidden" name="id" value="" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    // $('.servicetype-box').hide(); $('#servicetype-4').show().trigger('display'); $('#section-1').hide(); $('#section-2').show();
    $('.next-btn').click(function() {
        var servicetype = $('input[name="servicetype"]:checked').val();
        var confirmed = false;
        if (servicetype == 3) {
            confirmed = confirm('<?php _e('According to our company policy, goods you purchased from using our assisted payment service should arrive our warehouse first. If not, we are not sure if we can process your assisted payment services in the future. On top of that, in these days many fraud from sellers ocurr in online or SNS shopping activities. To avoid from the situation, you should get as much as detailed information(at least cell phone number) of your sellers. We will not be reponsible for any default and loss caused from sellers, which means that customers cannot claim to Paypal or Korgou due to the problem between customers and sellers. From now on, customers should send us email regarding that each customer will take all reponsibility on transaction money between himself or herself and sellers and Korgou helps only transaction activity on behalf of each customer before customers submit assisted payment application.', 'korgou'); ?>');
        } else if (servicetype == 4) {
            confirmed = true;
        } else {
            confirmed = confirm('<?php _e('THE ORDERED GOODS MUST ARRIVE AT OUR WAREHOUSE FIRST.', 'korgou'); ?>');
        }
        if (confirmed) {
            if (servicetype == 3)
                $('.page-title').text('<?php _e('Assisted payment application', 'korgou'); ?>');

            $('.servicetype-box').hide();
            $('#servicetype-'+servicetype).show().trigger('display');
            $('#section-1').hide();
            $('#section-2').show();
        }
        return false;
    });
    $('#section-2').on('click', '.prev-btn', function() {
        $('#section-2').hide();
        $('.page-title').text('<?php _e('Assisted purchase application', 'korgou'); ?>');
        $('#section-1').show();
    });
    $('#section-2').on('click', '.submit-btn', function() {
        $(this).closest('form').ajaxSubmit(function(response) {
            $('#assist-purchase-result').html(response);
            $('#section-2').hide();
            $('#section-3').show();
            // console.log(response);
        });
        return false;
    });
    /*
    $('input[name="servicetype"]').click(function() {
        $('.servicetype-box').hide();
        $('#servicetype-'+$(this).val()).show();
    });
    */
    var itemSet = $('.item-set').get(0).outerHTML;
    $('#add-item-btn').click(function() {
        $(itemSet).insertBefore($(this));
    });

    <?php if ($type == 'direct'): ?>
        $('.next-btn').click();
    <?php endif; ?>
    
});
</script>
