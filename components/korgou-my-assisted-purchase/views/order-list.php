<div class="row">
    <div class="col-12">
        <h4 class="section-title"><?php _e('Assisted purchase order', 'korgou'); ?></h4>
        <div class="card-box">
            <?php
            do_action('korgou_datatable', [
                __('ID', 'korgou'),
                __('Shopping site address', 'korgou'),
                __('Price', 'korgou'),
                __('Service charge', 'korgou'),
                __('Total payment', 'korgou'),
                __('Refund', 'korgou'),
                __('Status', 'korgou'),
                __('Application Time', 'korgou'),
                '',
            ], 'my_assisted_purchase_payment_order_list'
            );
            ?>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <h4 class="section-title"><?php _e('Assisted payment order', 'korgou'); ?></h4>
        <div class="card-box">
            <?php
            do_action('korgou_datatable', [
                __('ID', 'korgou'),
                __('Name of bank', 'korgou'),
                __('Name of the account holder', 'korgou'),
                __('Amount', 'korgou'),
                __('Service charge', 'korgou'),
                __('Total payment', 'korgou'),
                __('Refund', 'korgou'),
                __('Status', 'korgou'),
                __('Application Time', 'korgou'),
                '',
            ], 'my_assisted_purchase_transfer_order_list'
            );
            ?>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<?php $this->ajax_form('cancel_purchase'); ?>
    <input type="hidden" name="id" value="" id="cancel-purchase-id">
</form>

<?php $this->ajax_form('delete_address'); ?>
    <input type="hidden" name="id" value="" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $(document).on('click', '.cancel-purchase-btn', function() {
        var $self = $(this);
        if (confirm('<?php _e('Are you sure?', 'korgou'); ?>')) {
            $('#cancel-purchase-id').val($(this).data('id'));
            $('form[name="<?php $this->the_tag('cancel_purchase'); ?>"]').ajaxSubmit(function(response) {
                alert(response.data);
                $self.closest('table.datatable').DataTable().ajax.reload();
                //location.reload();
            });
        }
        return false;
    });
    $(document).on('click', '.pay-btn', function() {
        var id = $(this).data('id');
        KG.payNow('<?php _e('Are you sure to pay for this order?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('pay'); ?>',
            _wpnonce: '<?php $this->the_nonce('pay'); ?>',
            id: id
        },
        location.href,
        {
            action: '<?php $this->the_tag('add_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_to_cart'); ?>',
            id: id
        });
    });
    $('.default-btn').click(function() {
        $('#default-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('default_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
    $('.delete-btn').click(function() {
        $('#delete-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('delete_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
});
</script>
