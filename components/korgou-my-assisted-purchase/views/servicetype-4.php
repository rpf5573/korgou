<?php $this->ajax_form('servicetype_4'); ?>
    <p>
        <?php _e('The following products are only for ASSISTED PURCHASE service.', 'korgou'); ?>
    </p>

    <div id="product-list" class="loader">
    </div>
</form>

<script type="text/javascript">
jQuery(function($) {
    $('#servicetype-4').on('display', function() {
        $('#product-list').load('/wp-admin/admin-ajax.php', {
            action: '<?php $this->the_tag('load_products'); ?>',
            nonce: '<?php $this->the_nonce('load_products'); ?>',
            product_id: '<?php echo esc_attr($_GET['product_id'] ?? ''); ?>'
        }, function() {
            $('#product-list').removeClass('loader');
        });
    });
});
</script>
