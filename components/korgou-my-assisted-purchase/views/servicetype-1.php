<p>
<?php _e('If you have already registered at the shopping site or use the "non-member login" method to purchase, please submit the information of the site address, username, password or "non-member login information" after you added the items to be purchased into the shopping cart.', 'korgou'); ?><br>
<?php _e('For the sake of your account security, KorGou suggests that you change the password to a temporary one before the submission and change it back after we processed your order.', 'korgou'); ?>
</p>

<p class="text-danger">
<?php _e('Regarding Assisted purchase and payment services, we are not responsible for any possible default from sellers. We only do those services on behalf of customers but customers should have all responsibility on the transaction.', 'korgou'); ?>
</p>
<ul>
    <li>
        <?php _e('Regarding claims of loss and missing on packages customers received from us, we only accept box opening video as a proof.', 'korgou'); ?>
    </li>
    <li>
        <?php _e('All claims for damages must be reported to KorGou within 7 days after receiving the package. Otherwise they will be automatically denied.', 'korgou'); ?>
    </li>
    <li>
        <?php _e('Regarding import duty in customer’s country, it is on customer’s reponsibility.', 'korgou'); ?>
    </li>
    <li>
        <?php _e('Please note that battery itself or battery built-in products, spray type product, liquid products, perfume etc. may be difficult to clear from all countries custom’s office. We suggest you using Sea parcel for forwarding those items.', 'korgou'); ?>
    </li>
</ul>

<?php $this->ajax_form('servicetype_1'); ?>
    <div class="form-group">
        <label for="input-siteurl"><?php _e('Shopping site address', 'korgou'); ?>*</label>
        <input type="text" class="form-control" id="input-siteurl" name="siteurl" placeholder="http://">
    </div>
    <div class="form-group">
        <label for="input-loginusername"><?php _e('Login username', 'korgou'); ?>*</label>
        <input type="text" class="form-control" id="input-loginusername" name="loginusername" >
    </div>
    <div class="form-group">
        <label for="input-loginuserpsw"><?php _e('Password', 'korgou'); ?>*</label>
        <input type="text" class="form-control" id="input-loginuserpsw" name="loginuserpsw" >
    </div>
    <div class="form-group">
        <label for="input-goodsmoney"><?php _e('Assisted payment amount', 'korgou'); ?>*</label>
        <input type="text" class="form-control" id="input-goodsmoney" name="goodsmoney" >
        <small id="passwordHelpBlock" class="form-text text-muted">
        <?php _e('The final checkout amount might be different with the amount provided. KorGou will refund for any overpayment or request a supplemental payment for any deficciency.', 'korgou'); ?>
        </small>
    </div>
    <div class="form-group">
        <label><?php _e('If an item becomes out of stock', 'korgou'); ?>*</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="goodsoutstock" id="input-goodsoutstock-1" value="1" checked>
            <label class="form-check-label" for="input-goodsoutstock-1">
                <?php _e('Skip and continue to checkout', 'korgou'); ?>
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="goodsoutstock" id="input-goodsoutstock-2" value="2">
            <label class="form-check-label" for="input-goodsoutstock-2">
                <?php _e('Pause the payment', 'korgou'); ?>
            </label>
        </div>
    </div>
    <div class="form-group">
        <label for="input-otherdemands"><?php _e('Notes and other requirements', 'korgou'); ?></label>
        <textarea class="form-control" id="input-otherdemands" name="otherdemands" rows="5"></textarea>
    </div>

    <p>
        <button type="button" class="btn btn-secondary prev-btn"><?php _e('Prev', 'korgou'); ?></button>
        <button type="button" class="btn btn-primary submit-btn"><?php _e('Submit', 'korgou'); ?></button>
    </p>
</form>

<script type="text/javascript">
</script>