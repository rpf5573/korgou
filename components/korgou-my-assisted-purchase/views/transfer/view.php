<div class="row mb-4">
    <div class="col-12">
        <div class="card-box">
            <h4 class="card-title"><?php _e('Order', 'korgou'); ?></h4>

            <div class="table-responsive">
            <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th><?php _e('Order ID', 'korgou'); ?></th>
                    <th><?php _e('Name of Bank', 'korgou'); ?></th>
                    <th><?php _e('Account Holder', 'korgou'); ?></th>
                    <th><?php _e('Account Number', 'korgou'); ?></th>
                    <th><?php _e('Amount', 'korgou'); ?></th>
                    <th><?php _e('Service charge', 'korgou'); ?></th>
                    <th><?php _e('Total payment', 'korgou'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $purchase->id; ?></td>
                    <td><?php echo $purchase->siteurl; ?></td>
                    <td><?php echo $purchase->loginusername; ?></td>
                    <td><?php echo $purchase->loginuserpsw; ?></td>
                    <td><?php echo $purchase->goodsmoney; ?></td>
                    <td><?php echo $purchase->expectcommission; ?></td>
                    <td>
                        <?php echo $purchase->realmoney; ?>
                    </td>
                </tr>
            </tbody>
            </table>
            </div>

            <div class="table-responsive">
            <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th><?php _e('Status', 'korgou'); ?></th>
                    <th><?php _e('Application Time', 'korgou'); ?></th>
                    <th><?php _e('Completion Time', 'korgou'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php echo $purchase->get_status_name(); ?>
                        <?php if ($purchase->status == Korgou_Purchase::STATUS_WAIT_PAY): ?>
                            <button type="button" class="btn btn-warning btn-xs pay-btn" data-id="<?php echo $purchase->id; ?>"><?php _e('Pay Now', 'korgou'); ?></button>
                        <?php endif; ?>
                    </td>
                    <td><?php echo $purchase->applytime; ?></td>
                    <td><?php echo $purchase->donetime; ?></td>
                </tr>
            </tbody>
            </table>
            </div>

            <div class="table-responsive">
            <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th><?php _e('Notes and other requirements', 'korgou'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $purchase->otherdemands; ?></td>
                </tr>
            </tbody>
            </table>
            </div>

            <div class="table-responsive">
            <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th><?php _e('Remark', 'korgou'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $purchase->remark; ?></td>
                </tr>
            </tbody>
            </table>
            </div>
        </div>

        <?php if (!empty($images)): ?>
            <div class="card-box">
                <h4 class="card-title"><?php _e('Images', 'korgou'); ?></h4>
                <?php do_action('korgou_package_show_image', $images); ?>
            </div>
        <?php endif; ?>

        <a href="#" onclick="history.back(); return false;" class="btn btn-secondary"><?php _e('Back', 'korgou'); ?></a>

    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('.pay-btn').click(function() {
        var id = $(this).data('id');
        KG.payNow('<?php _e('Are you sure to pay for this order?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('pay'); ?>',
            _wpnonce: '<?php $this->the_nonce('pay'); ?>',
            id: id
        },
        location.href,
        {
            action: '<?php $this->the_tag('add_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_to_cart'); ?>',
            id: id
        });
    });
});
</script>

