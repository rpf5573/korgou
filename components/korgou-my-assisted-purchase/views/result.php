<h3><?php _e('Operation Success', 'korgou'); ?></h3>
<p>
    <?php _e('Please wait while KorGou confirms your order.', 'korgou'); ?>
</p>
<table class="table table-bordered col-lg-3 col-md-6 col-sm-12">
    <tr>
        <th><?php _e('Assisted payment amount', 'korgou'); ?></th>
        <td>₩<?php echo number_format($goodsmoney); ?></td>
    </tr>
    <tr>
        <th><?php _e('Service charge', 'korgou'); ?></th>
        <td>₩<?php echo number_format($expectcommission); ?></td>
    </tr>
    <tr>
        <th><?php _e('Total payment', 'korgou'); ?></th>
        <td>₩<?php echo number_format($realmoney); ?></td>
    </tr>
</table>

<p>
<?php _e('The final checkout amount might be different with the amount provided. KorGou will refund for any overpayment or request a supplemental payment for any deficiency.', 'korgou'); ?>
</p>

<a href="<?php echo home_url('/my/assisted-purchase/order-list/'); ?>" class="btn btn-primary"><?php _e('Go to order list', 'korgou'); ?></button>

<script type="text/javascript">
jQuery(function($) {
    $('#reload-balance-btn').click(function() {
        var id = $(this).data('id');
        KG.payNow('<?php _e('Are you sure to pay for this order?', 'korgou'); ?>', {
            action: '<?php $this->the_tag('pay'); ?>',
            _wpnonce: '<?php $this->the_nonce('pay'); ?>',
            id: id
        },
        '<?php echo home_url('/my/assisted-purchase/order-list/'); ?>',
        {
            action: '<?php $this->the_tag('add_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_to_cart'); ?>',
            id: id
        });
        /*
        KG.reloadBalance({
            action: '<?php $this->the_tag('add_to_cart'); ?>',
            _wpnonce: '<?php $this->the_nonce('add_to_cart'); ?>',
            id: '<?php echo $id; ?>'
        }, '<?php echo home_url('/my/assisted-purchase/order-list/'); ?>');
        */
        return false;
    })
});
</script>
