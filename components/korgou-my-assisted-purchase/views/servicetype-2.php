        <?php $this->ajax_form('servicetype_2'); ?>
            <p>
                <?php _e('Please provide accurate information about the item link, quantity, color, size or other options.', 'korgou'); ?>
            </p>
            <div class="alert alert-secondary bg-white item-set" role="alert">
                <div class="form-group">
                    <label for="input-goodsurl"><?php _e('Item link', 'korgou'); ?>*</label>
                    <input type="text" class="form-control" id="input-goodsurl" name="goodsurl[]" placeholder="http://">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="input-piece"><?php _e('Quantity', 'korgou'); ?>*</label>
                        <input type="text" class="form-control" name="piece[]" id="input-piece">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="input-color"><?php _e('Color', 'korgou'); ?>*</label>
                        <input type="text" class="form-control" name="color[]" id="input-color">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="input-goodssize"><?php _e('Size', 'korgou'); ?>*</label>
                        <input type="text" class="form-control" name="goodssize[]" id="input-goodssize">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="input-price"><?php _e('Unit price', 'korgou'); ?>*</label>
                        <input type="text" class="form-control" name="price[]" id="input-price">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-goodsoptions"><?php _e('Other options or requirements', 'korgou'); ?>*</label>
                    <input type="text" class="form-control" name="goodsoptions[]" id="input-goodsoptions">
                </div>
            </div>

            <p>
                <button type="button" id="add-item-btn" class="btn btn-info btn-sm"><?php _e('Add item', 'korgou'); ?></button>
            </p>

            <div class="form-group">
                <label for="input-goodsoutstock"><?php _e('If an item becomes out of stock', 'korgou'); ?>*</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="goodsoutstock" id="input-goodsoutstock-1" value="1" checked>
                    <label class="form-check-label" for="input-goodsoutstock-1">
                        <?php _e('Skip and continue to checkout', 'korgou'); ?>
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="goodsoutstock" id="input-goodsoutstock-2" value="2">
                    <label class="form-check-label" for="input-goodsoutstock-2">
                        <?php _e('Pause the payment', 'korgou'); ?>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label for="input-otherdemands"><?php _e('Notes and other requirements', 'korgou'); ?></label>
                <textarea class="form-control" id="input-otherdemands" name="otherdemands" rows="5"></textarea>
            </div>

            <p>
                <button type="button" class="btn btn-secondary prev-btn"><?php _e('Prev', 'korgou'); ?></button>
                <button type="button" class="btn btn-primary submit-btn"><?php _e('Submit', 'korgou'); ?></button>
            </p>
        </form>
        