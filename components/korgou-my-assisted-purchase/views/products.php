<div class="korgou-direct">
    <table id="product-table" class="table table-bordered">
        <thead class="thead-light">
            <th>CATEGORY</th>
            <th>PRODUCT</th>
            <th>PRICE</th>
            <th>QUANTITY</th>
            <th>SUBTOTAL</th>
        </thead>
        <tbody>
            <?php
            if ($product_id != null) {
                $this->products_by_id($product_id);
            } else {
                foreach ($categories as $category) {
                    $this->products_by_cat($category);
                }
            }
            ?>
        </tbody>
    </table>

    <h5>CART TOTALS</h5>

    <table class="table table-bordered" style="width: 600px;">
        <colgroup>
            <col style="width: 50%;">
            <col style="width: 50%;">
        </colgroup>
        <tbody>
            <tr>
                <th>TOTAL</th>
                <td id="product-total"></td>
            </tr>
        </tbody>
    </table>

    <p>
        <button type="button" class="btn btn-secondary prev-btn"><?php _e('Prev', 'korgou'); ?></button>
        <button type="button" class="btn btn-primary submit-btn"><?php _e('Submit', 'korgou'); ?></button>
    </p>
</div>

<script type="text/javascript">
jQuery(function($) {
    var formatter = new Intl.NumberFormat();
    $('#product-table .qty').change(function() {
        var $td = $(this).closest('td');
        var price = $td.data('price');
        var subtotal = parseInt($(this).val()) * parseInt(price);
        var $subtotal = $td.next();
        $subtotal.data('subtotal', subtotal);
        $subtotal.html(subtotal > 0 ? '&#8361;' + formatter.format(subtotal) : '');
        var total = 0;
        $('.product-subtotal').each(function() {
            total += parseInt($(this).data('subtotal'));
        });
        $('#product-total').html(total > 0 ? '&#8361;' + formatter.format(total) : '');
    });

    $('.input-number').each(function() {
        var $input = $(this);
        var $group = $('<div class="input-group"></div>');
        $input.parent().append($group);
        $group.append($input);
        $('<button type="button" class="btn btn-outline-secondary btn-number" data-type="minus"><span class="fa fa-minus"></span></button>')
            .insertBefore($input);
        $('<button type="button" class="btn btn-outline-secondary btn-number" data-type="plus""><span class="fa fa-plus"></span></button>')
            .insertAfter($input);
    });

    $('.quantity').on('click', '.btn-number', function(e){
        console.log('btn-number');
        var type      = $(this).attr('data-type');
        var input = (type == 'minus') ? $(this).next() : $(this).prev();
        var currentVal = parseInt(input.val());
        var max = input.attr('max');
        max = (max == '') ? 10000 : parseInt(max);
        console.log(type, currentVal, max);
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                
                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                } 
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < max) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == max) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {
        
        minValue =  parseInt($(this).attr('min'));
        var max = $(this).attr('max');
        maxValue = (max == '') ? 10000 : parseInt(max);
        valueCurrent = parseInt($(this).val());
        
        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(this).next().removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(this).prev().removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
    });

    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
        
    
    

});
</script>

