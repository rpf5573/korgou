<div class="row mb-3">
    <div class="col-12" id="section-1">
        <div class="card-box">
            <p><b><?php _e('With KorGou\'s assisted payment service, you can conveniently shop in Korea regardless of the language and payment barriers.', 'korgou'); ?></b>
            </p>
            <p>
                <?php _e('International shipping charges will be caculated after the purchased items arrive at the KorGou warehouse.', 'korgou'); ?><br>
                <?php _e('For more information about KorGou\'s assisted payment service and pricing schedule, please check <a href="/assisted-purchase/">http://www.korgou.com/en/assisted-purchase/</a>', 'korgou'); ?>
            </p>
            <p class="text-danger font-weight-bold">
                <?php _e(' Regarding Assisted purchase and payment services, we are not responsible for any possible default from sellers. We only do those services on behalf of customers but customers should have all responsibility on the transaction.', 'korgou'); ?>
            </p>
            <ul>
                <li>
                    <?php _e('Regarding claims of loss and missing on packages customers received from us, we only accept box opening video as a proof.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('All claims for damages must be reported to KorGou within 7 days after receiving the package. Otherwise they will be automatically denied.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('Regarding import duty in customer’s country, it is on customer’s reponsibility.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('Please note that battery itself or battery built-in products, spray type product, liquid products, perfume etc. may be difficult to clear from all countries custom’s office. We suggest you using Sea parcel for forwarding those items.', 'korgou'); ?>
                </li>
            </ul>
            <div class="form-group">
                <label for=""><?php _e('Payment type', 'korgou'); ?></label>
                <?php if ($type != 'transfer'): ?>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="servicetype" id="input-servicetype-1" value="1" checked>
                        <label class="form-check-label" for="input-servicetype-1">
                            <?php _e('Assisted payment (users have already registered at the shopping site or the items to be purchased have been added to the cart)', 'korgou'); ?>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="servicetype" id="input-servicetype-2" value="2">
                        <label class="form-check-label" for="input-servicetype-2">
                            <?php _e('Assisted payment (users haven\'t registered at the shopping site or the site is not open to foreign registration)', 'korgou'); ?>
                        </label>
                    </div>
                <?php endif; ?>
                <?php if ($type != 'payment'): ?>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="servicetype" id="input-servicetype-3" value="3" <?php if ($type == 'transfer') echo 'checked'; ?>>
                        <label class="form-check-label" for="input-servicetype-3">
                            <?php _e('Korean domestic money transfer or payment', 'korgou'); ?>
                        </label>
                    </div>
                <?php endif; ?>
            </div>

            <button type="button" class="btn btn-primary next-btn"><?php _e('Next', 'korgou'); ?></button>
        </div> <!-- end card-bmx -->

    </div>

    <div class="col-12" id="section-2" style="display: none;">
        <div class="card-box servicetype-box" id="servicetype-1">
            <?php include 'servicetype-1.php'; ?>
        </div>

        <div class="card-box servicetype-box" id="servicetype-2" style="display: none;">
            <?php include 'servicetype-2.php'; ?>
        </div>

        <div class="card-box servicetype-box" id="servicetype-3" style="display:none;">
            <?php include 'servicetype-3.php'; ?>
        </div>

    </div> <!-- end col -->

    <div class="col-12" id="section-3" style="display: none;">
        <div class="card-box" id="assist-purchase-result">
        </div>
    </div> <!-- end col -->

</div>

<?php $this->ajax_form('default_address'); ?>
    <input type="hidden" name="id" value="" id="default-id">
</form>

<?php $this->ajax_form('delete_address'); ?>
    <input type="hidden" name="id" value="" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.next-btn').click(function() {
        $('.servicetype-box').hide();
        $('#servicetype-'+$('input[name="servicetype"]:checked').val()).show();
        $('#section-1').hide();
        $('#section-2').show();
    });
    $('.prev-btn').click(function() {
        $('#section-2').hide();
        $('#section-1').show();
    });
    $('.submit-btn').click(function() {
        console.log('submit');
        $(this).closest('form').ajaxSubmit(function(response) {
            $('#assist-purchase-result').html(response);
            $('#section-2').hide();
            $('#section-3').show();
            // console.log(response);
        });
        return false;
    });
    /*
    $('input[name="servicetype"]').click(function() {
        $('.servicetype-box').hide();
        $('#servicetype-'+$(this).val()).show();
    });
    */
    var itemSet = $('.item-set').get(0).outerHTML;
    $('#add-item-btn').click(function() {
        $(itemSet).insertBefore($(this));
    });
});
</script>
