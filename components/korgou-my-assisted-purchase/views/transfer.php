<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="table-responsive">
                <p class="text-right">
                    <a href="<?php echo home_url('/my/assited-purchase/?type=transfer'); ?>" class="btn btn-primary"><?php _e('Apply', 'korgou'); ?></a>
                </p>
                <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th><?php _e('ID', 'korgou'); ?></th>
                        <th><?php _e('Name of the account holder', 'korgou'); ?></th>
                        <th><?php _e('Assisted payment amount', 'korgou'); ?>	</th>
                        <th><?php _e('Service charge', 'korgou'); ?></th>
                        <th><?php _e('Total payment', 'korgou'); ?></th>
                        <th><?php _e('Refund', 'korgou'); ?></th>
                        <th><?php _e('Status', 'korgou'); ?></th>
                        <th><?php _e('Application Time', 'korgou'); ?></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($purchases->items as $purchase): ?>
                    <tr>
                        <td><a href="?id=<?php echo $purchase->id; ?>"><?php echo $purchase->id; ?></a></td>
                        <td><?php echo $purchase->loginusername; ?></td>
                        <td><?php echo $purchase->goodsmoney; ?></td>
                        <td><?php echo $purchase->expectcommission; ?></td>
                        <td><?php echo $purchase->realmoney; ?></td>
                        <td><?php echo $purchase->refundmoney; ?></td>
                        <td><?php echo $purchase->get_status_name(); ?></td>
                        <td><?php echo $purchase->applytime; ?></td>
                        <td></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                </div>
            </div>

            <?php do_action('korgou_my_pagination', $count); ?>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<?php $this->ajax_form('default_address'); ?>
    <input type="hidden" name="id" value="" id="default-id">
</form>

<?php $this->ajax_form('delete_address'); ?>
    <input type="hidden" name="id" value="" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.default-btn').click(function() {
        $('#default-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('default_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
    $('.delete-btn').click(function() {
        $('#delete-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('delete_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
});
</script>
