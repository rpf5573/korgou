<?php $i = 0; foreach ($products as $product):
    $i++;
    $size = sizeof($products);
    $url = $product->get_product_url(); ?>
    <tr>
        <?php if ($i == 1): ?>
            <td <?php if ($size > 1) echo 'rowspan="' . $size . '"'; ?>
                    style="vertical-align: top;">
                <?php echo $category->name; ?>
            </td>
        <?php endif; ?>
        <td>
            <div style="display: flex; align-items: center;">
                <div>
                    <a href="<?php echo $url; ?>" target="_blank"><img src="<?php echo wp_get_attachment_url( $product->get_image_id() ); ?>" style="width: 90px;" /></a>
                </div>
                <div style="margin-left: 20px;">
                    <a href="<?php echo $url; ?>" target="_blank" class="title ml-2"><?php echo $product->get_name(); ?></a>
                    <br>
                    <a href="<?php echo $url; ?>" target="_blank" class="subtitle ml-2"><?php echo get_field('subtitle', $product->get_id()); ?></a>
                    <input type="hidden" name="product_id[]" value="<?php echo $product->get_id(); ?>">
                </div>
            </div>
        </td>
        <td>
            &#8361;<?php echo number_format(get_post_meta($product->get_id(), '_regular_price', true)); ?>
        </td>
        <td class="product-quantity text-center" data-price="<?php echo get_post_meta($product->get_id(), '_regular_price', true); ?>">
            <?php woocommerce_quantity_input([
                'classes' => 'form-control input-number qty',
                'input_name' => 'quantity[]',
                'input_value' => '0',
                ], $product); ?>
        </td>
        <td class="product-subtotal" data-subtotal="0"></td>
    </tr>
<?php endforeach; ?>
