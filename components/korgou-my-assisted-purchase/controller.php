<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_my_assisted_purchase';

    function load()
    {
        $this->add_shortcode();
        $this->add_shortcode('application');
        $this->add_shortcode('payment');
        $this->add_shortcode('transfer');
        $this->add_shortcode('result');
        $this->add_shortcode('order_list');
        $this->add_ajax('payment_order_list');
        $this->add_ajax('transfer_order_list');
        $this->add_ajax('servicetype_1');
        $this->add_ajax('servicetype_2');
        $this->add_ajax('servicetype_3');
        $this->add_ajax('servicetype_4');
        $this->add_ajax('load_products');
        $this->add_ajax('pay');
        $this->add_ajax('add_to_cart');
        $this->add_ajax('add_balance_to_cart');
        $this->add_ajax('cancel_purchase');
        $this->add_action('process_purchase', 10, 2);
    }

    function order_list()
    {
        $context = [];
        $this->view('order-list', $context);
    }

    function cancel_purchase()
    {
        $data = Shoplic_Util::get_post_data('id');
        if (!isset($data['id']) || empty($data['id']))
            throw new RuntimeException(__('Purchase not available', 'korgou'));

        $data['userid'] = KG::get_current_userid();

        $purchase = apply_filters('korgou_purchase_get_purchase', null, $data);
        if ($purchase == null)
            throw new RuntimeException(__('Purchase not available', 'korgou'));

        if ($purchase->status != Korgou_Purchase::STATUS_WAIT_CHECK)
            throw new RuntimeException(__('Purchase not awaiting user process', 'korgou'));

        $data = [
            'id' => $purchase->id,
            'status' => Korgou_Purchase::STATUS_CANCEL,
        ];
        do_action('korgou_purchase_update_purchase', $data);
        
        kg_send_json_success();
    }

    function payment_order_list()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = [
            'userid' => KG::get_current_userid(),
        ];

        $page = apply_filters('korgou_purchase_get_assist_purchase_page', null, $args, null, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {

            $btn = '';
            switch($item->status) {
                case Korgou_Purchase::STATUS_WAIT_CHECK:
                    $btn = sprintf('<button type="button" class="btn btn-xs btn-secondary cancel-purchase-btn" data-id="%s">%s</button>', $item->id, __('Cancel', 'korgou'));
                break;
                case Korgou_Purchase::STATUS_WAIT_PAY:
                    $btn = sprintf('<button type="button" class="btn btn-warning btn-xs pay-btn" data-id="%s">%s</button>', $item->id, __('Pay Now', 'korgou'));
                break;
                default:
                break;
            }

            $data[] = [
                sprintf('<a class="link btn btn-link purchase-btn" href="%2$s?id=%1$s">%1$s</a>', $item->id, home_url('/my/assisted-purchase/')),
                $item->siteurl,
                $item->goodsmoney,
                $item->expectcommission,
                $item->realmoney,
                $item->refundmoney,
                __(Korgou_Purchase::$STATUSES[$item->status] ?? '', 'korgou'),
                $item->applytime,
                $btn,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
            'lang' => ICL_LANGUAGE_CODE
        ];

        $this->send_json($result);

    }

    function transfer_order_list()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = [
            'userid' => KG::get_current_userid(),
        ];

        $page = apply_filters('korgou_purchase_get_bank_transfer_page', null, $args, null, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            $btn = '';
            switch($item->status) {
                case Korgou_Purchase::STATUS_WAIT_CHECK:
                    $btn = sprintf('<button type="button" class="btn btn-xs btn-secondary cancel-purchase-btn" data-id="%s">%s</button>', $item->id, __('Cancel', 'korgou'));
                break;
                case Korgou_Purchase::STATUS_WAIT_PAY:
                    $btn = sprintf('<button type="button" class="btn btn-warning btn-xs pay-btn" data-id="%s">%s</button>', $item->id, __('Pay Now', 'korgou'));
                break;
                default:
                break;
            }

            $data[] = [
                sprintf('<a class="link btn btn-link purchase-btn" href="%2$s?id=%1$s">%1$s</a>', $item->id, home_url('/my/assisted-purchase/')),
                $item->siteurl,
                $item->loginusername,
                $item->goodsmoney,
                $item->expectcommission,
                $item->realmoney,
                $item->refundmoney,
                __($item->get_status_name(), 'korgou'),
                $item->applytime,
                $btn,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);

    }

    function pay()
    {
        $id = $_POST['id'];
        $userid = KG::get_current_userid();

        $this->process_purchase($userid, $id);
        wp_send_json_success(__('Operation success', 'korgou'));
    }

    function process_purchase($userid, $id)
    {
        $this->debug($userid, $id);

        $purchase = apply_filters('korgou_purchase_get_purchase', null, [
            'userid' => $userid,
            'id' => $id,
        ]);
        $this->debug('Purchase:', $purchase);
        if ($purchase == null)
            throw new RuntimeException('Purchase not available');

        if ($purchase->status != Korgou_Purchase::STATUS_WAIT_PAY)
            throw new RuntimeException('Purchase not awaiting user process');

        do_action('korgou_user_balance_check_balance', - $purchase->realmoney);

        $data = [
            'id' => $purchase->id,
            'status' => Korgou_Purchase::STATUS_PAY_SUCC,
        ];
        do_action('korgou_purchase_update_purchase', $data);

        do_action('korgou_user_balance_change_balance',
            $purchase->id, $purchase->userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $purchase->realmoney, 'KRW', 'Assisted purchase', '', '', 'sys', 'sys', '');
    }

    function add_to_cart()
    {
        $id = $_POST['id'];
        $userid = KG::get_current_userid();
        $purchase = apply_filters('korgou_purchase_get_purchase', null, [
            'userid' => $userid,
            'id' => $id,
        ]);
        if ($purchase == null)
            throw new RuntimeException('Purchase not available');

        if ($purchase->status != Korgou_Purchase::STATUS_WAIT_PAY)
            throw new RuntimeException('Purchase not awaiting user process');

        if ($purchase->realmoney > KG::get_user_balance()) {
            do_action('korgou_my_payment_add_to_cart', $userid, $purchase->realmoney, 'assisted-purchase', ['id' => $purchase->id]);

            wp_send_json_success();
        } else {
            wp_send_json_error(__('You have enough balance now.'));
        }
    }

    function add_balance_to_cart()
    {
        $id = $_POST['id'];
        $userid = KG::get_current_userid();
        $purchase = apply_filters('korgou_purchase_get_purchase', null, [
            'userid' => $userid,
            'id' => $id,
        ]);
        if ($purchase == null)
            throw new RuntimeException('Purchase not available');

        /*
        if ($purchase->status != Korgou_Purchase::STATUS_WAIT_PAY)
            throw new RuntimeException('Purchase not awaiting user process');
            */

        if ($purchase->realmoney > KG::get_user_balance()) {
            do_action('korgou_my_payment_add_to_cart', $userid, $purchase->realmoney, 'balance');

            wp_send_json_success();
        } else {
            wp_send_json_error(__('You have enough balance now.'));
        }
    }

    function shortcode()
    {
        $id = $_GET['id'] ?? 0;

        if (!$id)
            return;

        $purchase = apply_filters('korgou_purchase_get_purchase', null, [
            'userid' => KG::get_current_userid(),
            'id' => $id,
        ]);
        if ($purchase == null)
            return;
        
        $context = [
            'purchase' => $purchase,
        ];

        if ($purchase->purchasetype == '3') {
            $view = 'transfer';
        } else {
            $view = 'payment';
            $goods_list = [];
            if ($purchase->purchasetype == '2') {
                $context['goods_list'] = apply_filters('korgou_purchase_get_goods_list', [], [
                    'purchaseid' => $purchase->id,
                ]);
            }
        }

        $context['images'] = apply_filters('korgou_package_get_image_list', [], [
            'packageid' => $purchase->id,
        ]);

        $this->view($view . '/view', $context);
    }

    function application()
    {
        $context = [
            'type' => $_REQUEST['type'] ?? '',
        ];
        $this->view('application', $context);
    }

    function payment()
    {
        $id = $_GET['id'] ?? 0;

        $view = 'payment';
        if ($id) {
            $view .= '/view';
            $goods_list = [];
            $purchase = apply_filters('korgou_purchase_get_purchase', null, [
                'userid' => KG::get_current_userid(),
                'id' => $id,
            ]);
            $context = [
                'purchase' => $purchase,
            ];
            if ($purchase != null && $purchase->purchasetype == '2') {
                $context['goods_list'] = apply_filters('korgou_purchase_get_goods_list', [], [
                    'purchaseid' => $purchase->id,
                ]);
                $context['images'] = apply_filters('korgou_package_get_image_list', [], [
                    'packageid' => $purchase->id,
                ]);
            }
        } else {
            $paged = max( 1, get_query_var('paged') );
            $rowcount = $_GET['rowcount'] ?? 10;
            $context = [
                'purchases' => apply_filters('korgou_purchase_get_assist_purchase_page', [], [
                    'userid' => KG::get_current_userid()
                ], null, $rowcount, $paged),
            ];
        }

        $this->view($view, $context);
    }

    function transfer()
    {
        $id = $_GET['id'] ?? 0;

        $view = 'transfer';
        if ($id) {
            $view .= '/view';
            $goods_list = [];
            $purchase = apply_filters('korgou_purchase_get_purchase', null, [
                'userid' => KG::get_current_userid(),
                'id' => $id,
            ]);
            $context = [
                'purchase' => $purchase,
            ];
            if ($purchase != null) {
                $context['images'] = apply_filters('korgou_package_get_image_list', [], [
                    'packageid' => $purchase->id,
                ]);
            }
        } else {
            $paged = max( 1, get_query_var('paged') );
            $rowcount = $_GET['rowcount'] ?? 10;
            $context = [
                'purchases' => apply_filters('korgou_purchase_get_bank_transfer_page', [], [
                    'userid' => KG::get_current_userid()
                ], null, $rowcount, $paged),
            ];
        }

        $this->view($view, $context);
    }

    function pagination($page)
    {
        $context = [
            'page' => $page,
            'rowcount' => $_GET['rowcount'] ?? 10,
        ];
        $this->view('pagination', $context);
    }

    function servicetype_1()
    {
        Shoplic_Util::display_errors();
        $data = Korgou_Util::get_post_data('siteurl', 'loginusername', 'loginuserpsw', 'goodsmoney', 'goodsoutstock', 'otherdemands');
        $data = array_merge($data, [
            'purchasetype' => Korgou_Purchase::PURCHASETYPE_DAI_FU,
            'shopplace' => Korgou_Purchase::SHOPPLACE_ONLINE,
            'loginuserpsw' => apply_filters('aes_encrypt', $data['loginuserpsw']),
        ]);

        $result = apply_filters('korgou_purchase_apply_assisted_purchase', null, $data);

        $this->view('result', $result);
        exit;
    }

    function servicetype_2()
    {
        Shoplic_Util::display_errors();

        $data = Korgou_Util::get_post_data('goodsoutstock', 'otherdemands');
        $data = array_merge($data, [
            'purchasetype' => Korgou_Purchase::PURCHASETYPE_DAI_GOU,
            'shopplace' => Korgou_Purchase::SHOPPLACE_ONLINE,
        ]);

        $goods = Korgou_Util::get_post_data_array('goodsurl', 'piece', 'color', 'goodssize', 'price', 'goodsoptions');

        $result = apply_filters('korgou_purchase_apply_assisted_purchase', null, $data, $goods);

        $this->view('result', $result);
        exit;
    }

    function servicetype_3()
    {
        $data = Korgou_Util::get_post_data('siteurl', 'loginusername', 'loginuserpsw', 'goodsmoney', 'otherdemands');
        $data = array_merge($data, [
            'purchasetype' => Korgou_Purchase::PURCHASETYPE_ZHUAN_ZHANG,
            'shopplace' => Korgou_Purchase::SHOPPLACE_ONLINE,
        ]);

        $result = apply_filters('korgou_purchase_apply_assisted_purchase', null, $data);

        $this->view('result', $result);
        exit;
    }

    function servicetype_4()
    {
        $data = [
            'siteurl' => 'Korgou Direct Selling',
            'purchasetype' => Korgou_Purchase::PURCHASETYPE_DAI_GOU,
            'shopplace' => Korgou_Purchase::SHOPPLACE_KORGOU,
        ];

        $cart = Shoplic_Util::get_post_data_array('product_id', 'quantity');
        $goods = [];
        foreach ($cart as $item) {
            if ($item['quantity'] != 0) {
                $product = wc_get_product(intval($item['product_id']));
                
                $url = $product->get_product_url();
                if (empty($url))
                    $url = get_permalink($product->get_id());

                $goods[] = [
                    'goods' => $product->get_name() . ' / ' . get_field('subtitle', $product->get_id()),
                    'goodsurl' => $url,
                    'price' => $product->get_regular_price(),
                    'piece' => $item['quantity'],
                ];
            }
        }

        // print_r($goods);
        $result = apply_filters('korgou_purchase_apply_assisted_purchase', null, $data, $goods);

        $this->view('result', $result);
        exit;
    }

    function result()
    {
        $id = $_REQUEST['id'];
        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]);
        if ($purchase == null || $purchase->userid != KG::get_current_userid()) {
            echo 'Purchase not found';
            return;
        }

        $result = $purchase->get_data();

        $this->view('result', $result);
    }

    function load_products()
    {
        $product_id = $_REQUEST['product_id'] ?? '';
        if (!empty($product_id)) {
            $this->view('products', [
                'product_id' => $product_id
            ]);
        } else {
            $categories = get_terms([
                'taxonomy' => 'product_cat',
                'hide_empty' => false,
                'exclude' => 15,
                'parent' => 0,
            ]);

            $this->view('products', [
                'categories' => $categories,
            ]);
        }

        exit;
    }

    function products_by_cat($category)
    {
        $products = wc_get_products([
            'type' => 'external',
            'limit' => -1,
            'orderby' => 'ID',
            'order' => 'ASC',
            'category' => [$category->slug],
        ]);

        $this->view('products-by-cat', [
            'category' => $category,
            'products' => $products,
        ]);

    }

    function products_by_id($product_id)
    {
        $products = wc_get_products([
            'include' => [intval($product_id)]
        ]);

        $this->view('products-by-cat', [
            'category' => get_the_terms ( $product_id, 'product_cat' )[0],
            'products' => $products,
        ]);

    }
};

