<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'woocommerce';

    private $forward = null;

    function load()
    {
        $this->add_action('before_checkout_shipping_form');
        // $this->add_action('checkout_process');
        $this->add_action('checkout_create_order_line_item', 10, 4);
        $this->add_action('order_status_processing');
        $this->add_action('order_status_completed');
        // $this->add_filter('countries', 10, 2);
        // $this->add_action( 'review_order_before_payment' );
        $this->add_action( 'checkout_before_order_review' );
        $this->add_action( 'after_checkout_shipping_form' );

        $this->add_filter('add_to_cart_validation', 20, 3);
        $this->add_filter('order_number', 10, 2);
        $this->add_filter('get_price_html', 10, 2);
        $this->add_filter('product_add_to_cart_url', 10, 2);
    }

    function product_add_to_cart_url($url, $product)
    {
        if ($product->is_type('external')) {
            $url = '/my/assisted-purchase/application/?type=direct&product_id=' . $product->get_id();
        }
        return $url;
    }

    function get_price_html($price, $product)
    {
        if ($product->is_type('external')) {
            $price = '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8361;</span>' .
                number_format($product->get_regular_price()) . '</span>';
        }

        return $price;
    }

    function after_checkout_shipping_form()
    {
        $this->view('after-checkout-shipping-form');
    }

    function checkout_before_order_review()
    {
        echo do_shortcode('[woo_multi_currency_plain_horizontal]');
    }

    function review_order_before_payment()
    {
        echo do_shortcode('[woo_multi_currency_plain_horizontal]');
    }

    function before_checkout_shipping_form()
    {
    ?>
        <h3>
            <?php esc_html_e('Shipping address', 'woocommerce'); ?>
		</h3>
        <p style="color: red; font-weight: bold;">
            <?php _e('Please enter the shipping address you will actually receive in accordance with PayPal’s customer protection policy.', 'korgou'); ?>
        </p>
    <?php
        foreach (WC()->cart->get_cart_contents() as $key => $item) {
            foreach ($item['_service'] as $serviceid => $id) {
                if ($serviceid == 'forwardid') {
                    $this->forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $id]);
                }
            }
        }
        if ($this->forward != null) {
            $this->add_filter('checkout_get_value', 10, 2);
        }
    }

    function checkout_get_value($value, $input)
    {
        switch ($input) {
            case 'shipping_first_name':
                return $this->forward->englishname;

            case 'shipping_country':
                return '';

            case 'shipping_address_1':
                return $this->forward->addressdetail;

            case 'shipping_city':
                return $this->forward->city;

            case 'shipping_state':
                return '';

            case 'shipping_postcode':
                return $this->forward->zipcode;

            default:
                return $value;
        }
    }

    function countries($countries)
    {
        return apply_filters('korgou_get_countries', $countries);
    }

    function checkout_process()
    {
        add_filter('woocommerce_states', function($states) {
            return [$_REQUEST['shipping_country'] => [
                $_REQUEST['shipping_state']
            ]];
        });
        /*
        add_filter('woocommerce_cart_needs_shipping', function($needs_shipping) {
            return false;
        });
        */
    }

    function order_number($order_id, $order)
    {
        return 'C' . (1000000000 + $order_id);
    }

    function add_to_cart_validation( $passed, $product_id, $quantity )
    {
        if( ! WC()->cart->is_empty() )
            WC()->cart->empty_cart();

        return $passed;
    }

    function order_status_processing($order_id)
    {
        $this->add_to_user_balance($order_id);
    }

    function order_status_completed($order_id)
    {
        $this->add_to_user_balance($order_id);
    }

    function checkout_create_order_line_item($item, $cart_item_key, $values, $order)
    {
        if (isset($values['_service'])) {
            $item->add_meta_data('_service', $values['_service']);
        }
    }

    function add_to_user_balance($order_id)
    {
        $meta_balance = get_post_meta($order_id, 'user_balance', true);
        if (empty($meta_balance)) {
            $currency = apply_filters('korgou_currency_get_exchange', null, ['currency' => 'USD']);
            if ($currency != null) {
                $krw = $currency->exchangekrw;

                $order = new WC_Order( $order_id );
                $total = $order->get_total();

                $money = $krw * $total;

                $user = $order->get_user();
                $userid = $user->user_login;

                $payment_method = $order->get_payment_method_title();
                if ($payment_method == 'PayPal') {
                    $payment_method .= ' (' . get_post_meta($order->get_id(), '_transaction_id', true) . ') USD ' . get_post_meta($order->get_id(), '_order_total', true);
                }
                do_action('korgou_user_balance_change_balance',
                    $order->get_order_number(), $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, $money, 'KRW',
                    'Checkout - ' . $payment_method, '', '', 'sys', 'sys', ''
                ); 

                $order->add_order_note(sprintf(
                    'USD %1$d(KRW %2$d) has been updated on your account. Please check for details at <a href="%3$s">My Korgou</a>.<br><br>您的账户已更新了%1$d美元(%2$d韓元). 请登录<a href="%3$s">My KorGou</a>查询',
                    $total, $money, home_url('/my/dashboard/')
                ), 1);

                foreach ($order->get_items() as $item) {
                    $service = $item->get_meta('_service');
                    $this->debug('Service:', $service);
                    if ($service) {
                        $product = $item->get_product();
                        $this->debug('Product:', $product->get_slug());

                        try {
                            switch ($product->get_slug()) {
                                case 'forward':
                                    do_action('korgou_my_forward_process_payment', $userid, $service['forwardid']);
                                    break;
                                case 'item-photography':
                                    do_action('korgou_my_package_process_photo', $userid, $service['packageid']);
                                    break;
                                case 'item-sorting':
                                    do_action('korgou_my_package_process_sorting', $userid, $service['packageid'], $service['cargodetail']);
                                    break;
                                case 'item-disposal':
                                    do_action('korgou_my_package_process_disposal', $userid, $service['packageid']);
                                    break;
                                case 'assisted-purchase':
                                    do_action('korgou_my_assisted_purchase_process_purchase', $userid, $service['id']);
                                    break;
                                case 'return-exchange':
                                    do_action('korgou_my_package_process_return_exchange', $userid, $service['packageid']);
                                    break;
                                default:
                                    break;
                            }
                        } catch (Exception $e) {
                            $this->debug('Exception in processing service:', $e->getMessage());
                        }
                    }
                }
            }
        }
    }
};

