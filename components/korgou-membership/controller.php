<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_membership';

    function load()
    {
        // $this->add_shortcode('box');
    }

    function box()
    {
        if (is_user_logged_in()) {
            $this->view('box');
        }
    }
};
