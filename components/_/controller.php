<?php
defined('ABSPATH') or exit;

function cptui_register_my_cpts_courier()
{

  /**
   * Post Type: Couriers.
   */

  $labels = [
    "name" => __("Couriers", "custom-post-type-ui"),
    "singular_name" => __("Courier", "custom-post-type-ui"),
  ];

  $args = [
    "label" => __("Couriers", "custom-post-type-ui"),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => false,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => ["slug" => "courier", "with_front" => true],
    "query_var" => true,
    "supports" => ["title", "thumbnail"],
    "show_in_graphql" => false,
  ];

  register_post_type("courier", $args);
}

add_action('init', 'cptui_register_my_cpts_courier');

return new class(__FILE__) extends Shoplic_Controller
{
  protected $id = '_';

  function load()
  {
    // add_action('wp_footer', [$this, 'wp_footer']);
    add_action('wp_head', [$this, 'wp_head']);
    add_action('template_redirect', [$this, 'template_redirect']);
    add_filter('wp_nav_menu_gnb_items', [$this, 'menu_gnb_items'], 10, 2);
  }

  function wp_head()
  {
    echo '<link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossorigin="anonymous">';
  }

  function template_redirect()
  {
    if (is_user_logged_in() && is_account_page() && !is_wc_endpoint_url()) { // && !isset($_GET['password-reset'])) {
      wp_safe_redirect(home_url('/my/dashboard/'));
      exit;
    }
  }

  function menu_gnb_items($items, $args)
  {
    if (is_user_logged_in()) {
      $icon = korgou_user_role_icon();
      if (!empty($icon)) {
        $items = str_replace(['My Korgou'], [$icon . 'My Korgou'], $items);
      }
    }
    return $items;
  }
};
