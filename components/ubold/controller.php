<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'ubold';

    function load()
    {
        add_action('after_setup_theme', function() {
            register_nav_menu( 'dashboard_menu', 'Dashboard Menu');
        });
        $this->add_action('nav_menu');
        $this->add_action('head');
    }

    function nav_menu()
    {
        wp_nav_menu([
            'theme_location' => 'dashboard_menu',
            'container' => '',
            'items_wrap' => '%3$s',
        ]);
    }

    function head()
    {
        echo '<link href="' . $this->get_url() . 'assets/css/loader.css" rel="stylesheet" type="text/css" />';
        echo '<link href="' . $this->get_url() . 'assets/css/style.css?ver=1.0.1.2" rel="stylesheet" type="text/css" />';
        echo '<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">';
        echo '<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>';
    }
};
