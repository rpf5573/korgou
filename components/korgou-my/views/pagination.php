<div class="row">
    <div class="col-sm-12 col-md-6 text-left">
        <div class="dataTables_length" id="selection-datatable_length">
            <label><?php _e('Show', 'korgou'); ?> <select name="" class="custom-select custom-select-sm form-control form-control-sm" style="width: auto;">
                <?php foreach ([10, 25, 50, 100] as $rc): ?>
                    <option value="<?php echo add_query_arg('rowcount', $rc); ?>" <?php if ($rowcount == $rc) echo 'selected'; ?>><?php echo $rc; ?></option>
                <?php endforeach; ?>
                </select> <?php _e('entries', 'korgou'); ?>
            </label>
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="dataTables_paginate paging_full_numbers text-right">
            <?php
            	$pagination =  paginate_links( array(
                    'base' => str_replace( PHP_INT_MAX, '%#%', esc_url( get_pagenum_link( PHP_INT_MAX ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total' => $page->max_num_pages,
                    'type' => 'array',
                    'prev_text'          => __('<span></span> Prev', 'korgou'),
                    'next_text'          => __('Next <span></span>', 'korgou'),
                    'before_page_number' => '<span class="screen-reader-text">' . $translated . ' </span>'
                ) );  

        		if ( ! empty( $pagination ) ) : ?>
                    <ul class="pagination pagination-rounded" style="justify-content: flex-end;">
                        <?php foreach ( $pagination as $key => $page_link ) : ?>
                            <li class="page-item<?php if ( strpos( $page_link, 'current' ) !== false ) { echo ' active'; } ?>">
                                <?php echo str_replace( 'page-numbers', 'page-link', $page_link ); ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                <?php endif;
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('.custom-select').on('change', function() {
        location.href = $(this).val();
    });
});
</script>