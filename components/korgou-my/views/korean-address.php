<div class="row">
    <div class="col-12">
        <div class="card-box">

          <dl class="row">
            <dt class="col-sm-2"><?php _e('Address', 'korgou'); ?></dt>
            <dd class="col-sm-10">경기도 성남시 중원구 순환로 79 2층 <?php echo $user->user_login; ?></dd>

            <dt class="col-sm-2"><?php _e('Address in English', 'korgou'); ?></dt>
            <dd class="col-sm-10"><?php echo $user->user_login; ?>, 2F, 79, Sunhwan-ro, Jungwon-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</dd>

            <dt class="col-sm-2"><?php _e('Zip code', 'korgou'); ?></dt>
            <dd class="col-sm-10">13230</dd>

            <dt class="col-sm-2"><?php _e('Recipient', 'korgou'); ?></dt>
            <dd class="col-sm-10 text-danger font-weight-bold"><?php echo $user->user_login; ?> (<?php _e('You should put your unique Identity Number. If not possible, please put your full name that you registered', 'korgou'); ?>)
            </dd>

            <dt class="col-sm-2"><?php _e('Mobile', 'korgou'); ?></dt>
            <dd class="col-sm-10">010-8996-4730</dd>

            <dt class="col-sm-2"><?php _e('Landline', 'korgou'); ?></dt>
            <dd class="col-sm-10">070-4250-0440</dd>

          </dl>

          <p class="text-danger">
          <?php _e('Your arrived packages are automatically sorted and recorded by the Unique Identity Number and recipient name.', 'korgou'); ?><br>
          </p>
          <p>
          <?php _e('In case you haven\'t seen the packages in the system days after the order status changed to "shipped/배송완료" on the shopping sites, please check the list of unidentified packages and claim the package if possible. Please also feel free to contact our customer support for help', 'korgou'); ?>.
          </p>

          <p>
          <?php _e('Some websites still use the <span class="text-danger"><b>ODL TYPE of ADDRESS FORMAT</b></span> type of address format, for these sites, your KorGou address is', 'korgou'); ?>:
          </p>

          <dl class="row">
            <dt class="col-sm-2"><?php _e('Address format', 'korgou'); ?></dt>
            <dd class="col-sm-10">경기도 성남시 중원구 상대원동 446-2 2층 <?php echo $user->user_login; ?></dd>

            <dt class="col-sm-2"><?php _e('Address format in English', 'korgou'); ?></dt>
            <dd class="col-sm-10"><?php echo $user->user_login; ?>, 2F, 446-2 Sangdaewon-dong, Jungwon-gu, Seongnam-si, Gyeonggi-do</dd>

            <dt class="col-sm-2"><?php _e('Zip code', 'korgou'); ?></dt>
            <dd class="col-sm-10">462-807</dd>
          </dl>

          <p class="lead"><b>
            <a href="<?php echo home_url('/guide/1-overview/'); ?>"><?php _e('Guide on how to input your KorGou address at Korean shopping sites.', 'korgou'); ?></a>
          </b></p>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>