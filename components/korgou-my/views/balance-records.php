<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-light">
                <tr>
                    <th><?php _e('Transaction No.', 'korgou'); ?></th>
                    <th><?php _e('Order No.', 'korgou'); ?></th>
                    <th><?php _e('Amount changed(KRW)', 'korgou'); ?></th>
                    <th><?php _e('Description', 'korgou'); ?></th>
                    <th><?php _e('Time', 'korgou'); ?></th>
                    <th><?php _e('Remark', 'korgou'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($records->items as $record): ?>
                <tr>
                    <td><?php echo $record->tradeno; ?></td>
                    <td>
                        <?php
                        if (Shoplic_Util::starts_with($record->outorderno, 'G'))
                            printf('<a href="%1$s?id=%2$s">%2$s</a', home_url('/my/assisted-purchase/'), $record->outorderno);
                        else if (Shoplic_Util::starts_with($record->outorderno, 'F'))
                            printf('<a href="%1$s?id=%2$s">%2$s</a', home_url('/my/forwards/'), $record->outorderno);
                        else
                            echo $record->outorderno;
                        ?>
                    </td>
                    <td><?php echo number_format($record->realbalance); ?></td>
                    <td>
                        <?php
                        _e($record->tradedesc);
                        ?>
                    </td>
                    <td><?php echo $record->tradetime; ?></td>
                    <td><?php echo $record->remark; ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            </div>

            <?php do_action('korgou_my_pagination', $records); ?>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<?php $this->ajax_form('default_address'); ?>
    <input type="hidden" name="id" value="" id="default-id">
</form>

<?php $this->ajax_form('delete_address'); ?>
    <input type="hidden" name="id" value="" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.default-btn').click(function() {
        $('#default-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('default_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
    $('.delete-btn').click(function() {
        $('#delete-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('delete_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
});
</script>
