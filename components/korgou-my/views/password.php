<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_password'); ?>
                <div class="form-group required row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('Current Password', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="password" class="form-control" id="-input" name="password_current" value="" required>
                    </div>
                </div>
                <div class="form-group required row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('New Password', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="password" class="form-control" id="-input" name="password_1" value="" required>
                    </div>
                </div>
                <div class="form-group required row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('Confirm Password', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="password" class="form-control" id="-input" name="password_2" value="" required>
                    </div>
                </div>
                <div>
                    <span class="required-mark">*</span>
                    <?php _e('Required field', 'korgou'); ?>
                </div>

                <div class="mt-4">
                    <input type="submit" id="submit-btn" class="btn btn-primary" value="<?php _e('Change Password', 'korgou'); ?>">
                </div>
            </form>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script src="https://cdn.jsdelivr.net/gh/guillaumepotier/Parsley.js@2.9.2/dist/parsley.js"></script>
<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.ajaxForm(function(response) {
        alert(response.data);
        if (response.success)
            location.reload();
    }).parsley({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid',
        errorsWrapper: '<span class="form-text text-danger"></span>',
        errorTemplate: '<span></span>',
        trigger: 'change'
    });
});
</script>
