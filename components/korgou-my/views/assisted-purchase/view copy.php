<div class="row">
    <div class="col-12">
        <div class="card-box">
            <dl class="row">
                <dt class="col-sm-3">ID</dt>
                <dd class="col-sm-9"><?php echo $purchase->id; ?></dd>

                <dt class="col-sm-3">Shopping site address</dt>
                <dd class="col-sm-9"><?php echo $purchase->siteurl; ?></dd>

                <dt class="col-sm-3">Assisted payment amount</dt>
                <dd class="col-sm-9"><?php echo $purchase->goodsmoney; ?></dd>

                <dt class="col-sm-3">Service charge</dt>
                <dd class="col-sm-9"><?php echo $purchase->expectcommission; ?></dd>

                <dt class="col-sm-3">Total payment</dt>
                <dd class="col-sm-9"><?php echo $purchase->realmoney; ?></dd>

                <dt class="col-sm-3">Refund</dt>
                <dd class="col-sm-9"><?php echo $purchase->refundmoney; ?></dd>

                <dt class="col-sm-3">If an item becomes out of stock</dt>
                <dd class="col-sm-9"><?php echo $purchase->get_goodsoutstock_desc(); ?></dd>

                <dt class="col-sm-3">Notes and other requirements</dt>
                <dd class="col-sm-9"><?php echo $purchase->otherdemands; ?></dd>

                <dt class="col-sm-3">Status</dt>
                <dd class="col-sm-9"><?php echo $purchase->get_status_name(); ?></dd>

                <dt class="col-sm-3">Domestic tracking number</dt>
                <dd class="col-sm-9"><?php echo $purchase->domestictrackno; ?></dd>

                <dt class="col-sm-3">Application Time</dt>
                <dd class="col-sm-9"><?php echo $purchase->applytime; ?></dd>

                <dt class="col-sm-3">Remarks</dt>
                <dd class="col-sm-9"><?php echo $purchase->remark; ?></dd>
            </dl>
        </div>
    </div>
</div>

