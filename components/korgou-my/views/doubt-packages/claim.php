<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="card-title"><?php _e('Claim your un-identified packages here', 'korgou'); ?></h4>
            <p class="text-danger">
                <?php _e('Update purchase details including tracking number and conversation details with seller including tracking number as evidence.', 'korgou'); ?>
                <br>
                <?php _e('And updating is possible when the English name in the profile and the name specified in the package are the same.', 'korgou'); ?>
            </p>
            <?php $this->ajax_form('claim_package', ['enctype' => 'multipart/form-data']); ?>
                <input type="hidden" name="id" value="<?php echo $package->id; ?>">
                <div class="form-row mb-2">
                    <div class="col-md-6">
                        <label for="input-trackno" class="col-form-label"><?php _e('Domestic tracking number', 'korgou'); ?></label>
                        <input type="text" class="form-control" id="input-trackno" name="trackno" value="<?php echo $package->trackno; ?>" readonly>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-6">
                        <label for="input-claimpackagecontent" class="col-form-label"><?php _e('Package contents', 'korgou'); ?></label>
                        <textarea rows="5" class="form-control" id="input-claimpackagecontent" name="claimpackagecontent"></textarea>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-6">
                        <label for="input-photos" class="col-form-label"><?php _e('Order details page screenshot', 'korgou'); ?></label>
                        <small class="text-muted">(png, jpg, jpeg, gif, bmp)</small>
                        <input type="file" class="form-control-file" name="photos[]">
                        <input type="file" class="form-control-file" name="photos[]">
                        <input type="file" class="form-control-file" name="photos[]">
                        <input type="file" class="form-control-file" name="photos[]">
                        <input type="file" class="form-control-file" name="photos[]">
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-6">
                        <label for="input-claimnote" class="col-form-label"><?php _e('Note', 'korgou'); ?></label>
                        <textarea rows="5" class="form-control" id="input-claimnote" name="claimnote"></textarea>
                    </div>
                </div>

                <a href="<?php echo get_permalink_by_path('my/unidentified-packages'); ?>" class="btn btn-secondary"><?php _e('Cancel', 'korgou'); ?></a>
                <button type="button" id="submit-btn" class="btn btn-primary waves-effect waves-light"><?php _e('Claim the package', 'korgou'); ?></button>
            </form>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
    var $form = $('form[name="<?php echo $this->get_tag('claim_package'); ?>"]');
    // $form.parsley();
    $form.submit(function() {
        return false;
    })
    $('#submit-btn').click(function() {
        $form.ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                location.href = '<?php echo home_url('/my/unidentified-packages/'); ?>';
            }
        });
        return false;
    })
});
</script>
