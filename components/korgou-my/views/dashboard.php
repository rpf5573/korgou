<div class="row mt-3">
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-6">
                    <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                        <i class="fas fa-won-sign font-22 avatar-title text-success"></i>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo number_format($balance); ?></span></h3>
                        <p class="mb-1 text-truncate">
                            <?php _e('Balance', 'korgou'); ?>(<?php _e('KRW', 'korgou'); ?>)
                        </p>
                    </div>
                </div>
            </div> <!-- end row-->
            <div class="row">
                <div class="col text-right">
                    <a href="<?php echo home_url('/my/payment/'); ?>" class="btn btn-sm btn-outline-success"><?php _e('Add Balance', 'korgou'); ?></a>
                    <a href="<?php echo home_url('/my/transaction-history/'); ?>" class="btn btn-sm btn-outline-secondary"><?php _e('Transaction History', 'korgou'); ?></a>
                </div>
            </div>
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->

    <div class="col-md-6 col-xl-3 mt-3 mt-sm-0">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-6">
                    <div class="avatar-lg rounded-circle bg-soft-warning border-warning border">
                        <i class="fas fa-box-open font-22 avatar-title text-warning"></i>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo $package_count; ?></span></h3>
                        <p class="mb-1 text-truncate"><?php _e('Arrived Packages', 'korgou'); ?></p>
                    </div>
                </div>
            </div> <!-- end row-->
            <div class="row">
                <div class="col text-right">
                    <a href="<?php echo home_url('/my/packages/'); ?>" class="btn btn-sm btn-outline-warning"><?php _e('View More', 'korgou'); ?></a>
                </div>
            </div>
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->

    <div class="col-md-6 col-xl-3 mt-3 mt-sm-0">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-6">
                    <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                        <i class="fas fa-shipping-fast font-22 avatar-title text-info"></i>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo $forward_count; ?></span></h3>
                        <p class="mb-1 text-truncate"><?php _e('Forwards', 'korgou'); ?></p>
                    </div>
                </div>
            </div> <!-- end row-->
            <div class="row">
                <div class="col text-right">
                    <a href="<?php echo home_url('/my/forwards/'); ?>" class="btn btn-sm btn-outline-info"><?php _e('View More', 'korgou'); ?></a>
                </div>
            </div>
        </div> <!-- end widget-rounded-circle-->
    </div> <!-- end col-->

    <div class="col-md-6 col-xl-3 mt-3 mt-sm-0">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-6">
                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                        <i class="fas fa-cart-arrow-down font-22 avatar-title text-primary"></i>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo $purchase_count; ?></span></h3>
                        <p class="mb-1 text-truncate"><?php _e('Assisted Purchase', 'korgou'); ?></p>
                    </div>
                </div>
            </div> <!-- end row-->
            <div class="row">
                <div class="col text-right">
                    <a href="<?php echo home_url('/my/assisted-purchase/order-list/'); ?>" class="btn btn-sm btn-outline-primary"><?php _e('View More', 'korgou'); ?></a>
                </div>
            </div>
        </div> <!-- end widget-rounded-circle-->
    </div>

</div>

<div class="row">
    <div class="col">
        <h4 class="section-title" style="background: rgba(102,88,221,0.7);"><?php _e('User profile', 'korgou'); ?></h4>
        <div class="widget-rounded-circle card-box">
            <dl class="row">
                <dt class="col-2 h4"><?php _e('Unique identity number', 'korgou'); ?></dt>
                <dd class="col-4 h4 d-flex align-items-center" style="color: #0063DC;">
                    <span><?php echo $user->userid; ?></span>
                    <a href="<?php echo home_url('/membership/'); ?>"><span class="ml-1 badge badge-light"><?php echo KG::the_role(); ?></span></a>
                    <?php korgou_user_role_image(); ?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="col-2"><?php _e('Email', 'korgou'); ?></dt>
                <dd class="col-4">
                    <?php echo $user->email; ?>
                </dd>
                <dt class="col-2"><?php _e('Country', 'korgou'); ?></dt>
                <dd class="col-4">
                    <?php if ($address): ?>
                        <?php echo $address->country; ?>
                    <?php endif; ?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="col-2"><?php _e('English Name', 'korgou'); ?></dt>
                <dd class="col-4"><?php echo $user->englishname; ?></dd>
                <dt class="col-2"><?php _e('Native Name', 'korgou'); ?></dt>
                <dd class="col-4">
                    <?php echo $user->nativename; ?>
                </dd>
            </dl>
            <dl class="row">
                <dt class="col-2"><?php _e('Address', 'korgou'); ?></dt>
                <dd class="col-10">
                    <?php if ($address): ?>
                        <?php echo $address->addressdetail . ' ' .  $address->city . ' ' .  $address->province; ?>
                    <?php endif; ?>
                </dd>
            </dl>
            <p class="text-danger font-weight-bold">
            <?php _e('Your address and contact method is very important for package processing. Please keep your account profile up to date.', 'korgou'); ?>
            </p>
            <div class="text-right1">
                <a href="<?php echo home_url('/my/address/'); ?>" class="btn btn-sm btn-outline-secondary"><?php _e('Manage Address', 'korgou'); ?></a>
                <a href="<?php echo home_url('/my/profile/'); ?>" class="btn btn-sm btn-outline-secondary mx-2"><?php _e('Update Profile', 'korgou'); ?></a>
                <a href="<?php echo home_url('/my/password/'); ?>" class="btn btn-sm btn-outline-secondary mt-2 mt-sm-0"><?php _e('Change Password', 'korgou'); ?></a>
            </div>
        </div> <!-- end widget-rounded-circle-->
    </div>
</div>

<div class="row">
    <div class="col-12">
        <h4 class="section-title"><?php _e('Korean Address', 'korgou'); ?></h4>
        <div class="card-box">
          <dl class="row">
            <dt class="col-sm-2"><?php _e('Address', 'korgou'); ?></dt>
            <dd class="col-sm-10">경기도 성남시 중원구 순환로 79 2층 <?php echo $user->userid; ?></dd>

            <dt class="col-sm-2"><?php _e('Address in English', 'korgou'); ?></dt>
            <dd class="col-sm-10"><?php echo $user->userid; ?>, 2F, 79, Sunhwan-ro, Jungwon-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</dd>

            <dt class="col-sm-2"><?php _e('Zip code', 'korgou'); ?></dt>
            <dd class="col-sm-10">13230</dd>

            <dt class="col-sm-2"><?php _e('Recipient', 'korgou'); ?></dt>
            <dd class="col-sm-10 text-danger font-weight-bold"><?php echo $user->userid; ?> (<?php _e('You should put your unique Identity Number. If not possible, please put your full name that you registered', 'korgou'); ?>)
            </dd>

            <dt class="col-sm-2"><?php _e('Mobile', 'korgou'); ?></dt>
            <dd class="col-sm-10">010-8996-4730</dd>

            <dt class="col-sm-2"><?php _e('Landline', 'korgou'); ?></dt>
            <dd class="col-sm-10">070-4250-0440</dd>

          </dl>

            <div class="row">
                <div class="col">
                    <a href="<?php echo home_url('/my/korean-address/'); ?>" class="btn btn-sm btn-outline-blue"><?php _e('View More', 'korgou'); ?></a>
                </div>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
