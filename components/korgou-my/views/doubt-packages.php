<div class="row">
    <div class="col-12">
        <div class="card-box">
            <p>
                <?php _e('KorGou\'s package forwarding system is built on the principle of automatic identification upon package arrival, providing the correct address and user unique identification number are correctly printed on the shipping invoice.', 'korgou'); ?>
            </p>
            <p>
                <?php _e('However, owing to issues such as the lack of enough room for address printing in English on the invoice, the seller\'s informaiton protection policy, invoice wearout or complete lost during the domestic shipping, some packages arrive at the KorGou warehouse without an identifiable recipient or unique identification number. These packages are hence classified as unidentified packages and awaiting to be individually claimed by our users.', 'korgou'); ?>
            </p>
            <p>
                <?php _e('Those who haven\'t received the package arrival notification email from KorGou several days after the order status changed to "shipped/배송완료" on the shopping sites are encouraged to periodically check the list of unidentified packages and claim the package if possible.', 'korgou'); ?>
            </p>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php
            do_action('korgou_search_form', [[
                    'name' => 'trackno', 'type' => 'text', 'label' => __('Tracking number', 'korgou')
                ], [
                    'type' => 'search',
                ], [
                    'label' => __('Search for the tracking number you want to find', 'korgou'),
                ], 
            ]);
            ?>

<!-- qqq
    <?php print_r($packages); ?>
    -->
            <div class="table-responsive">
            <table class="table-bordered table table-hover">
                <thead class="thead-light">
                <tr>
                    <th><?php _e('Arrival date', 'korgou'); ?></th>
                    <th><?php _e('Domestic tracking number', 'korgou'); ?></th>
                    <th><?php _e('Courier', 'korgou'); ?></th>
                    <th><?php _e('Claim it', 'korgou'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php if ($packages != null): ?>
                    <?php foreach ($packages->items as $package): ?>
                    <tr>
                        <td><?php echo $package->receivetime; ?></td>
                        <td><?php echo $package->trackno; ?></td>
                        <td><?php echo $package->courier; ?></td>
                        <td>
                            <a href="<?php echo home_url("/my/unidentified-packages/claim/?id={$package->id}&trackno={$package->trackno}"); ?>" class="btn btn-warning btn-xs"><?php _e('Claim it', 'korgou'); ?></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            </div>

            <?php do_action('korgou_my_pagination', $packages); ?>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<?php $this->ajax_form('default_address'); ?>
    <input type="hidden" name="id" value="" id="default-id">
</form>

<?php $this->ajax_form('delete_address'); ?>
    <input type="hidden" name="id" value="" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.default-btn').click(function() {
        $('#default-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('default_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
    $('.delete-btn').click(function() {
        $('#delete-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('delete_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
});
</script>
