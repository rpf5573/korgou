<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_profile', ['class' => 'needs-validation']); ?> 
                <h4 class="card-title"><?php _e('Basic Information', 'korgou'); ?></h4>
                <p class="text-warning">
                    <?php _e('Your address and contact method is very important for package processing, please keep your account profile up to date.', 'korgou'); ?>
                </p>
                <div class="form-group required row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('Email', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="email-input" name="email" value="<?php echo $user->email; ?>" required>
                        <small class="form-text">
                            <?php _e('For password recovry and important notifications, please enter your frequently used email address', 'korgou'); ?>
                        </small>
                    </div>
                </div>
                <div class="form-group required row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('English Full Name', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="-input" name="englishname" value="<?php echo $user->englishname; ?>" required>
                        <small class="form-text"><?php _e('Capitalized letters in English', 'korgou'); ?></small>
                    </div>
                </div>
                <div class="form-group required row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('Native Full Name', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="-input" name="nativename" value="<?php echo $user->nativename; ?>" required>
                        <small class="form-text"><?php _e('In the language of the recipient country', 'korgou'); ?></small>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('Gender', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <?php BS_Form::select([
                            'name' => 'gender',
                            'value' => $user->gender,
                            'id' => 'gender-input',
                            'options' => Korgou_User::$GENDERS,
                        ]); ?>
                    </div>
                </div>
                <div class="form-group required row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('Country', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <?php do_action('korgou_select_country', $user->country); ?>
                    </div>
                </div>

                <h4 class="card-title mt-4"><?php _e('Contact Information', 'korgou'); ?></h4>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('Phone Number', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="-input" name="phonenum" value="<?php echo $user->phonenum; ?>">
                        <small class="form-text">Eg. +1-312-400-5566</small>
                    </div>
                </div>
                <div class="form-group required row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('Mobile Phone', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="-input" name="mobilenum" value="<?php echo $user->mobilenum; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('Facebook', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="-input" name="facebookqq" value="<?php echo $user->facebookqq; ?>">
                    </div>
                </div>
                <div>
                    <span class="required-mark">*</span>
                    <?php _e('Required field', 'korgou'); ?>
                </div>

                <div class="mt-4">
                    <input type="reset" class="btn btn-secondary" value="<?php _e('Reset', 'korgou'); ?>">
                    <input type="submit" id="submit-btn" class="btn btn-primary" value="<?php _e('Update Account Profile', 'korgou'); ?>">
                </div>
            </form>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script src="https://cdn.jsdelivr.net/gh/guillaumepotier/Parsley.js@2.9.2/dist/parsley.js"></script>
<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.ajaxForm({
        beforeSubmit: function(arr, $form, options) {
            return $form.parsley().isValid();
        },
        success: function(response) {
            if (response.success)
                location.reload();
            else
                alert(response.data);
        }
    }).parsley({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid',
        errorsWrapper: '<span class="form-text text-danger"></span>',
        errorTemplate: '<span></span>',
        trigger: 'change'
    });
});
</script>
