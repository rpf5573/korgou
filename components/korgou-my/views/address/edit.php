<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('edit_address'); ?>
                <input type="hidden" name="id" value="<?php echo $address->id; ?>">
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-englishname" class="col-form-label"><?php _e('Full Name (Capatalized letters in English)', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-englishname" name="englishname" placeholder="" value="<?php echo $address->englishname; ?>" required>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-nativename" class="col-form-label"><?php _e('Full Name (in the language of the recipient country)', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-nativename" name="nativename" placeholder="" value="<?php echo $address->nativename; ?>" required>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-5">
                        <label for="input-country" class="col-form-label"><?php _e('Country', 'korgou'); ?> <span>*</span></label>
                        <?php do_action('korgou_select_country', $address->country); ?>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-zipcode" class="col-form-label"><?php _e('Zip / Postal Code (Please enter 000 if you don\'t have a Zip Code.)', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-zipcode" name="zipcode" placeholder="" value="<?php echo $address->zipcode; ?>" required>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-province" class="col-form-label"><?php _e('State / Province / Region', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-province" name="province" placeholder="" value="<?php echo $address->province; ?>" required>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-city" class="col-form-label"><?php _e('City', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-city" name="city" placeholder="" value="<?php echo $address->city; ?>" required>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-detail" class="col-form-label"><?php _e('Address detail', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-detail" name="addressdetail" placeholder="" value="<?php echo $address->addressdetail; ?>" required>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-company" class="col-form-label"><?php _e('Company / Organization name', 'korgou'); ?></label>
                        <input type="text" class="form-control" id="input-company" name="company" value="<?php echo $address->company; ?>" placeholder="">
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-phone" class="col-form-label"><?php _e('Phone number (Eg. +1-312-400-5566)', 'korgou'); ?></label>
                        <input type="text" class="form-control" id="input-phone" name="phonenum" value="<?php echo $address->phonenum; ?>" placeholder="">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-4">
                        <label for="input-mobile" class="col-form-label"><?php _e('Mobile phone', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-mobile" name="mobilenum" placeholder="" value="<?php echo $address->mobilenum; ?>" required>
                    </div>
                </div>

                <a href="<?php echo home_url('/my/address/'); ?>" class="btn btn-secondary"><?php _e('Cancel', 'korgou'); ?></a>
                <button type="button" id="submit-btn" class="btn btn-primary waves-effect waves-light"><?php _e('Submit', 'korgou'); ?></button>
            </form>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
    var $form = $('form[name="<?php $this->the_tag('edit_address'); ?>"]');
    $form.parsley();
    $('#submit-btn').click(function() {
        $form.ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    })
});
</script>
