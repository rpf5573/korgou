<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('add_address'); ?>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-englishname" class="col-form-label"><?php _e('Full Name (Capatalized letters in English)', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-englishname" name="englishname" placeholder="" required1>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-nativename" class="col-form-label"><?php _e('Full Name (in the language of the recipient country)', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-nativename" name="nativename" placeholder="" required1>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-5">
                        <label for="input-country" class="col-form-label"><?php _e('Country', 'korgou'); ?> <span>*</span></label>
                        <?php do_action('korgou_select_country'); ?>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-zipcode" class="col-form-label"><?php _e('Zip / Postal Code (Please enter 000 if you dont\'t have a Zip Code.)', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-zipcode" name="zipcode" placeholder="" required1>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-province" class="col-form-label"><?php _e('State / Province / Region', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-province" name="province" placeholder="" required1>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-city" class="col-form-label"><?php _e('City', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-city" name="city" placeholder="" required1>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-detail" class="col-form-label"><?php _e('Address detail', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-detail" name="addressdetail" placeholder="" required1>
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-company" class="col-form-label"><?php _e('Company / Organization name', 'korgou'); ?></label>
                        <input type="text" class="form-control" id="input-company" name="company" placeholder="">
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-4">
                        <label for="input-phone" class="col-form-label"><?php _e('Phone number', 'korgou'); ?> (Eg. +1-312-400-5566)</label>
                        <input type="text" class="form-control" id="input-phone" name="phonenum" placeholder="">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-4">
                        <label for="input-mobile" class="col-form-label"><?php _e('Mobile phone', 'korgou'); ?> <span>*</span></label>
                        <input type="text" class="form-control" id="input-mobile" name="mobilenum" placeholder="" required1>
                    </div>
                </div>

                <a href="<?php echo home_url('/my/address'); ?>" class="btn btn-secondary"><?php _e('Cancel', 'korgou'); ?></a>
                <button type="button" id="submit-btn" class="btn btn-primary waves-effect waves-light"><?php _e('Submit', 'korgou'); ?></button>
            </form>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
    var $form = $('form[name="<?php echo $this->get_tag('add_address'); ?>"]');
    $form.parsley();
    $('#submit-btn').click(function() {
        $form.ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    })
});
</script>