<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th><?php _e('English Name', 'korgou'); ?></th>
                        <th><?php _e('Native Name', 'korgou'); ?></th>
                        <th><?php _e('Address Detail', 'korgou'); ?></th>
                        <th><?php _e('City', 'korgou'); ?></th>
                        <th><?php _e('Province', 'korgou'); ?></th>
                        <th><?php _e('Country', 'korgou'); ?></th>
                        <th><?php _e('Zip code', 'korgou'); ?></th>
                        <th><?php _e('Default', 'korgou'); ?></th>
                        <th><?php _e('Actions', 'korgou'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($addresses as $addr): ?>
                    <tr>
                        <td><?php echo $addr->englishname; ?></td>
                        <td><?php echo $addr->nativename; ?></td>
                        <td><?php echo $addr->addressdetail; ?></td>
                        <td><?php echo $addr->city; ?></td>
                        <td><?php echo $addr->province; ?></td>
                        <td><?php echo $addr->country; ?></td>
                        <td><?php echo $addr->zipcode; ?></td>
                        <td><?php if ($addr->asdefault == '1'): ?>
                                <?php _e('Yes', 'korgou'); ?>
                            <?php else: ?>
                                <button class="default-btn btn btn-outline-success btn-xs" data-id="<?php echo $addr->id; ?>"><?php _e('Set default', 'korgou'); ?></button>
                            <?php endif; ?>
                        </td>
                        <td>
                            <a href="<?php echo home_url('/my/address/edit/?id=' . $addr->id); ?>" class="edit-btn btn btn-info btn-xs"><?php _e('Edit', 'korgou'); ?></a>
                            <button class="delete-btn btn btn-danger btn-xs" data-id="<?php echo $addr->id; ?>"><?php _e('Delete', 'korgou'); ?></button>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <a href="<?php echo home_url('/my/address/add/'); ?>" class="btn btn-primary"><?php _e('Add address', 'korgou'); ?></a>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<?php $this->ajax_form('default_address'); ?>
    <input type="hidden" name="id" value="" id="default-id">
</form>

<?php $this->ajax_form('delete_address'); ?>
    <input type="hidden" name="id" value="" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.default-btn').click(function() {
        $('#default-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('default_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
    $('.delete-btn').click(function() {
        $('#delete-id').val($(this).data('id'));
        $('form[name="<?php $this->the_tag('delete_address'); ?>"]').ajaxSubmit(function(response) {
            location.href = '<?php echo home_url('/my/address/'); ?>';
        });
        return false;
    });
});
</script>
