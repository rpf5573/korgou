<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_my';

    function load()
    {
        // $this->script('front');

        $this->add_action('page_title');
        $this->add_action('pagination');
        $this->add_ajax('update_profile');
        $this->add_ajax('update_password');
        $this->add_ajax('add_address');
        $this->add_ajax('edit_address');
        $this->add_ajax('delete_address');
        $this->add_ajax('default_address');
        $this->add_ajax('claim_package');
        $this->add_shortcode('profile');
        $this->add_shortcode('address');
        $this->add_shortcode('korean_address');

        // $this->add_ajax('package_apply_forward');
        // $this->add_ajax('package_apply_photo');
        // $this->add_ajax('package_apply_sorting');
        // $this->add_ajax('package_apply_return_exchange');

        $this->add_shortcode('dashboard');
        $this->add_shortcode('password');
        // $this->add_shortcode('packages');
        // $this->add_shortcode('packages_sorting_service');
        // $this->add_shortcode('packages_sorting_status');
        // $this->add_shortcode('packages_photos');
        // $this->add_shortcode('packages_forward_application');

        $this->add_shortcode('doubt_packages');
        $this->add_shortcode('doubt_packages_claim');

        $this->add_shortcode('balance_records');
    }

    function password()
    {
        $this->view('password');
    }

    function update_password()
    {
        $pass_cur             = ! empty( $_POST['password_current'] ) ? $_POST['password_current'] : '';
		$pass1                = ! empty( $_POST['password_1'] ) ? $_POST['password_1'] : '';
        $pass2                = ! empty( $_POST['password_2'] ) ? $_POST['password_2'] : '';

        $user_id = get_current_user_id();
        $current_user       = get_user_by( 'id', $user_id );

        $error = false;
        if ( ! empty( $pass_cur ) && empty( $pass1 ) && empty( $pass2 ) ) {
			$error = __( 'Please fill out all password fields.', 'woocommerce' );
		} elseif ( ! empty( $pass1 ) && empty( $pass_cur ) ) {
			$error = __( 'Please enter your current password.', 'woocommerce' );
		} elseif ( ! empty( $pass1 ) && empty( $pass2 ) ) {
			$error = __( 'Please re-enter your password.', 'woocommerce' );
		} elseif ( ( ! empty( $pass1 ) || ! empty( $pass2 ) ) && $pass1 !== $pass2 ) {
			$error = __( 'New passwords do not match.', 'woocommerce' );
		} elseif ( ! empty( $pass1 ) && ! wp_check_password( $pass_cur, $current_user->user_pass, $current_user->ID ) ) {
			$error = __( 'Your current password is incorrect.', 'woocommerce' );
		}

        if ($error) {
            wp_send_json_error($error);
        } else {
            $user            = new stdClass();
            $user->ID        = $user_id;
        	$user->user_pass = $pass1;
            wp_update_user( $user );

            wp_send_json_success('Password changed successfully.');
        }
    }

    function dashboard()
    {
        $userid = KG::get_current_userid();
        $user = apply_filters('korgou_user_get_user', null, ['userid' => $userid]);

        $this->view('dashboard', [
            'user' => $user,
            'balance' => KG::get_user_balance($userid),
            'address' => apply_filters('korgou_user_get_default_address', null, $userid),
            'purchase_count' => apply_filters('korgou_purchase_count', 0, [
                'userid' => $userid,
                // 'purchasetype' => ['1', '2'],
                'status' => [
                    Korgou_Purchase::STATUS_WAIT_CHECK, Korgou_Purchase::STATUS_WAIT_PAY, Korgou_Purchase::STATUS_PAY_SUCC, 
                ]
            ]),
            'package_count' => apply_filters('korgou_package_count', 0, [
                'userid' => $userid,
                // 'status' => [Korgou_Package::STATUS_IN_REPO, Korgou_Package::STATUS_APPLY_EXCHANGE, Korgou_Package::STATUS_APPLY_FORWARD],
                'status' => Korgou_Package::STATUS_IN_REPO,
            ]),
            'forward_count' => apply_filters('korgou_forward_count', 0, [
                'userid' => $userid,
                'status' => [
                    Korgou_Forward::STATUS_ACCEPTED, Korgou_Forward::STATUS_WAIT_PAY, Korgou_Forward::STATUS_PAID, Korgou_Forward::STATUS_FORWARDED,
                ]
            ]),
        ]);
    }

    function profile()
    {
        $userid = KG::get_current_userid();
        $user = apply_filters('korgou_user_get_user', null, ['userid' => $userid]);

        $this->view('profile', [
            'user' => $user,
        ]);
    }

    function address($atts = [])
    {
        $context = [];
        $view = 'address';

        switch ($atts['view'] ?? '') {
            case 'edit':
                $view .= '/edit';
                $context['address'] = apply_filters('korgou_user_get_current_address', null, $_GET['id']??0);
                break;
            case 'add':
                $view .= '/add';
                break;
            default:
                $context['addresses'] = apply_filters('korgou_user_get_addresses', []);
                break;
        }
        $this->view($view, $context);
    }

    function update_profile()
    {
        $data = Shoplic_util::get_post_data('email', 'englishname', 'nativename', 'gender', 'country', 'phonenum', 'mobilenum', 'facebookqq');
        $data['email'] = sanitize_email($data['email']);

        $user = wp_get_current_user();

        if ($user->user_email != $data['email'] && email_exists($data['email'])) {
            wp_send_json_error(_('You cannot use this email: ' . $data['email']));
        }

        $data['userid'] = KG::get_current_userid();
        $user_id = get_current_user_id();
        wp_update_user([
            'ID' => $user_id,
            'user_email' => $data['email']
        ]);

        update_user_meta($user_id, 'billing_first_name', $data['englishname']);
		update_user_meta($user_id, 'billing_phone', $data['phonenum']);
		update_user_meta($user_id, 'billing_email', $data['email']);

        do_action('korgou_user_update_user', $data);

        wp_send_json_success();
    }

    function add_address()
    {
        $address = apply_filters('korgou_user_insert_address', $_POST);
        wp_send_json_success();
    }

    function edit_address()
    {
        if (apply_filters('korgou_user_get_current_address', null, $_POST['id']??0) == null)
            wp_send_json_error();

        $address = apply_filters('korgou_user_update_address', $_POST);
        wp_send_json_success();
    }

    function default_address()
    {
        if (apply_filters('korgou_user_get_current_address', null, $_POST['id']??0) == null)
            wp_send_json_error();

        do_action('korgou_user_default_address', $_POST['id']);
        wp_send_json_success();
    }

    function delete_address()
    {
        if (apply_filters('korgou_user_get_current_address', null, $_POST['id']??0) == null)
            wp_send_json_error();

        do_action('korgou_user_delete_address', $_POST['id']);
        wp_send_json_success();
    }

    function page_title($title = '')
    {
        global $post;

        $this->view('page_title', ['title' => empty($title) ? $post->post_title : $title]);
    }

    function packages()
    {
        $paged = max( 1, get_query_var('paged') );
        $rowcount = $_GET['rowcount'] ?? 10;
        $context = [
            'packages' => apply_filters('korgou_package_get_package_page', [], [
                'userid' => KG::get_current_userid(),
            ], null, null, $rowcount, $paged),
        ];
        $this->view('packages', $context);
    }

    function the_status_style($package)
    {
        $style = '';
        switch ($package->status) {
            case '2':
            case '3':
                $style = 'text-success';
                break;
            case '4':
            case '5':
                $style = 'text-primary font-weight-bold';
                break;
            default:
                break;
        }
        echo $style;
    }
    function pagination($page)
    {
        $context = [
            'page' => $page,
            'rowcount' => $_GET['rowcount'] ?? 10,
        ];
        $this->view('pagination', $context);
    }

    function doubt_packages()
    {
        $paged = max( 1, get_query_var('paged') );
        $rowcount = $_GET['rowcount'] ?? 10;
        $trackno = $_GET['trackno'] ?? '';

        $context = [
            'packages' => null,
            'trackno' => $trackno,
        ];

        if (!empty($trackno)) {
            $conditions = [
                ['status', '=', Korgou_Doubt_Package::STATUS_WAIT],
                ['trackno', 'LIKE', '%'.$trackno.'%']
            ];
            $context['packages'] = apply_filters('korgou_get_doubt_package_page', [], [], $conditions, $rowcount, $paged);
        }

        $this->view('doubt-packages', $context);
    }

    function doubt_packages_claim()
    {
        $data = [
            'id' => $_GET['id'] ?? 0,
            'trackno' => $_GET['trackno'] ?? '0',
        ];
        $context = [
            'package' => apply_filters('korgou_get_doubt_package', [], $data),
        ];
        $this->view('doubt-packages/claim', $context);
    }

    function claim_package()
    {
        $data = Shoplic_Util::get_post_data('id');
        if (apply_filters('korgou_get_doubt_package', null, $data) == null)
            wp_send_json_error();

        try {
            korgou_check_images();
            $images = korgou_upload_images();
            for ($i = 0; $i < sizeof($images); ++$i) {
                $args = [
                    'packageid' => $data['id'],
                    'smallimageurl' => $images[$i]['small'],
                    'imageorder' => ($i + 1),
                    'bigimageurl' => $images[$i]['big'],
                    'imagetype' => Korgou_Package_Image::IMAGETYPE_DOUBT_PACKAGE,
                ];
                do_action('korgou_package_insert_image', 0, $args);
            }
        } catch (RuntimeException $e) {
            wp_send_json_error($e->getMessage());
        }

        $data = Shoplic_Util::get_post_data('claimpackagecontent', 'claimnote');
        $data['claimuserid'] = KG::get_current_userid();
        $data['status'] = Korgou_Doubt_Package::STATUS_CLAIMING;

        $condition = [
            'id' => $_POST['id'],
            'status' => Korgou_Doubt_PACKAGE::STATUS_WAIT,
        ];

        do_action('korgou_update_doubt_package', $data, $condition);

        wp_send_json_success(__('Operation success', 'korgou'));
    }

    function package_apply_photo()
    {
        Shoplic_Util::display_errors();

        $packageid = $_POST['packageid'];
        $userid = KG::get_current_userid();
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            wp_send_json_error('Package not available');

        $value_added = apply_filters('korgou_get_value_added', null, ['type' => 'ITEM_PHOTOGRAPHY']);

        do_action('korgou_user_balance_check_balance', - $value_added->price);

        $desc = $value_added->get_desc();

        do_action('korgou_user_balance_change_balance',
            $packageid, $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $value_added->price, 'KRW', $desc, '', '', 'sys', 'sys', ''
        ); 

        do_action('korgou_package_update_package',
            ['photo' => Korgou_Package::PHOTO_APPLY],
            ['packageid' => $packageid, 'photo' => Korgou_Package::PHOTO_NONE]
        );

        wp_send_json_success(__('Operation success', 'korgou'));
    }

    function packages_sorting_service()
    {

        $context = [
            'package' => apply_filters('korgou_package_get_package', null, [
                'userid' => KG::get_current_userid(),
                'packageid' => $_GET['packageid'] ?? '',
            ]),
        ];
        $this->view('packages/sorting-service', $context);

    }

    function packages_sorting_status()
    {
        $package =  apply_filters('korgou_package_get_package', null, [
            'userid' => KG::get_current_userid(),
            'packageid' => $_GET['packageid'] ?? '',
        ]);
        $check = apply_filters('korgou_package_get_check', null, [
            'packageid' => $package->packageid,
        ]);
        $context = [
            'package' => $package,
            'check' => $check,
        ];
        $this->view('packages/sorting-status', $context);
    }

    function package_apply_sorting()
    {
        $packageid = $_POST['packageid'];
        $userid = KG::get_current_userid();
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
            'userid' => $userid,
        ]);
        if ($package == null)
            wp_send_json_error('Package not available');

        if ($package->status != Korgou_Package::STATUS_IN_REPO)
            wp_send_json_error('Package not awaiting user process');

        $value_added = apply_filters('korgou_get_value_added', null, ['type' => 'ITEM_SORTING']);

        do_action('korgou_user_balance_check_balance', - $value_added->price);

        $desc = $value_added->get_desc();

        do_action('korgou_user_balance_change_balance',
            $packageid, $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $value_added->price, 'KRW', $desc, '', '', 'sys', 'sys', ''
        ); 

        do_action('korgou_package_update_package',
            ['checkliststatus' => Korgou_Package::CHECKLISTSTATUS_APPLY],
            ['packageid' => $packageid, 'checkliststatus' => Korgou_Package::CHECKLISTSTATUS_NONE]
        );

        do_action('korgou_package_insert_check', [
            'packageid' => $packageid, 
            'cargodetail' => $_POST['cargodetail'],
        ]); 

        wp_send_json_success(__('Operation success', 'korgou'));

    }

    function check_package()
    {
        $packageid = $_GET['packageid'] ?? '';
        if (!empty($packageid)) {
            $package =  apply_filters('korgou_package_get_package', null, [
                'userid' => KG::get_current_userid(),
                'packageid' => $packageid,
            ]);
            if ($package != null)
                return $package;
        }

        do_action('korgou_message', 'error', 'Package is not available');
        return false;
    }

    function packages_photos()
    {
        $package = $this->check_package();
        if (!$package)
            return;

        $images = apply_filters('korgou_package_get_image_list', [], [
            'packageid' => $package->packageid,
        ]);
        $context = [
            'package' => $package,
            'images' => $images,
        ];
        $this->view('packages/photos', $context);
    }

    function package_apply_return_exchange()
    {
        Shoplic_Util::display_errors();

        $packageid = $_POST['packageid'] ?? '';
        $ids = explode(',', $packageid);
        if (empty($ids))
            wp_send_json_error('Package not available');

        $userid = KG::get_current_userid();

        $packages = [];
        $args = [
            'userid' => $userid,
            'status' => Korgou_Package::STATUS_IN_REPO
        ];
        foreach ($ids as $id) {
            $args['packageid'] = $id;
            $package =  apply_filters('korgou_package_get_package', null, $args);
            if ($package == null)
                wp_send_json_error('Package not available');

            $packages[] = $package;
        }

        if (empty($packages))
            wp_send_json_error('Package not available');
        
        $value_added = apply_filters('korgou_get_value_added', null, ['type' => 'RETURN_EXCHANGE']);

        do_action('korgou_user_balance_check_balance', - $value_added->price * sizeof($packages));

        $desc = $value_added->get_desc();

        try {
            SPDB::start_transaction();

            foreach ($packages as $package) {
                do_action('korgou_package_update_package', [
                    'packageid' => $packageid, 
                    'status' => Korgou_Package::STATUS_APPLY_EXCHANGE
                ]);

                do_action('korgou_user_balance_change_balance',
                    $packageid, $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $value_added->price, 'KRW', $desc, '', '', 'sys', 'sys', ''
                ); 
            }

            SPDB::commit();
            wp_send_json_success();
        } catch (Exception $e) {
            SPDB::rollback();
            throw $e;
        } 
    }

    function korean_address()
    {
        $context = [
            'user' => wp_get_current_user(),
        ];
        $this->view('korean-address', $context);
    }

    function packages_forward_application()
    {
        $packageids = explode(',', $_GET['packageids']);
        $packages = [];
        $args = [];
        foreach ($packageids as $id) {
            $args['packageid'] = $id;
            $packages[] = apply_filters('korgou_package_get_package', null, $args);
        }
        $context = [
            'packages' => $packages,
            'value_addeds' => apply_filters('korgou_get_value_added_list', [], ['status' => Korgou_Value_Added::STATUS_OK]),
            'couriers' => apply_filters('korgou_forward_get_courier_list', [], ['status' => Korgou_Forward_Courier::STATUS_OPEN]),
            'addresses' => apply_filters('korgou_user_get_addresses', []),
        ];
        $this->view('packages/forward-application', $context);
    }

    function package_apply_forward()
    {
        //Shoplic_Util::display_errors();
        // validation

        $packageid = $_POST['packageid'] ?? '';
        if (empty($packageid))
            wp_send_json_error('No package id');
        
        $userid = KG::get_current_userid();
        $packages = [];
        $weight_total = 0;
        foreach ($packageid as $pid) {
            $package = apply_filters('korgou_package_get_package', null, [
                'userid' => $userid,
                'packageid' => $pid,
            ]);
            /*
            if ($package == null || $package->status != Korgou_Package::STATUS_IN_REPO)
                wp_send_json_error('Package is not available.');
                */

            $packages[] = $package;
            $weight_total += $package->weight;
        }

        $courier = apply_filters('korgou_forward_get_courier', null, ['id' => $_POST['forwardcourierid']]);
        if ($courier == null)
            wp_send_json_error('Forward courier is not available.');

        $user_discount = apply_filters('korgou_user_get_discount', null, $userid);
        $discount = ($user_discount != null) ? $user_discount->forwarddiscount : KG::get_user_discount_by_role($userid);

        $forwardid = 'F' . apply_filters('korgou_nextval', 0, 'packageseq');

        $fee = 0;
        if (isset($_POST['valueaddedservice'])) {
            foreach ($_POST['valueaddedservice'] as $type) {
                $value_added = apply_filters('korgou_get_value_added', null, ['type' => $type]);
                if ($value_added == null)
                    continue;

                $fee += $value_added->price;

                do_action('korgou_forward_insert_valueadded', [
                    'forwardid' => $forwardid,
                    'valueaddedtype' => $type,
                ]);
            }
        }

        // $customs_decls = Shoplic_Util::get_post_data_array('contents', 'quantity', 'value', 'netweight', 'hstariffnumber');
        $customs_decls = Shoplic_Util::get_post_data_array('contents', 'quantity', 'value');
        foreach ($customs_decls as $customs) {
            $customs['forwardid'] = $forwardid;
            do_action('korgou_forward_insert_customs_declaration', $customs);
        }

        foreach ($packages as $package) {
            do_action('korgou_package_update_package', [
                'packageid' => $package->packageid,
                'status' => Korgou_Package::STATUS_APPLY_FORWARD,
            ]);
        }

        $forward = array_merge([
            'forwardid' => $forwardid,
            'userid' => $userid,
            'status' => Korgou_Forward::STATUS_ACCEPTED,
            'valueaddedfee' => $fee,
            'packagetime' => current_time('mysql'),
            'forwardcouriername' => $courier->zhname . '-' . $courier->enname,
            'servicefeediscount' => $discount,
            'packagelist' => implode(',', $packageid),
        ], Shoplic_Util::get_post_data(
            'forwardcourierid', 'forwardcomment', 'englishname', 'nativename', 'country',
            'zipcode', 'province', 'city', 'addressdetail', 'company',
            'phonenum', 'mobilenum', 'processing'
        ));
        do_action('korgou_forward_insert_forward', $forward);

        wp_send_json_success();
    }

    function balance_records()
    {
        $paged = max( 1, get_query_var('paged') );
        $rowcount = $_GET['rowcount'] ?? 10;
        $context = [
            'records' => apply_filters('korgou_user_balance_get_balance_record_page', [], [
                'userid' => KG::get_current_userid()
            ], null, $rowcount, $paged),
        ];
        $this->view('balance-records', $context);
    }
};

