<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_my_payment';

    function load()
    {
        $this->add_shortcode();
        $this->add_action('add_to_cart', 10, 4);
    }

    function shortcode()
    {
        $context = [];
        if (isset($_REQUEST['amount']) && !empty($_REQUEST['amount'])) {
            $amount = intval($_REQUEST['amount']);
            $exs = apply_filters('korgou_currency_get_exchange_list', [], []);
            foreach ($exs as $ex) {
                $context[strtolower($ex->currency)] = round($amount / $ex->exchangekrw);
            }
        }
        $this->view('payment', $context);
    }

    function add_to_cart($userid, $total, $product_name, $service = [])
    {
        $balance = KG::get_user_balance($userid);
        if ($total <= $balance)
            return;

        $amount = apply_filters('korgou_currency_exchange_to_usd', 0, $total - $balance, 'USD');

        $post = get_page_by_path( $product_name, OBJECT, 'product' );
        if ($post) {
            WC()->cart->empty_cart();
            WC()->cart->add_to_cart($post->ID, 1, 0, [], ['ywcnp_amount' => $amount, '_service' => $service]);
        }
    }
};
