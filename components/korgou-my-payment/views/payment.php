<div class="row">
    <div class="col-12">
        <div class="card-box">
            <p>
            <?php _e('Your account balance will be used to pay the package forwarding and assisted payment orders, please make sure that you fund your KorGou account in advance to ensure smooth transactions.', 'korgou'); ?>
            </p>
            <p>
            <?php _e('KorGou accepts funding by the Alipay (in CNY), Paypal (in USD), and Korean bank wire transfer (in KRW). Funding made in CNY and USD will be changed to KRW (Korean Won) based on the current exchange rate and deposited to your KorGou account by which the subsequent package forwarding and assisted payment orders are paid.', 'korgou'); ?>
            </p>
            <p>
            <?php _e('You may fund your KorGou account using the following methods (Alipay Money Transfer,Alipay APP barcode payment,Paypal online payment ). In case you don\'t have Alipay or Paypal or prefer to pay by Korean bank wire transfer, please Contact us.', 'korgou'); ?>
            </p>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>


<div class="row">
    <div class="col">
        <div class="card-box">
            <h4 class="card-title"><?php _e('Alipay Money Transfer', 'korgou'); ?></h4>
            <ol>
                <li><?php _e('Please login to Alipay website or use Alipay APP to transfer to KorGou\'s Alipay account.', 'korgou'); ?></li>
                <li><?php _e('KorGou\'s Alipay account is pay@korgou.com , make sure it is correctly inputted.', 'korgou'); ?></li>
                <li><?php _e('Please avoid editing the default data in "Payment description" and "Note" which are reserved for payment check and confirmation.', 'korgou'); ?></li>
                <li><?php _e('Your payment will typically be confirmed and deposited to your KorGou account in an hour during business hours.
                Payment confirmation delay is expected during non-business hours. In case you need assistance concerning payment, please Contact KorGou Customer Support Center.', 'korgou'); ?>
                </li>
            </ol>
        </div> <!-- end card-box -->

        <div class="card-box">
            <h4 class="card-title"><?php _e('Alipay APP barcode payment', 'korgou'); ?>
            </h4>
            <p>
                <?php _e('KorGou Alipay Account pay@korgou.com', 'korgou'); ?>
            </p>
            <div class="row">
                <div class="col-sm-8">
                    <ol>
                        <li>
                        <?php _e('In case you don\'t have a computer on hand or are worried about the security of public computers, you can still add funds to your KorGou account with the help of the Alipay APP barcode payment.', 'korgou'); ?>
                        </li>
                        <li>
                            <?php _e('Simply open the Alipay APP on your mobile device, choose "Scan barcode" at the upper left corner of the screen, bring the KorGou\'s Alipay account barcode into view in the center of your device\'s screen, and the payment screen will open. Complete the payment following the on-screen instructions.', 'korgou'); ?>
                        </li>
                        <li>
                        <?php _e('KorGou\'s Alipay account is pay@korgou.com , make sure the scanned "payee" is identical to it.', 'korgou'); ?>
                        </li>
                        <li>
                        <?php _e('Since Alipay barcode payment doesn\'t support adding notes, to help we identify your payment, please email the payment confirmation screenshot to <b>pay@korgou.com</b> with the subject format of"Date+User identification number+AlipayXXXCNY" (<b>20150515+X99999+Alipay5000CNY</b>。', 'korgou'); ?>
                        </li>
                        <li>
                        <?php _e('It can be confirmed and credited within one hour after recharge on working days, there will be delays on non-working days. For assistance, please contact KorGou Customer Support Center.', 'korgou'); ?>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <img src="/wp-content/uploads/2021/03/korgou_qrcode.png" style="width: 100%;">
                </div>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->

    <div class="col">
        <div class="card-box">
            <h4 class="card-title">
                <?php _e('Paypal online payment', 'korgou'); ?>
            </h4>
            <div class="text-center">
                <p>
                    <img src="/wp-content/uploads/2020/03/bdg_now_accepting_pp_2line_w.png">
                </p>
                <form id="cart-form" method="POST" action="/deposit/" class="form-inline justify-content-center mb-2">
                    <input type="hidden" name="quantity" value="1">
                    <input type="hidden" name="add-to-cart" value="136">
                    <label class="mr-2" for="inlineFormCustomSelectPref"><?php _e('Amount', 'korgou'); ?> : </label>
                    <input type="text" class="form-control mr-sm-2" id="ywcnp_amount" name="ywcnp_amount" placeholder="" value="<?php echo $usd ?? ''; ?>">
                    <label class="mr-2" for="inlineFormCustomSelectPref"><?php _e('USD', 'korgou'); ?></label>
                </form>
                <p>
                    <input type="image" id="cart-btn" src="/wp-content/uploads/2020/03/btn_paynow_LG.gif">
                </p>
            </div>

            <ol>
                <li>
                    <?php _e('You will be automatically re-directed to the Paypal payment page when you click the "Pay Now" button. Make sure that the URL address opened is the Paypal\'s HTTPS secure link, otherwise completely close the page and exit any on-going payment.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('KorGou\'s Paypal account is <b>pay@korgou.com</b>, make sure the "payee" appeared on the page is identical.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('Your KorGou account information will be automatically integrated into the Paypal transaction, thus additional note is not necessary. Your Paypal payment should be correctly identified and the corresponding amount will be deposited to your KorGou account.', 'korgou'); ?>
                </li>
                <li>
                    <?php _e('Your payment will typically be confirmed and deposited to your KorGou account in half an hour during business hours. Payment confirmation delay is expected during non-business hours. In case you need assistance concerning payment, please Contact us.', 'korgou'); ?>
                </li>
            </ol>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
    function addToCart() {
        $('#cart-form').ajaxSubmit(function() {
            document.location = '<?php echo home_url('/checkout/'); ?>';
        });
    }
    $('#cart-form').submit(function() {
        addToCart();
        return false;
    });
    $('#cart-btn').click(function() {
        addToCart();
        return false;
    });
});
</script>
