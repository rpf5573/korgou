<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_direct';

    function load()
    {
        $this->style('front');
		$this->add_shortcode('category');
		$this->add_shortcode('products');
		$this->add_shortcode('product_list');

        wp_enqueue_style( 'dashicons' );
    }

    function product_list($atts = [])
    {
        $cat = $atts['cat'];
        
        $products = get_posts([
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'tax_query' => [
                [
                    'taxonomy' => 'product_cat',
                    'terms' => $cat,
                    'field' => 'slug'
                ],
            ],
            'orderby' => 'ID',
            'order' => 'ASC',
        ]);

        $this->view('product-list', [
            'products' => $products,
            'cat' => $cat,
        ]);

    }

    function category($atts = [])
    {
        $cat = $atts['cat'];
        
        $products = get_posts([
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'tax_query' => [
                [
                    'taxonomy' => 'product_cat',
                    'terms' => $cat,
                    'field' => 'slug'
                ],
            ],
            'orderby' => 'ID',
            'order' => 'ASC',
        ]);

        $this->view('category', [
            'products' => $products,
            'cat' => $cat,
        ]);

    }

    function products($atts = [])
    {
        $cat = $atts['cat'];
        $category = get_term_by('slug', $cat, 'product_cat');
        if (!$category)
            return;

        
        echo '<div class="korgou-direct">';

        $this->category_image($category);

        echo '<div class="product-list">';

        $children = get_terms([
            'taxonomy' => 'product_cat',
            'hide_empty' => false,
            'parent' => $category->term_id,
            'orderby' => 'meta_value_num',
            'meta_key' => 'order',
        ]);

        if (empty($children)) {
            $this->show_products($category);
        } else {
            foreach ($children as $category) {
                $this->subcategory($category);
                $this->show_products($category);
            }
        }

        echo '</div>';
        echo '</div>';
    }

    function category_image($category)
    {
        $thumb_id = get_term_meta($category->term_id, 'thumbnail_id', true);
        if (empty($thumb_id))
            return;

        $url = wp_get_attachment_url($thumb_id);
        
        echo '<div class="cat-thumb"><img src="' . $url . '"></div>';
    }

    function subcategory($category)
    {
        $this->view('subcategory', [
            'category' => $category,
        ]);
    }
    function show_products($category)
    {
        $products = get_posts([
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'tax_query' => [
                [
                    'taxonomy' => 'product_cat',
                    'terms' => $category->term_id,
                    'field' => 'term_id'
                ],
            ],
            'orderby' => 'ID',
            'order' => 'ASC',
        ]);

        $this->view('products', [
            'products' => $products,
            'cat' => $category,
        ]);

    }
};
