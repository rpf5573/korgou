<div class="subcategory">
    <div class="name"><?php echo $category->name; ?></div>
    <?php if (!empty($category->description)): ?>
    <div class="description">
        <?php echo $category->description; ?>
    </div>
    <?php endif; ?>
</div>