<div class="korgou-direct">
    <div class="product-list">
        <div class="carousel" id="carousel-<?php echo $cat; ?>">

            <?php foreach ($products as $product): ?>
            <div class="carousel-cell">
                <a href="/direct-selling/<?php echo $cat; ?>/">
                    <figure>
                        <img src="<?php echo get_the_post_thumbnail_url($product); ?>">
                    </figure>
                    <div class="noto-sans">
                        <div class="title">
                            <?php echo $product->post_title; ?>
                        </div>
                        <div class="subtitle">
                            <?php echo get_field('subtitle', $product->ID); ?>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
        .flickity({
    // options
    cellAlign: 'left',
    contain: true
    });
    const swiper = new Swiper('.swiper', {
        slidesPerView: 3,
        spaceBetween: 20,
        // Optional parameters

        pagination: {
            el: '.swiper-pagination',
            type: "fraction"
        },
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev-2',
        },
        /*
        scrollbar: {
            el: '.swiper-scrollbar',
            draggable: true,
        },
        */
        loop: false
    });
});
</script>


