<div class="korgou-direct">
    <div class="product-list-wrap">
        <div class="product-list slides">
            <div class="swiper-wrap">
                <div class="swiper" id="swiper-<?php echo $cat; ?>">
                    <div class="swiper-wrapper">

                        <?php foreach ($products as $product): ?>
                        <div class="swiper-slide">
                            <a href="/direct-selling/<?php echo $cat; ?>/">
                                <figure>
                                    <img src="<?php echo get_the_post_thumbnail_url($product); ?>">
                                </figure>
                                <div class="noto-sans">
                                    <div class="title">
                                        <?php echo $product->post_title; ?>
                                    </div>
                                    <div class="subtitle">
                                        <?php echo get_field('subtitle', $product->ID); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>

                    <!--
                    <div class="swiper-pagination"></div>

                    <div class="swiper-scrollbar"></div>
                    -->

                </div>

            </div>
        </div>

        <div class="swiper-button-prev" id="swiper-button-prev-<?php echo $cat; ?>"></div> <div class="swiper-button-next" id="swiper-button-next-<?php echo $cat; ?>"></div>

    </div>

</div>

<script type="text/javascript">
jQuery(function($) {
    const swiper = new Swiper('#swiper-<?php echo $cat; ?>', {
        slidesPerView: 3,
        spaceBetween: 20,
        // Optional parameters

/*
        pagination: {
            el: '.swiper-pagination',
            type: "fraction"
        },
            */
        // Navigation arrows
        navigation: {
            nextEl: '#swiper-button-next-<?php echo $cat; ?>',
            prevEl: '#swiper-button-prev-<?php echo $cat; ?>',
        },
        /*
        scrollbar: {
            el: '.swiper-scrollbar',
            draggable: true,
        },
        */
        loop: false,
        breakpoints: {
            320: {
                slidesPerView: 1
            },
            610: {
                slidesPerView: 2
            },
            970: {
                slidesPerView: 3
            }
        }
    });
});
</script>