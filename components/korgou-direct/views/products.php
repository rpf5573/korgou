<div class="products">
    <?php foreach ($products as $product): ?>
        <div class="product">
            <!-- <a href="/my/assisted-purchase/application/?type=direct"> -->
            <a href="<?php echo get_permalink($product); ?>">
                <figure>
                    <img src="<?php echo get_the_post_thumbnail_url($product); ?>">
                </figure>
                <div class="title noto-sans">
                    <?php echo $product->post_title; ?>
                </div>
                <div class="subtitle noto-sans">
                    <?php echo get_field('subtitle', $product->ID); ?>
                </div>
                <?php if (!empty($product->post_excerpt)): ?>
                    <hr>
                    <div class="excerpt noto-sans fw-m">
                        <?php echo $product->post_excerpt; ?>
                    </div>
                <?php endif; ?>
            </a>

        </div>
    <?php endforeach; ?>
</div>
